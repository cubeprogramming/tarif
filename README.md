# tarif

Part of billing project designed for telecom back in 2005 and made public. Intention of this repository is to serve primarily as a backup for this project.
Cod relays on multithreading asynchronous framework "thrdmonitor" for heavy parallel processing of CDR telephone call files. Code is also using specific features of Oracle database driver to allow for connection polling, batching and background thread creation for each batch sent to database.
