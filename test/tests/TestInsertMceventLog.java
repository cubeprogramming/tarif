package tests;

import junit.framework.*;
import tarif.logs.*;
import tarif.utils.*;
import tarif.*;
import java.io.*;
import com.cubeprogramming.utils.*;

public class TestInsertMceventLog
    extends TestCase {
  private InsertMceventLog insertMceventLog = null;

  public TestInsertMceventLog(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    insertMceventLog = new InsertMceventLog(Main.jdbcPool.getConnection(), 50);
  }//~setUp()

  protected void tearDown() throws Exception {
    insertMceventLog.closeConnection();
    insertMceventLog = null;
    super.tearDown();
  }//~tearDown()

  public void testUpisi() throws ExitException {
    String cdrName = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\cdr-in\\tt31001_20041001_00XXXX.txt";
    String logName = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\cdr-in\\tt31004_20041027_11XXXX.log";
    File cdrFile = new File(cdrName);
    File logFile = new File(logName);
    insertMceventLog.upisi(cdrFile, logFile);
  }//~testUpisi()

}//~class TestInsertMceventLog
