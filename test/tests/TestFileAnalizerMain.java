package tests;

import tarif.threads.*;
import tarif.rating.*;
import tarif.logs.*;
import tarif.*;
import java.io.*;
import java.util.*;
import com.cubeprogramming.thrdmonitor.*;


public class TestFileAnalizerMain implements ThreadListener, FreeResources{
  private ThreadGroup testThrdGrp = new ThreadGroup("testThrdGrp");
  private Monitor monitor = Monitor.getMonitor(testThrdGrp);
  private String fileName1, fileName2;

  public static void main(String[] argv) throws Exception {
    new TestFileAnalizerMain();
  }//~main()

  public TestFileAnalizerMain() throws Exception {
    Configuration.dateFormat.setTimeZone(Configuration.CET);
    System.out.println("---- Startano: " + Configuration.dateFormat.format(new java.util.Date( ) ) );
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    Main.ratingTables = new RatingTables(Main.jdbcPool.getConnection());
    Main.DBConfig = (Map)Main.cfg.configHash.get(Configuration.DB_CONFIG);
    String logPath = (String)Main.DBConfig.get(Globals.LOG_DIR);
    String logMain = (String)Main.DBConfig.get(Globals.LOG_FILE);
    String fileLog = (String)Main.DBConfig.get(Globals.DEBUG_LOG);
    Main.mainLog = new MainLog(logPath + logMain);
    Main.debugLog = new LogFile(logPath + fileLog); //DEBUG
    fileName1 = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\test\\cdr-in\\TestFileAnalizer\\tt31001_20041001_001845.txt";
    fileName2 = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\test\\cdr-in\\TestFileAnalizer\\tt31001_20041001_001846.txt";
    monitor.addThreadListener(this);

    new FileAnalizer(testThrdGrp, new File(fileName1) );
    new FileAnalizer(testThrdGrp, new File(fileName2) );
  }//~TestMonitorMain()

  public void notified(ThreadEvent thrdEvent) {
    System.out.println("Notification in TestCase received: " + thrdEvent.toString());
  }//~notified()


  public void free() throws Exception {
    System.out.println("---- Zavrseno: " + Configuration.dateFormat.format(new java.util.Date( ) ) );
  }//~free()

}//~class TestMonitorMain
