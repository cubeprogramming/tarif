package tests;

import junit.framework.*;
import java.sql.*;
import tarif.JDBCPool;

public class TestJDBCPool
    extends TestCase {
  private JDBCPool jDBCPool = null;

  public TestJDBCPool(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
  }

  protected void tearDown() throws Exception {
    jDBCPool = null;
    super.tearDown();
  }

  public void testGetConnection() throws SQLException {
    Connection conn = jDBCPool.getConnection();
    assertNotNull(conn);
  }
}
