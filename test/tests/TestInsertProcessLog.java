package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import tarif.logs.*;
import java.sql.*;


public class TestInsertProcessLog
    extends TestCase {
  private InsertProcessLog insertProcessLog = null;

  public TestInsertProcessLog(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    insertProcessLog = new InsertProcessLog(Main.jdbcPool.getConnection());
  }//~setUp()

  protected void tearDown() throws Exception {
    insertProcessLog.closeConnection();
    insertProcessLog = null;
    super.tearDown();
  }//~tearDown()

  public void testUpisi() throws SQLException {
    insertProcessLog.upisi();
  }//~testUpisi()

}//~class TestInsertProcessLog
