package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import com.cubeprogramming.utils.*;

public class TestInsertParcijalnih
    extends TestCase {
  private InsertParcijalnih insertParcijalnih = null;

  public TestInsertParcijalnih(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    insertParcijalnih = new InsertParcijalnih(jDBCPool.getConnection(),50);
  }//~setUp()

  protected void tearDown() throws Exception {
    insertParcijalnih.closeConnection();
    insertParcijalnih = null;
    super.tearDown();
  }//~tearDown()

  public void testOdvoji() throws ExitException {
    MetaSlog metaSlog = new MetaSlog();

    metaSlog.cause_for_output = new Long(1);
    metaSlog.record_number = new Long(2);
    metaSlog.naziv_datoteke = "TT52004_20031101_000755.txt";
    metaSlog.call_id = new Long(1853221929);
    //metaSlog.call_id = null;
    metaSlog.mrezna_grupa = "052";
    metaSlog.msisdn = new Long(830552);
    metaSlog.sifra_servisa = "10";
    metaSlog.sifra_sservisa = "FWD";
    metaSlog.pozivni_broj = "076767676";
    metaSlog.datumv = new Timestamp(System.currentTimeMillis());
    metaSlog.ptrajanje = new Long(11525);
    metaSlog.otrajanje = null;
    metaSlog.impulsi = new Long(1);
    metaSlog.status = "0";
    metaSlog.iznos = null;
    metaSlog.pcijena = null;
    metaSlog.bbroj_prefiks = null;
    metaSlog.zona_id = null;
    metaSlog.katpoziva_id = null;
    metaSlog.plan_id = null;
    metaSlog.tarifa_id = null;
    metaSlog.pretplatnik_id = null;
    metaSlog.korisnik_id = null;
    metaSlog.bgrupa_id = null;
    metaSlog.paket_id = null;
    metaSlog.a_category = "0";
    metaSlog.b_category = null;
    metaSlog.type_of_b_num = null;
    metaSlog.charged_party = "0";
    metaSlog.origin_for_charging = null;
    metaSlog.tariff_class = null;
    metaSlog.charging_case = "28";
    metaSlog.charging_case_origin = null;
    //metaSlog.tftraffic_id = null;
    metaSlog.tftraffic_id = new Long(100);
    metaSlog.flex_uus = new Long(1);
    metaSlog.file_prefix = "TT52004";
    metaSlog.type_of_a_num = "2";

    assertTrue(insertParcijalnih.odvoji(metaSlog));
  }//~testOdvoji()

}//~class TestObradaParcijalnih
