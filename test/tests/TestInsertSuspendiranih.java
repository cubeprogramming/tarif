package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import com.cubeprogramming.utils.*;

public class TestInsertSuspendiranih
    extends TestCase {
  private InsertSuspendiranih insertSuspendiranih = null;

  public TestInsertSuspendiranih(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    insertSuspendiranih = new InsertSuspendiranih(jDBCPool.getConnection(),50);
  }

  protected void tearDown() throws Exception {
    insertSuspendiranih.closeConnection();
    insertSuspendiranih = null;
    super.tearDown();
  }

  public void testUpisi() throws ExitException {
    MetaSlog metaSlog = new MetaSlog();

    metaSlog.naziv_datoteke = "TT52004_20031101_000755.txt";
    metaSlog.call_id = new Long(1853221929);
    metaSlog.mrezna_grupa = "051";
    metaSlog.msisdn = new Long(274214);
    metaSlog.sifra_servisa = "10";
    metaSlog.sifra_sservisa = "FWD";
    metaSlog.pozivni_broj = "076767676";
    metaSlog.datumv = new Timestamp(System.currentTimeMillis());
    metaSlog.ptrajanje = new Long(2410);
    metaSlog.otrajanje = new Long(2500);
    metaSlog.impulsi = new Long(0);
    metaSlog.iznos = new Float(2.56);
    metaSlog.bbroj_prefiks = "076";
    metaSlog.zona_id = new Long(51);
    metaSlog.katpoziva_id = new Long(10713);
    metaSlog.plan_id = new Long(1);
    metaSlog.tarifa_id = new Long(1);
    metaSlog.a_category = "0";
    metaSlog.b_category = null;
    metaSlog.type_of_b_num = null;
    metaSlog.charged_party = "0";
    metaSlog.origin_for_charging = null;
    metaSlog.tariff_class = null;
    metaSlog.charging_case = "28";
    metaSlog.charging_case_origin = null;
    metaSlog.description = "Nepoznata zona B-broja";
    metaSlog.flex_uus = new Long(1);
    metaSlog.error_code = "921";
    metaSlog.type_of_a_num = "2";

    metaSlog.pretplatnik_id = new Long(0);
    metaSlog.korisnik_id = new Long("985102742141");
    metaSlog.bgrupa_id = new Long(1);
    metaSlog.paket_id = new Long(1);
    metaSlog.file_prefix = "TT52004";


    assertTrue(insertSuspendiranih.upisi(metaSlog));
  }//~testUpisi()

}//~class TestInsertSuspendiranih
