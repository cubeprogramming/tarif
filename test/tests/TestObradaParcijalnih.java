package tests;

import junit.framework.*;
import tarif.*;
import tarif.rating.*;
import tarif.logs.*;
import java.util.*;

public class TestObradaParcijalnih
    extends TestCase {
  private ObradaParcijalnih obradaParcijalnih = null;
  private ThreadGroup fileAnalizers = new ThreadGroup("fileAnalizers");

  public TestObradaParcijalnih(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    Main.DBConfig = (Map)Main.cfg.configHash.get(Configuration.DB_CONFIG);
    Main.ratingTables = new RatingTables(Main.jdbcPool.getConnection());
    String logPath = (String)Main.DBConfig.get(Globals.LOG_DIR);
    String logMain = (String)Main.DBConfig.get(Globals.LOG_FILE);
    String fileLog = (String)Main.DBConfig.get(Globals.DEBUG_LOG);
    Main.mainLog = new MainLog(logPath + logMain);
    Main.debugLog = new LogFile(logPath + fileLog); //DEBUG
    Main.DEBUG = true;
    obradaParcijalnih = new ObradaParcijalnih(fileAnalizers);
  }//~setUp()

  protected void tearDown() throws Exception {
    obradaParcijalnih = null;
    super.tearDown();
  }//~tearDown()

  public void testRun() {
    obradaParcijalnih.run();
  }//~testRun()

}//~class TestObradaParcijalnih
