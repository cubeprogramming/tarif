package tests;

import junit.framework.*;
import tarif.logs.*;
import tarif.*;
import java.sql.*;

public class TestUpdateProcessLog
    extends TestCase {
  private UpdateProcessLog updateProcessLog = null;

  public TestUpdateProcessLog(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    updateProcessLog = new UpdateProcessLog(Main.jdbcPool.getConnection());
  }//~setUp()

  protected void tearDown() throws Exception {
    updateProcessLog.closeConnection();
    updateProcessLog = null;
    super.tearDown();
  }//~tearDown()

  public void testUpisi() throws SQLException {
    char status = 'O';
    updateProcessLog.upisi(status);
  }//~testUpisi()

}//~class TestUpdateProcessLog
