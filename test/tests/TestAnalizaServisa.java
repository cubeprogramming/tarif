package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.threads.*;
import tarif.*;
import java.util.*;
import com.cubeprogramming.utils.*;

public class TestAnalizaServisa
    extends TestCase {
  private AnalizaServisa analizaServisa = null;
    private FieldFiller fieldFiller = null;
    private MetaSlog metaSlog = new MetaSlog();
    private CdrRecord cdrRecord;
    private HashMap parsedCdr;


  public TestAnalizaServisa(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
        JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
        Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
        Main.ratingTables = new RatingTables(jDBCPool.getConnection());
        cdrRecord = new CdrRecord(Main.cfg);
        String recordType = "EWSD_R16";
        String cdr = "101 2288 852830552           0 2 9076767676         0310312300380000000     1  28  3 00                3         2         30       0000       130                        BRITCI    3 2413;1;37                                             0  0                                600000185322192952004E0001";
        String fileName = "TT52004_20031101_000755.txt";
        cdrRecord.initialize(recordType, cdr);
        fieldFiller = new FieldFiller(fileName, cdrRecord, metaSlog);
        fieldFiller.fillMeta();

    analizaServisa = new AnalizaServisa(metaSlog);
  }

  protected void tearDown() throws Exception {
    analizaServisa = null;
    super.tearDown();
  }

  public void testSifraSServisa() throws ExitException {
    analizaServisa.sifraSServisa();
    assertEquals("FWD",metaSlog.sifra_sservisa);
  }

}//~class TestAnalizaServisa
