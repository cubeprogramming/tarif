package tests;

import junit.framework.*;
import tarif.threads.*;
import tarif.*;
import tarif.utils.*;
import java.util.*;
import java.sql.*;
import com.cubeprogramming.utils.*;

public class TestCdrRecord
    extends TestCase {
  private CdrRecord cdrRecord;
  private String recordType;
  private String cdr;

  public TestCdrRecord(String name) throws SQLException {
    super(name);

  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(7) );
    cdrRecord = new CdrRecord(Main.cfg);
    recordType = "AXE_NEW";
    cdr = "0000101059035        319                                                    00823697402          01004010006697487            000040108            2369740206              697487     0003072609055909064000001200004200000000000010000005000201220    001   000100LC PAG HR11 CN-CJMAND CJMAND 0010200100200697487            23001A0002";
  }

  protected void tearDown() throws Exception {
    cdrRecord = null;
    super.tearDown();
  }

  public void testInitialize() {
    assertTrue(cdrRecord.initialize(recordType, cdr));
  }//~testInitialize()

  public void testParsedCdr() throws ExitException {
    assertTrue(cdrRecord.initialize(recordType, cdr));
    //assertNotNull(cdrRecord.parsedCdr());

    Object expectedReturn =  null;
    Object actualReturn = cdrRecord.parsedCdr().get("BLA");
    assertEquals(expectedReturn, actualReturn);
  }//~testParsedCdr()

}//~class TestCdrRecord
