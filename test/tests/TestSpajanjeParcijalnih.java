package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.logs.*;
import tarif.*;
import java.sql.*;
import java.util.*;
import java.io.*;

public class TestSpajanjeParcijalnih
    extends TestCase {
  private SpajanjeParcijalnih spajanjeParcijalnih = null;
  private static SelectParcijalnih selectParcijalnih;
  private ResultSet rset;
  private Connection conn;
  private JDBCPool jdbcPool;

  public TestSpajanjeParcijalnih(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    conn = Main.jdbcPool.getConnection();
    Main.cfg = new Configuration(conn, new Integer(8) );
    Main.DBConfig = (Map)Main.cfg.configHash.get(Configuration.DB_CONFIG);
    Main.ratingTables = new RatingTables(conn);
    int buffSize = new Integer( (String) Main.DBConfig.get(Globals.BUFF_SIZE)).intValue();
    File fileLog = new File(Globals.PARTLOGNAME);
    File partFile = new File(Globals.PARTPNAME);
    InsertMceventLog insertMceventLog = new InsertMceventLog(conn,buffSize);
    insertMceventLog.upisi(partFile, fileLog);
    UpdateMceventLog updateMceventLog = new UpdateMceventLog(conn,buffSize,partFile);
    UpdateParcijalnih updateParcijalnih = new UpdateParcijalnih(conn,buffSize);
    selectParcijalnih = new SelectParcijalnih(conn,20000);
    rset = selectParcijalnih.select(new Timestamp(System.currentTimeMillis()));
    spajanjeParcijalnih = new SpajanjeParcijalnih(updateMceventLog,updateParcijalnih, rset, true);
  }//~setUp()

  protected void tearDown() throws Exception {
    //spajanjeParcijalnih = null;
    super.tearDown();
  }//~tearDown()

  public void testSpoji() throws Exception {
    MetaSlog metaSlog = new MetaSlog();
    boolean expectedReturn = true;
    boolean actualReturn =true;
    if (rset.next()) {
      selectParcijalnih.fillMeta(metaSlog);
      actualReturn = spajanjeParcijalnih.spoji(metaSlog);
    }
    assertEquals("Spajanje parcijalnih", expectedReturn, actualReturn);
  }//~testSpoji()

}//~class TestSpajanjeParcijalnih
