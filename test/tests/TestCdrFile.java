package tests;

import junit.framework.*;
import java.io.*;
import java.util.*;
import tarif.*;
import tarif.utils.*;
import tarif.threads.CdrFile;
import com.cubeprogramming.utils.*;

public class TestCdrFile
    extends TestCase {
  private CdrFile cdrFile = null;

  public TestCdrFile(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(7) );
    Main.DBConfig = (Map)Main.cfg.configHash.get(Configuration.DB_CONFIG);
    String fileName = "d:\\home\\cube\\Projects\\cpp\\cdrs\\cdrin\\TT10062_UDR01_20030901_092749.txt";
    try {
      cdrFile = new CdrFile( new File(fileName) );
    }
    catch (ExitException e) {
      fail(e.getMessage());
    }
  }//~setUp()

  protected void tearDown() throws Exception {
    cdrFile = null;
    super.tearDown();
  }//~tearDown()

  public void testReadLine() throws IOException {
    //String expectedReturn = null;
    String actualReturn = cdrFile.readLine();
    //assertEquals("return value", expectedReturn, actualReturn);
    assertNotNull(actualReturn);
  }//~testReadLine()

}
