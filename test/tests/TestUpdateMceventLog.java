package tests;

import junit.framework.*;
import tarif.logs.*;
import java.sql.*;
import tarif.utils.*;
import tarif.*;
import java.io.*;
import com.cubeprogramming.utils.*;

public class TestUpdateMceventLog
    extends TestCase {
  private UpdateMceventLog updateMceventLog = null;

  public TestUpdateMceventLog(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    String cdrName = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\cdr-in\\tt31001_20041001_00XXXX.txt";
    File cdrFile = new File(cdrName);
    updateMceventLog = new UpdateMceventLog(Main.jdbcPool.getConnection(),50,cdrFile);
  }//~setUp()

  protected void tearDown() throws Exception {
    updateMceventLog.closeConnection();
    updateMceventLog = null;
    super.tearDown();
  }//~tearDown()

  public void testGetCountIn() {
    int expectedReturn = 1;
    updateMceventLog.incCountIn();
    int actualReturn = updateMceventLog.getCountIn();
    assertEquals("incCountIn", expectedReturn, actualReturn);
  }//~testGetCountIn()

  public void testGetCountOut() {
    int expectedReturn = 1;
    updateMceventLog.incCountOut();
    int actualReturn = updateMceventLog.getCountOut();
    assertEquals("incCountOut", expectedReturn, actualReturn);
  }//~testGetCountOut()

  public void testSetDate() {
    Timestamp date = new Timestamp(System.currentTimeMillis()-Globals.DAY_IN_MILLS);
    updateMceventLog.setDate(date);
  }//~testSetDate()

  public void testSetPulseIn() {
    Long pulseIn = new Long(10);
    updateMceventLog.setPulseIn(pulseIn);
    updateMceventLog.setPulseOut(pulseIn);
  }//~testSetPulseIn()

  public void testUpisi() throws ExitException {
    char status = 'W';
    updateMceventLog.upisi(status);
  }//~testUpisi()

}//~class TestUpdateMceventLog
