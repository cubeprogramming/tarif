package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import java.util.*;
import tarif.rating.obracun.*;

public class TestDataExtract
    extends TestCase {
  private DataExtract dataExtract = null;

  public TestDataExtract(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    Main.ratingTables = new RatingTables(jDBCPool.getConnection());

    //Postavi datum poziva
    Calendar calendar = Calendar.getInstance();
     //set(int year, int month, int date, int hour, int minute, int second)
    calendar.set(2005,3,6, 17,34,9);
    Timestamp datumv = new Timestamp(calendar.getTimeInMillis());
    dataExtract = new DataExtract(datumv);
  }

  protected void tearDown() throws Exception {
    dataExtract = null;
    super.tearDown();
  }

  public void testFindData() {
    Long zona_id = new Long(0);
    Long plan_id = new Long(87);
    VdbAnalizaBbroj actualReturn = dataExtract.findData(zona_id, plan_id);
    assertNotNull(actualReturn);
    /**@todo fill in the test code*/
  }

}
