package tests;

import junit.framework.*;
import tarif.threads.*;
import tarif.*;
import tarif.utils.*;
import java.util.*;
import java.text.*;
import com.cubeprogramming.utils.*;

public class TestCdrField
    extends TestCase {
  private CdrField cdrField;
  private int fieldID = 0;
  private int offset = 0;
  private int length = 0;
  private String cdr = "0301098300TT98004_20030828_236428.txt             0096451600          00358900975                               008900492                                                                           2003082822474200000082      000             MICOUT  BSC40           00";

  public TestCdrField(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    cdrField = new CdrField();
  }//~setUp()

  protected void tearDown() throws Exception {
    cdrField = null;
    super.tearDown();
  }//~tearDown()

  public void testInitialize() {
    fieldID = 0;
    offset = 0;
    length = 2;
    FieldRec fieldRec = new FieldRec(0,"UNK",0,2,null);
    assertTrue(cdrField.initialize(cdr, fieldRec));
  }//~testInitialize()

  public void testConvertTo() throws ExitException {
    //Provjera konverzije u Integer
    FieldRec fieldRec = new FieldRec(0,"DEC",0,2,null);
    assertTrue(cdrField.initialize(cdr, fieldRec));

    int expectedReturn =  3;
    int actualReturn = ((Long)cdrField.convertTo()).intValue();
    assertEquals(expectedReturn, actualReturn);


    //Provjera konverzije u Integer iz hexadecimalnog stringa
    fieldRec = new FieldRec(1,"HEX",0,2,"");
    cdr = "0B01098300TT98004_20030828_236428.txt             0096451600          00358900975                               008900492                                                                           2003082822474200000082      000             MICOUT  BSC40           00";
    assertTrue(cdrField.initialize(cdr, fieldRec));

    expectedReturn =  11;
    actualReturn = ((Long)cdrField.convertTo()).intValue();
    assertEquals(expectedReturn, actualReturn);

    //Provjera konverzije u Date iz stringa
    fieldRec = new FieldRec(10,"DATE8",196,8,"yyyyMMdd");

    cdr = "0B01098300TT98004_20030828_236428.txt             0096451600          00358900975                               008900492                                                                           2003082822474200000082      000             MICOUT  BSC40           00";
    assertTrue(cdrField.initialize(cdr, fieldRec));

    Date testDate;
    assertNotNull( testDate = (Date)cdrField.convertTo() );
    System.out.println(DateFormat.getDateInstance().format( testDate ) );
  }//~testConvertTo()

}//~class TestCdrField
