package tests;

import junit.framework.*;
import java.io.*;
import tarif.logs.MainLog;
import tarif.*;

public class TestMainLog
    extends TestCase {
  private MainLog mainLog = null;

  public TestMainLog(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
  }

  protected void tearDown() throws Exception {
    mainLog = null;
    super.tearDown();
  }

  public void testMainLog() {
    String fileName = "d:\\home\\cube\\Projects\\cpp\\cdrs\\log\\proba.log";
    Configuration.dateFormat.setTimeZone(Configuration.CET);
    File testFile = new File("testfile.out");
    try {
      mainLog = new MainLog(fileName);
      mainLog.write(testFile,25,"Mali se zabio");
      mainLog.close();
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
  }//~testMainLog()

}//~class TestMainLog
