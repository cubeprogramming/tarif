package tests;

import junit.framework.*;
import java.sql.*;
import tarif.Configuration;
import tarif.JDBCPool;

public class TestConfiguration extends TestCase {
  private Configuration configuration = null;
  private JDBCPool jDBCPool = null;

  public TestConfiguration(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    configuration = new Configuration( jDBCPool.getConnection(), new Integer(8) );
  }//~setUp()

  protected void tearDown() throws Exception {
    configuration = null;
    super.tearDown();
  }//~tearDown()

  public void testConfNumber() throws SQLException {
    int setupID = 7;
    int expectedReturn = 7;
    //int actualReturn = configuration.confNumber(setupID);
    //assertEquals("return value", expectedReturn, actualReturn);
  }//~testConfNumber()

  public void testLoadConf() throws SQLException {
    int setupID = 7;
   /* try {
      configuration.loadConf(setupID);
    }
    catch (SQLException e) {
      fail(e.getMessage());
    } */
  }//~testLoadConf()

  public void testLoadSwitch() throws SQLException {
    int progID = 7;
   /* try {
      configuration.loadSwitch(progID);
    }
    catch (SQLException e) {
      fail(e.getMessage());
    } */
  }//~testLoadSwitch()

  public void testLoadRegion() throws SQLException {
   /* try {
      configuration.loadRegion();
    }
    catch (SQLException e) {
      fail(e.getMessage());
    } */
  }//~testLoadRegion()

  public void testLoadCdrField() throws SQLException {
    int progID = 7;
   /* CdrRec cdrRec = new CdrRec("00",160,"HEX",0,2,329);
    try {
      configuration.loadCdrField(cdrRec, progID);
    }
    catch (SQLException e) {
      fail(e.getMessage());
    } */
  }//~testLoadCdrField()

  public void testLoadCdrRec() throws SQLException {
    int progID = 7;
   /* try {
      configuration.loadCdrRec(progID);
    }
    catch (SQLException e) {
          fail(e.getMessage());
    } */
  }//~testLoadCdrRec()

  public void testLoadConfiguration() throws SQLException {
   /* int setupID = 7;
    try {
      configuration.loadConfiguration(setupID);
    }
    catch (SQLException e) {
      fail(e.getMessage());
    } */
  }//~testLoadConfiguration()

}//~class TestConfiguration
