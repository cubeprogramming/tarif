package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import java.util.*;
import com.cubeprogramming.utils.*;

public class TestSelectParcijalnih
    extends TestCase {
  //Variable se reinicijaliziraju za svaki test, zato mora biti static da zadr�i referencu
  private SelectParcijalnih selectParcijalnih;
  private JDBCPool jdbcPool;

  public TestSelectParcijalnih(String name) throws ExitException, SQLException {
    super(name);
    jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    selectParcijalnih = new SelectParcijalnih(jdbcPool.getConnection(),10);
  }

  protected void setUp() throws Exception {
    super.setUp();
    TimeZone.setDefault(Configuration.GMT);
    Configuration.dateFormat.setTimeZone(Configuration.CET);
  }//~setUp()

  protected void tearDown() throws Exception {
    selectParcijalnih = null;
    super.tearDown();
  }//~tearDown()

  public void testSelect() throws ExitException, SQLException {
    ResultSet rset = selectParcijalnih.select(new Timestamp(System.currentTimeMillis()));
  }//~testSelect()

  public void testFillMeta() throws ExitException, SQLException {
    ResultSet rset = selectParcijalnih.select(new Timestamp(System.currentTimeMillis()));
    assertTrue(rset.next());
    MetaSlog metaSlog = new MetaSlog();
    selectParcijalnih.fillMeta(metaSlog);
    assertTrue(metaSlog.naziv_datoteke != null);
  }//~testFillMeta()

}//~class TestSelectParcijalnih
