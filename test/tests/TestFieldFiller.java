package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.threads.*;
import tarif.*;
import java.util.*;
import com.cubeprogramming.utils.*;

public class TestFieldFiller
    extends TestCase {
  private FieldFiller fieldFiller = null;
  private MetaSlog metaSlog = new MetaSlog();
  private CdrRecord cdrRecord;

  public TestFieldFiller(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    Main.ratingTables = new RatingTables(jDBCPool.getConnection());
    cdrRecord = new CdrRecord(Main.cfg);
    String recordType = "AXE_R16";
    String cdr = "0012101059035        319                                                    00823697402          01004010006697487            000040108            2369740206              697487     0003072609055909064000001200004200000000000010000005000201220    001   000100LC PAG HR11 CN-CJMAND CJMAND 0010200100200697487            23001A0002";
    cdrRecord.initialize(recordType, cdr);
    String fileName = "tt23001_20030726_110855.txt";
    fieldFiller = new FieldFiller(fileName, cdrRecord, metaSlog);
  }

  protected void tearDown() throws Exception {
    fieldFiller = null;
    super.tearDown();
  }

  public void testFillMeta() throws ExitException {
    fieldFiller.fillMeta();
    assertEquals("00",metaSlog.sifra_servisa);
  }

}//~class TestFieldFiller
