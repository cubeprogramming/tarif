package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import com.cubeprogramming.utils.*;

public class TestInsertSumnjivih
    extends TestCase {
  private InsertSumnjivih insertSumnjivih = null;

  public TestInsertSumnjivih(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    insertSumnjivih = new InsertSumnjivih(jDBCPool.getConnection(),50);
  }//~setUp()

  protected void tearDown() throws Exception {
    insertSumnjivih.closeConnection();
    insertSumnjivih = null;
    super.tearDown();
  }//~tearDown()

  public void testUpisi() throws ExitException {
    MetaSlog metaSlog = new MetaSlog();

    metaSlog.naziv_datoteke = "TT52004_20031101_000755.txt";
    metaSlog.call_id = new Long(1853221929);
    metaSlog.mrezna_grupa = "033";
    metaSlog.msisdn = new Long(675599);
    metaSlog.sifra_servisa = "10";
    metaSlog.sifra_sservisa = "FWD";
    metaSlog.pozivni_broj = "***";
    metaSlog.datumv = new Timestamp(System.currentTimeMillis());
    metaSlog.ptrajanje = new Long(2410);
    metaSlog.otrajanje = new Long(2500);
    metaSlog.impulsi = new Long(0);
    metaSlog.iznos = new Float(2.56);
    metaSlog.pcijena = new Float(2.56);
    metaSlog.bbroj_prefiks = "076";
    metaSlog.zona_id = new Long(51);
    metaSlog.katpoziva_id = new Long(10713);
    metaSlog.plan_id = new Long(1);
    metaSlog.tarifa_id = new Long(1);
    metaSlog.pretplatnik_id = new Long("103306755991");
    metaSlog.korisnik_id = new Long("103306755991");
    metaSlog.bgrupa_id = new Long(1);
    metaSlog.paket_id = new Long(1);
    metaSlog.a_category = "0";
    metaSlog.b_category = null;
    metaSlog.type_of_b_num = null;
    metaSlog.charged_party = "0";
    metaSlog.origin_for_charging = null;
    metaSlog.tariff_class = null;
    metaSlog.charging_case = "28";
    metaSlog.charging_case_origin = null;
    metaSlog.flex_uus = new Long(1);
    metaSlog.error_code = "921";
    metaSlog.type_of_a_num = "2";
    metaSlog.file_prefix = "TT52004";

    assertTrue(insertSumnjivih.upisi(metaSlog));

  }//~testUpisi()

}//~class TestInsertSumnivih
