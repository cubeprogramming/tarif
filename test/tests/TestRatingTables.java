package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.*;
import java.sql.*;

public class TestRatingTables
    extends TestCase {
  private RatingTables ratingTables = null;
  private JDBCPool jDBCPool = null;

  public TestRatingTables(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
  }

  protected void tearDown() throws Exception {
    ratingTables = null;
    super.tearDown();
  }

  public void testRatingTables() throws SQLException {
    ratingTables = new RatingTables(jDBCPool.getConnection());
    assertNotNull(ratingTables);
  }

}//~class TestRatingTables
