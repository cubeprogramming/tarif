package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import java.util.*;
import com.cubeprogramming.utils.*;

public class TestSelectSuspendiranih
    extends TestCase {
  //Variable se reinicijaliziraju za svaki test, zato mora biti static da zadr�i referencu
  private SelectSuspendiranih selectSuspendiranih;
  private Connection conn;
  private JDBCPool jdbcPool;

  public TestSelectSuspendiranih(String name) throws ExitException, SQLException {
    super(name);
    jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    TimeZone.setDefault(Configuration.GMT);
    Configuration.dateFormat.setTimeZone(Configuration.CET);
    conn = jdbcPool.getConnection();
  }

  protected void setUp() throws Exception {
    super.setUp();
    selectSuspendiranih = new SelectSuspendiranih(conn,10);
    selectSuspendiranih = new SelectSuspendiranih(conn,10,"MAR2005");
    selectSuspendiranih = new SelectSuspendiranih(conn,10,new Timestamp(System.currentTimeMillis()-Globals.DAY_IN_MILLS*5));
  }//~setUp()

  protected void tearDown() throws Exception {
    selectSuspendiranih = null;
    super.tearDown();
  }//~tearDown()

  public void testSelect() throws ExitException, SQLException {
    ResultSet rset = selectSuspendiranih.select();
  }//~testSelect()

  public void testFillMeta() throws ExitException, SQLException {
    MetaSlog metaSlog = new MetaSlog();
    ResultSet rset = selectSuspendiranih.select();
    assertTrue(rset.next());
    selectSuspendiranih.fillMeta(metaSlog);
    assertTrue(metaSlog.naziv_datoteke != null);
  }//~testFillMeta()

}//~class TestSelectSuspendiranih
