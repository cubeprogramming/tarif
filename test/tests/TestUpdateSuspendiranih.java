package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.*;
import java.sql.*;

public class TestUpdateSuspendiranih
    extends TestCase {
  private UpdateSuspendiranih updateSuspendiranih = null;
  private SelectSuspendiranih selectSuspendiranih;
  private ResultSet rset;
  private Connection conn;
  private MetaSlog metaSlog;

  public TestUpdateSuspendiranih(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    //Main.jdbcPool = new JDBCPool("ITBRI", "htbisa", "ri"); //RADI! in na Oracle8i sa Client 9i
    conn = Main.jdbcPool.getConnection();
    selectSuspendiranih = new SelectSuspendiranih(conn,10);
    rset = selectSuspendiranih.select();
    rset.next();
    metaSlog = new MetaSlog();
    selectSuspendiranih.fillMeta(metaSlog);
    updateSuspendiranih = new UpdateSuspendiranih(rset);
  }//~setUp()

  protected void tearDown() throws Exception {
    conn.commit();
    rset.close();
    updateSuspendiranih = null;
    super.tearDown();
  }//~tearDown()

  public void testUpdate() throws SQLException {
    updateSuspendiranih.update(metaSlog);
  }//~testUpdate()

}//~class TestUpdateSuspendiranih
