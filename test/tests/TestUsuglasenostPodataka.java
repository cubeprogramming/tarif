package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;


public class TestUsuglasenostPodataka
    extends TestCase {
  private UsuglasenostPodataka usuglasenostPodataka = null;

  public TestUsuglasenostPodataka(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    Main.ratingTables = new RatingTables(jDBCPool.getConnection());
    usuglasenostPodataka = new UsuglasenostPodataka();
  }

  protected void tearDown() throws Exception {
    usuglasenostPodataka = null;
    super.tearDown();
  }

  public void testIsUsuglasen() {
    MetaSlog metaSlog = new MetaSlog();

    //Standardni paketi usluga osim govornica
    metaSlog.paket_id = new Long(1);
    metaSlog.type_of_a_num = "0";
    assertTrue(usuglasenostPodataka.isUsuglasen(metaSlog));

    //Govornice
    metaSlog.paket_id = new Long(2);
    metaSlog.type_of_a_num = "0";
    metaSlog.a_category = "11";
    metaSlog.origin_for_charging = "0002";
    assertTrue(usuglasenostPodataka.isUsuglasen(metaSlog));

    //BGS
    metaSlog.paket_id = new Long(3);
    metaSlog.origin_for_charging = "0007";
    assertTrue(usuglasenostPodataka.isUsuglasen(metaSlog));

    //Kabinski pretplatnici
    metaSlog.paket_id = new Long(4);
    metaSlog.a_category = "13";
    metaSlog.origin_for_charging = "0006";
    assertTrue(usuglasenostPodataka.isUsuglasen(metaSlog));

    //ISDN
    metaSlog.paket_id = new Long(6);
    metaSlog.type_of_a_num = "2";
    assertTrue(usuglasenostPodataka.isUsuglasen(metaSlog));

    //Ostalo
    metaSlog.paket_id = new Long(5);
    assertTrue(usuglasenostPodataka.isUsuglasen(metaSlog));

    //Suspendiranje
    metaSlog.paket_id = new Long(15); //Standardni paket usluga - Ostali
    assertTrue(!usuglasenostPodataka.isUsuglasen(metaSlog));

  }//~testIsUsuglasen()

}//~class TestUsuglasenostPodataka
