package tests;

import junit.framework.*;
import tarif.rating.*;
import tarif.utils.*;
import tarif.*;
import java.sql.*;
import com.cubeprogramming.utils.*;

public class TestAnalizaBbroja
    extends TestCase {
  private AnalizaBbroja analizaBbroja = null;

  public TestAnalizaBbroja(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    JDBCPool jDBCPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( jDBCPool.getConnection(), new Integer(8) );
    Main.ratingTables = new RatingTables(jDBCPool.getConnection());
    analizaBbroja = new AnalizaBbroja();
  }

  protected void tearDown() throws Exception {
    analizaBbroja = null;
    super.tearDown();
  }

  public void testBbrojPrefiks() throws ExitException {
    MetaSlog metaSlog = new MetaSlog();
    metaSlog.mrezna_grupa = "033";
    //metaSlog.paket_id = new Long(50); //Ostali - mini Halo
    metaSlog.paket_id = new Long(1); //Standardni paket usluga - Ostali
    metaSlog.datumv = new Timestamp(System.currentTimeMillis()-Globals.DAY_IN_MILLS*20);
    //metaSlog.trajanje = new Timestamp(10000); //10 Sekundi
    //metaSlog.trajanje = new Timestamp(5*60*100 + 10 * 1000); //5h i 10 Sekundi
    metaSlog.trajanje = new Timestamp(Globals.DAY_IN_MILLS*3 + 10 * 1000); //24h i 10 Sekundi - pada na ovom

    //Test bbroj_prefiks lokaniPoziv()
    metaSlog.pozivni_broj = "400064";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks mreznaGrupa()
    metaSlog.pozivni_broj = "051274214";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks mreznaGrupa() -- suspendiran
    metaSlog.pozivni_broj = "03394";
    assertTrue(!analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks mreznaGrupa() -- VAS broj
    metaSlog.pozivni_broj = "033985";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks zemljaMspecial()
    metaSlog.pozivni_broj = "00486076767676";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks tVas()
    metaSlog.pozivni_broj = "94";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks tInvas()
    metaSlog.pozivni_broj = "*#21";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks tInvas() - bbroj_ind = 'I'
    metaSlog.pozivni_broj = "*31767676";
    assertTrue(analizaBbroja.analiza(metaSlog));
    //Test bbroj_prefiks tInvas() - bbroj_ind = '1'
    metaSlog.pozivni_broj = "*#51767676";
    assertTrue(analizaBbroja.analiza(metaSlog));
  }//~testBbrojPrefiks()

}//~class TestAnalizaBbroja
