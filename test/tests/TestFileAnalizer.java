package tests;

import junit.framework.*;
import tarif.threads.*;
import tarif.rating.*;
import tarif.logs.*;
import tarif.*;
import java.io.*;
import java.util.*;
import com.cubeprogramming.thrdmonitor.*;

public class TestFileAnalizer
    extends TestCase implements ThreadListener{
  private ObradaParcijalnih fileAnalizer;
  private ThreadGroup testThrdGrp = new ThreadGroup("testThrdGrp");
  private Monitor monitor = Monitor.getMonitor(testThrdGrp);
  private String fileName1, fileName2;

  public TestFileAnalizer(String name) throws Exception {
    super(name);
    Configuration.dateFormat.setTimeZone(Configuration.CET);
    System.out.println("---- Startano: " + Configuration.dateFormat.format(new java.util.Date( ) ) );
    Main.jdbcPool = new JDBCPool("FRIDA", "arate", "rato");
    Main.cfg = new Configuration( Main.jdbcPool.getConnection(), new Integer(8) );
    Main.ratingTables = new RatingTables(Main.jdbcPool.getConnection());
    Main.DBConfig = (Map)Main.cfg.configHash.get(Configuration.DB_CONFIG);
    String logPath = (String)Main.DBConfig.get(Globals.LOG_DIR);
    String logMain = (String)Main.DBConfig.get(Globals.LOG_FILE);
    String fileLog = (String)Main.DBConfig.get(Globals.DEBUG_LOG);
    Main.mainLog = new MainLog(logPath + logMain);
    Main.debugLog = new LogFile(logPath + fileLog); //DEBUG
    fileName1 = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\test\\cdr-in\\TestFileAnalizer\\tt31001_20041001_001845.txt";
    fileName2 = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\test\\cdr-in\\TestFileAnalizer\\tt31001_20041001_001846.txt";
    monitor.addThreadListener(this);
    //fileAnalizer = new FileAnalizer(testThrdGrp, new File(fileName1) );
  }

  protected void setUp() throws Exception {
    super.setUp();
  }//~setUp()

  protected void tearDown() throws Exception {
    //fileAnalizer = null;
    System.out.println("---- Zavrseno: " + Configuration.dateFormat.format(new java.util.Date( ) ) );
    super.tearDown();
  }//~tearDown()

  public void testRun() {
    try {
      new FileAnalizer(testThrdGrp, new File(fileName1) );
      //fileAnalizer = new FileAnalizer(testThrdGrp, new File(fileName2) );
    }
    catch (Exception e) {
      fail(e.getMessage());
    }
  }//~testRun()

  public void notified(ThreadEvent thrdEvent) {
    if (thrdEvent.getSourceThread().getName().indexOf("notify") != -1) {
      System.out.println("Notification in TestCase received: " + thrdEvent.toString());
    }else {
      System.out.println("Notification in TestCase received: " + thrdEvent.toString());
    }
  }

  public TestFileAnalizer() {
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception {
  }////~notified()

}//~class TestFileAnalizer
