package tests;

import junit.framework.*;

public class AllTests
    extends TestCase {

  public AllTests(String s) {
    super(s);
  }

  public static Test suite() {
    TestSuite suite = new TestSuite();
    suite.addTestSuite(TestJDBCPool.class);
    //suite.addTestSuite(TestConfiguration.class);
    suite.addTestSuite(TestMainLog.class);
    suite.addTestSuite(TestCdrFile.class);
    suite.addTestSuite(TestCdrField.class);
    suite.addTestSuite(TestCdrRecord.class);
    suite.addTestSuite(TestRatingTables.class);
    suite.addTestSuite(TestFieldFiller.class);
    suite.addTestSuite(TestAnalizaServisa.class);
    suite.addTestSuite(TestInsertParcijalnih.class);
    suite.addTestSuite(TestAnalizaAbroja.class);
    suite.addTestSuite(TestAnalizaBbroja.class);
    suite.addTestSuite(TestInsertSuspendiranih.class);
    suite.addTestSuite(TestUsuglasenostPodataka.class);
    suite.addTestSuite(TestInsertSumnjivih.class);
    suite.addTestSuite(TestInsertTarifiranih.class);
    suite.addTestSuite(TestInsertProcessLog.class);
    suite.addTestSuite(TestUpdateProcessLog.class);
    suite.addTestSuite(TestInsertMceventLog.class);
    suite.addTestSuite(TestUpdateMceventLog.class);
    suite.addTestSuite(TestFileAnalizer.class);
    suite.addTestSuite(TestSelectSuspendiranih.class);
    suite.addTestSuite(TestUpdateSuspendiranih.class);
    suite.addTestSuite(TestObradaSuspendiranih.class);
    suite.addTestSuite(TestSelectParcijalnih.class);
    suite.addTestSuite(TestSpajanjeParcijalnih.class);
    suite.addTestSuite(TestObradaParcijalnih.class);
    return suite;
  }
}//~class AllTests
