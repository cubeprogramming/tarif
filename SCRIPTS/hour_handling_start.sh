#!/bin/sh
#
# Skripta za pokretanje baznog procesa satne obrade.
#

if [ $# -lt 4 ]; then
   echo "usage: $0 <connect_string> <ociklus> <parallel_degree> <itbprocesslog_id>"
   exit 5
fi

CONNECT_STRING=$1;

#echo $CONNECT_STRING;

if [ -f ../cdrs/logs/sqlplus.log ]; then
  #echo "brisem datoteku";
  rm ../cdrs/logs/sqlplus.log;
fi

touch ../cdrs/logs/sqlplus.log

sqlplus -s //nolog >../cdrs/logs/sqlplus.log <<!
whenever sqlerror exit FAILURE;
set termout off;
set heading off;
set feedback off;
conn $CONNECT_STRING
exec tft_hour_handling.tft_hour_handling($3,$4);
exec tft_hour_handling.aggregate('$2');
exit;
! 

EXIT_STATUS=$?

#echo "exit status: " $EXIT_STATUS;

if [ $EXIT_STATUS -ne 0 ]; then
  cat ../cdrs/logs/sqlplus.log;
  exit 1;
fi


echo "done"
exit 0
