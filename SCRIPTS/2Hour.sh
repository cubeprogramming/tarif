#!/bin/sh
#
# Dnevni ciklus obrade
#

echo "Dvosatni ciklus obrade";
echo "v 1.0.0";
echo "Dvosatni ciklus obrade">>$BILLING_ROOT/IPC/info.dat
echo "v 1.0.0">>$BILLING_ROOT/IPC/info.dat

# PROVJERA KOMPLETNOG ZAUSTAVLJANJA

if [ -f $BILLING_ROOT/IPC/2Hour.stop ]; then
	rm -f $BILLING_ROOT/IPC/2Hour.stop
	echo "Zaustavljeno automatsko pokretanje dvosatnog ciklusa obrade">>$BILLING_ROOT/IPC/info.dat
	echo "Zaustavljeno automatsko pokretanje dvosatnog ciklusa obrade"
	exit 0;
fi

# KRON PROVJERA
# Ovdje provjeravamo da li je neki proces billinga vec u toku
# ako nije radimo posao kao da nista nije bilo
# ako jeste narucimo se opet za 15 minuta

if [ -f $BILLING_ROOT/IPC/*.running ]; then
	$HOME/client 'Billing 2Hour' 15
	exit 0;
fi

# KRAJ KRON PROVJERE

# PREDPROCESNE VALIDACIJE
if [ "$1" = "" ]; then
	echo "usage: $0 <processlog_id> <no processes>"
	exit 5
fi
if [ "$1" -gt '0' ]; then
	echo
else
	echo "Prvi parametar mora biti pozitivan broj"
	exit 5
fi
if [ "$2" = "" ]; then
	echo "usage: $0 <processlog_id> <no processes>"
	exit 5
fi
if [ "$2" -gt '0' ]; then
	echo
else
	echo "Drugi parametar mora biti pozitivan broj"
	exit 5
fi

ID=$1
NO_PROCESSES=$2
# END PREDPROCESNE VALIDACIJE

#PROVJERA STANJA RESURSA

echo "Provjera dostupnosti baze..."
echo "Provjera dostupnosti baze...">>$BILLING_ROOT/IPC/info.dat
$HOME/bin/oracle_available $CONNECT_STRING
EXIT_STATUS=$?

if [ $EXIT_STATUS -eq '1' ]; then
	echo "Baza nije dostupna. Pokretanje dvosatnog ciklusa odgodjeno za 30 minuta."
	echo "Baza nije dostupna. Pokretanje dvosatnog ciklusa odgodjeno za 30 minuta.">>$BILLING_ROOT/IPC/info.dat
	$HOME/client 'Billing 2Hour' 30
	exit 0
fi

echo "Baza dostupna."
echo "Baza dostupna.">>$BILLING_ROOT/IPC/info.dat

TMP_2Hour_FILE=`date '+/tmp/TMP_2Hour_%y%m%d_%H%M%S.txt'`

echo "Provjera stanja diskova..."
echo "Provjera stanja diskova...">>$BILLING_ROOT/IPC/info.dat

$HOME/bin/disk_free_space 300000 /usr1 /usr4 /usr5 >> `echo $TMP_2Hour_FILE` 2>> `echo $TMP_2Hour_FILE`
EXIT_STATUS=$?

if [ $EXIT_STATUS -eq '1' ]; then
	cat `echo $TMP_2Hour_FILE`
	cat `echo $TMP_2Hour_FILE`>>$BILLING_ROOT/IPC/info.dat
	echo "Dvosatni cilkus obrade zaustavljen."
	echo "Dvosatni cilkus obrade zaustavljen.">>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Nedovoljno diskovnog prostora. Dvosatni cilkus obrade zaustavljen."
	echo "Nedovoljno diskovnog prostora. Dvosatni cilkus obrade zaustavljen." >>`echo $TMP_2Hour_FILE`
	cat `echo $TMP_2Hour_FILE`|mailx -s "2Hour skripta: disk space" itboper@sunce.ri.ht.hr
	rm -f `echo $TMP_2Hour_FILE` 2>/dev/null
	exit 1
fi

echo "Diskovi OK."
echo "Diskovi OK.">>$BILLING_ROOT/IPC/info.dat

sqlplus -S $CONNECT_STRING <<!
declare
 v_exit number;
Begin
 select
 decode(sign(sysdate-(last_day(trunc(add_months(sysdate,-1)))+1-1/86400+get_sekunde(udr_offset,'US')/86400)),-1,1,0) into v_exit
 from itb_setup;

 if v_exit=1 then
  update itb_setup set udr_offset = 480000;
  commit;
 end if;
End;
/

!
#KRAJ PROVJERE STANJA RESURSA

#POSTAVLJANJE UDR OFFSETA NA 48 SATI AKO SMO U UDR OFSETU
#KRAJ POSTAVLJANJA UDR OFSETA


# POCETAK PROCESA
touch $BILLING_ROOT/IPC/2Hour.running 
$BILLING_ROOT/SCRIPTS/status_report $ID "Pocetak dvosatnog ciklusa obrade";

check_stop()
{
if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
	echo "Zaustavljen dvosatni ciklus obrade";
	echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
	rm -f $BILLING_ROOT/IPC/stop.dat
	rm $BILLING_ROOT/IPC/2Hour.running
	rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
	exit 4;
fi
}

# POCETAK PROCESA VALIDACIJE

rm -f `echo $TMP_2Hour_FILE` 2>/dev/null
if [ -f $HOME/ARBOR_IPC/valctl.ctl ]; then
#VALIDACIJA TT DATOTEKA
	echo "Pocetak validacije TT datoteka."
	echo "Pocetak validacije TT datoteka.">>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Pocetak validacije TT datoteka."
	$HOME/mgw/bin/validacija >> `echo $TMP_2Hour_FILE` 2>> `echo $TMP_2Hour_FILE`
	EXIT_STATUS=$?
	cat `echo $TMP_2Hour_FILE`
	cat `echo $TMP_2Hour_FILE`>>$BILLING_ROOT/IPC/info.dat
	if [ $EXIT_STATUS -eq '0' ]; then
		echo "Validacija TT datoteka zavrsena."
		echo "Validacija TT datoteka zavrsena.">>$BILLING_ROOT/IPC/info.dat
		$BILLING_ROOT/SCRIPTS/status_report $ID "Validacija TT datoteka zavrsena."
	elif [ $EXIT_STATUS -eq '2' ]; then
		echo "Nema ulaznih TT datoteka. Validacija TT datoteka preskocena."
		echo "Nema ulaznih TT datoteka. Validacija TT datoteka preskocena.">>$BILLING_ROOT/IPC/info.dat
		$BILLING_ROOT/SCRIPTS/status_report $ID "Nema ulaznih TT datoteka. Validacija TT datoteka preskocena."
	else 
		echo "Greska pri validaciji TT datoteka. Dvosatni ciklus obrade zaustavljen."
		echo "Greska pri validaciji TT datoteka. Dvosatni ciklus obrade zaustavljen.">>$BILLING_ROOT/IPC/info.dat
		$BILLING_ROOT/SCRIPTS/status_report $ID "Greska pri validaciji TT datoteka. Dvosatni ciklus obrade zaustavljen."
      		echo "Greska pri validaciji TT datoteka. Dvosatni ciklus obrade zaustavljen." >>`echo $TMP_2Hour_FILE`
		cat `echo $TMP_2Hour_FILE`|mailx -s "2Hour skripta: validacija TT datoteka" itboper@sunce.ri.ht.hr
		rm -f $BILLING_ROOT/IPC/2Hour.running
		rm -f `echo $TMP_2Hour_FILE` 2>/dev/null
		exit 1
	fi
	
#VALIDACIJA MMS DATOTEKA
	rm -f `echo $TMP_2Hour_FILE` 2>/dev/null
	echo "Pocetak validacije MMS datoteka."
	echo "Pocetak validacije MMS datoteka.">>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Pocetak validacije MMS datoteka."
	$HOME/mgw/bin/validacija_MMS >> `echo $TMP_2Hour_FILE` 2>> `echo $TMP_2Hour_FILE`
	EXIT_STATUS=$?
	cat `echo $TMP_2Hour_FILE`
	cat `echo $TMP_2Hour_FILE`>>$BILLING_ROOT/IPC/info.dat
	if [ $EXIT_STATUS -eq '0' ]; then
			echo "Validacija MMS datoteka zavrsena."
			echo "Validacija MMS datoteka zavrsena.">>$BILLING_ROOT/IPC/info.dat
			$BILLING_ROOT/SCRIPTS/status_report $ID "Validacija MMS datoteka zavrsena."
		elif [ $EXIT_STATUS -eq '2' ]; then
			echo "Nema ulaznih MMS datoteka. Validacija MMS datoteka preskocena."
			echo "Nema ulaznih MMS datoteka. Validacija MMS datoteka preskocena.">>$BILLING_ROOT/IPC/info.dat
			$BILLING_ROOT/SCRIPTS/status_report $ID "Nema ulaznih MMS datoteka. Validacija MMS datoteka preskocena."
		else 
			echo "Greska pri validaciji MMS datoteka. Dvosatni cilkus obrade zaustavljen."
			echo "Greska pri validaciji MMS datoteka. Dvosatni cilkus obrade zaustavljen.">>$BILLING_ROOT/IPC/info.dat
			$BILLING_ROOT/SCRIPTS/status_report $ID "Greska pri validaciji MMS datoteka. Dvosatni cilkus obrade zaustavljen."
	      		echo "Greska pri validaciji MMS datoteka. Dvosatni cilkus obrade zaustavljen." >>`echo $TMP_2Hour_FILE`
			cat `echo $TMP_2Hour_FILE`|mailx -s "2Hour skripta: validacija MMS datoteka" itboper@sunce.ri.ht.hr
			rm -f $BILLING_ROOT/IPC/2Hour.running
			rm -f `echo $TMP_2Hour_FILE` 2>/dev/null
			exit 1
	fi
else
	echo "Validacije rucno zaustavljene (valctl). Validacije TT i MMS preskocene."
	echo "Validacije rucno zaustavljene (valctl). Validacije TT i MMS preskocene.">>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Validacije rucno zaustavljene (valctl). Validacije TT i MMS preskocene."
fi

rm -f `echo $TMP_2Hour_FILE` 2>/dev/null

# END PROCESA VALIDACIJE


# POCETAK PROCESA KONVERZIJE
#COUNT=`ls -1 $BILLING_ROOT/CDR-IN/*.zip 2>/dev/null | wc -l`
COUNT=`ls -1 $BILLING_ROOT/CDR-IN/*.txt 2>/dev/null | wc -l`
if [ $COUNT -eq '0' ] ; then 
	echo "Nema datoteka u direktoriju $BILLING_ROOT/CDR-IN/ -Konverzija preskocena "
 	echo "Nema datoteka u direktoriju $BILLING_ROOT/CDR-IN/ -Konverzija preskocena ">>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Nema datoteka u direktoriju $BILLING_ROOT/CDR-IN/ -Konverzija preskocena "
else 
 	if [ $COUNT -lt $NO_PROCESSES ] ; then 
  		NO_PROCESSES=$COUNT
	fi

	$BILLING_ROOT/SCRIPTS/status_report $ID "Pocetak ISOKonverzije";
	echo "Pocetak ISOKonverzije" 
	echo "ISOKonverzija u toku ..." >>$BILLING_ROOT/IPC/info.dat
	COUNT=0
#	for FILE_NAME in `ls -1 $BILLING_ROOT/CDR-IN/*.zip 2>/dev/null | awk 'BEGIN {FS = "/";}  {print $NF;}' - `
	for FILE_NAME in `ls -1 $BILLING_ROOT/CDR-IN/*.txt 2>/dev/null | awk 'BEGIN {FS = "/";}  {print $NF;}' - `
 	 do
	 	echo $FILE_NAME>>$BILLING_ROOT/IPC/proc$COUNT.txt
		COUNT=`expr $COUNT + 1`
		COUNT=`expr $COUNT % $NO_PROCESSES`
	done
	COUNT=0
	while [ $COUNT -lt $NO_PROCESSES ]
 	 do
		exec $BILLING_ROOT/SCRIPTS/konverzija $ID proc$COUNT.txt &
		COUNT=`expr $COUNT + 1`
	done
	wait

        cd $BILLING_ROOT/OLD/CDR-IN
        $HOME/bin/zip_sve
        result=`echo $?`
        if [ "$result" -eq "1" ]; then
        if [ $NACIN -eq '0' ] ; then
             $BILLING_ROOT/SCRIPTS/status_report $PCID "Greska kod zipanja fajlova u CDR-IN folderu"
        fi
             echo "Greska kod zipanja fajlova u CDR-IN folderu" >>$BILLING_ROOT/IPC/info.dat
        fi
        cd $BILLING_ROOT/PROGRAMS

	# ukoliko ima stop.dat treba zaustaviti proces 
	if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
		echo "Proces konverzije zaustavljen"
		echo "Konverzija zaustavljena" >>$BILLING_ROOT/IPC/info.dat
		echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
  		$BILLING_ROOT/SCRIPTS/status_report $ID "Konverzija zaustavljena"
		$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
  		rm -f $BILLING_ROOT/IPC/stop.dat
		rm $BILLING_ROOT/IPC/2Hour.running
		rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
		exit 4;
	fi
	$BILLING_ROOT/SCRIPTS/status_report $ID "Kraj ISOKonverzije";    
	echo "Kraj ISOKonverzije">>$BILLING_ROOT/IPC/info.dat
	echo "Kraj ISOKonverzije"
fi
# END PROCESA KONVERZIJE

# POCETAK PROCESA TARIFIRANJA
if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
	echo "Zaustavljen dvosatni ciklus obrade";
	echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
	rm -f $BILLING_ROOT/IPC/stop.dat
	rm $BILLING_ROOT/IPC/2Hour.running
	rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
	exit 4;
fi
NO_PROCESSES=$2
COUNT=`ls -1 $BILLING_ROOT/TARIFF/*.?00 |wc -l`
if [ $COUNT -eq '0' ] ; then 
	echo "Nema datoteka u direktoriju $BILLING_ROOT/TARIFF/ - Tarifiranje preskoceno"
	echo "Nema datoteka u direktoriju $BILLING_ROOT/TARIFF/ - Tarifiranje preskoceno" >>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Nema datoteka u direktoriju $BILLING_ROOT/TARIFF/ - Tarifiranje preskoceno"
else
	if [ $COUNT -lt $NO_PROCESSES ] ; then 
  		NO_PROCESSES=$COUNT
	fi
	$BILLING_ROOT/SCRIPTS/status_report $ID "Pocetak tarifiranja ";    
	echo "Pocetak tarifiranja"
	echo "Tarifiranje u toku...">>$BILLING_ROOT/IPC/info.dat
	COUNT=0
	
	#parsiranje datoteka po procesima
	for FILE_NAME in `ls -1 $BILLING_ROOT/TARIFF/*.?00 2>/dev/null | awk 'BEGIN {FS = "/";}  {print $NF;}' - `
 	  do
    		echo $FILE_NAME>>$BILLING_ROOT/IPC/proc$COUNT.txt
		COUNT=`expr $COUNT + 1`
		COUNT=`expr $COUNT % $NO_PROCESSES`
	done
	
	# startanje procesa tarifiranja
	COUNT=0
	while [ $COUNT -lt $NO_PROCESSES ]
	 do
		exec $BILLING_ROOT/SCRIPTS/rating $ID proc$COUNT.txt &
		COUNT=`expr $COUNT + 1`
	done
 	wait
	
	# ukoliko ima stop.dat treba zaustaviti proces 
	if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
  		echo "Proces tarifiranja zaustavljen"
		echo "Tarifiranje zaustavljeno">>$BILLING_ROOT/IPC/info.dat
		echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
		$BILLING_ROOT/SCRIPTS/status_report $ID "Tarifiranje zaustavljeno"
		$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
		rm -f $BILLING_ROOT/IPC/stop.dat
		rm $BILLING_ROOT/IPC/2Hour.running
		rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
		exit 4;
	fi
	$BILLING_ROOT/SCRIPTS/status_report $ID "Kraj tarifiranja";    
	echo "Kraj tarifiranja">>$BILLING_ROOT/IPC/info.dat
	echo "Kraj tarifiranja"
fi
# END PROCESA TARIFIRANJA	

# POCETAK PROCESA SPAJANJA PARCIJALNIH
# ukoliko ima stop.dat treba zaustaviti proces 
if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
	echo "Zaustavljen dvosatni ciklus obrade";
	echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
	rm -f $BILLING_ROOT/IPC/stop.dat
	rm $BILLING_ROOT/IPC/2Hour.running
	rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
	exit 4;
fi
echo "Pocetak procesa spajanja parcijalnih ";
echo "Proces spajanja parcijalnih u toku... ">>$BILLING_ROOT/IPC/info.dat
$BILLING_ROOT/SCRIPTS/spajanje_parcijalnih_parallel $ID $NO_PROCESSES
echo "Zavrseno spajanje parcijalnih ";
echo "Zavrseno spajanje parcijalnih ">>$BILLING_ROOT/IPC/info.dat
# END PROCESA SPAJANJA PARCIJALNIH

# POCETAK SUSPLOAD4BONUS
# ukoliko ima stop.dat treba zaustaviti proces 
if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
	echo "Zaustavljen dvosatni ciklus obrade";
	echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
	rm -f $BILLING_ROOT/IPC/stop.dat
	rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
	rm $BILLING_ROOT/IPC/2Hour.running
	exit 4;
fi
echo "Pocetak ucitavanja suspendovanih za bonuse ";
echo "Proces ucitavanja suspendovanih za bonuse u toku... ">>$BILLING_ROOT/IPC/info.dat

sqlplus -S $CONNECT_STRING <<!
set termout off;
set heading off;
set feedback off;
truncate table tsbtraffic;
!

$BILLING_ROOT/SCRIPTS/suspload4bonus $ID
echo "Zavrseno ucitavanje suspendovanih za bonuse ";
echo "Zavrseno ucitavanje suspendovanih za bonuse ">>$BILLING_ROOT/IPC/info.dat
# END SUSPLOAD4BONUS


# POCETAK PLSQL BLOKA PROCEDURA
if [ -f $BILLING_ROOT/IPC/stop.dat ]; then
	echo "Zaustavljen dvosatni ciklus obrade";
	echo "Zaustavljen dvosatni ciklus obrade" >>$BILLING_ROOT/IPC/info.dat
	$BILLING_ROOT/SCRIPTS/status_report $ID "Zaustavljen dvosatni ciklus obrade";
	rm -f $BILLING_ROOT/IPC/stop.dat
	rm $BILLING_ROOT/IPC/2Hour.running
	rm $BILLING_ROOT/IPC/2Hour.stop 2>/dev/null
	exit 4;
fi

echo "Pocetak DB obrada ";
echo "Pocetak DB obrada ">>$BILLING_ROOT/IPC/info.dat
$BILLING_ROOT/SCRIPTS/status_report $ID "Pocetak DB obrada";

OCIKLUS=`
sqlplus -S $CONNECT_STRING <<!
set termout off;
set heading off;
set feedback off;
select to_char(sysdate,'MONRRRR') from dual;
!
`

# Ovime maknemo sve CR i LF iz OCIKLUS
OCIKLUS=`echo $OCIKLUS | tr -d '\12\13' -`
OCIKLUS=\'$OCIKLUS\'

sqlplus -S $CONNECT_STRING <<!
set termout off;
set heading off;
set feedback off;
exec tft_hour_handling.aggregate(0,$ID,$OCIKLUS);
!


echo "Kraj DB obrada ";
echo "Kraj DB obrada ">>$BILLING_ROOT/IPC/info.dat
$BILLING_ROOT/SCRIPTS/status_report $ID "Kraj DB obrada";
# END PLSQL BLOKA PROCEDURA

$BILLING_ROOT/SCRIPTS/status_report $ID "Kraj dvosatnog ciklusa obrade";
echo "Kraj dvosatnog ciklusa obrade"
echo "Kraj dvosatnog ciklusa obrade">>$BILLING_ROOT/IPC/info.dat
rm $BILLING_ROOT/IPC/2Hour.running



# Prije nego se narucimo provjerimo ima li zahtjev za zaustavljanjem

if [ -f $BILLING_ROOT/IPC/2Hour.stop ]; then
	rm -f $BILLING_ROOT/IPC/2Hour.stop
	echo "Zaustavljeno automatsko pokretanje dvosatnog ciklusa obrade">>$BILLING_ROOT/IPC/info.dat
	echo "Zaustavljeno automatsko pokretanje dvosatnog ciklusa obrade"
	exit 0;
fi


# PONOVNO NARUCIVANJE U SCHEDULIRANO VRIJEME
# Ovdje se ova skripta sama narucuje da se izvrsi
# u sljedecem trenutku koji je odredjen schedulingom

CURRENT_HHMM=`date +%H:%M`
CURRENT_HOUR=`echo $CURRENT_HHMM | cut -f1 -d:`
CURRENT_MINUTE=`echo $CURRENT_HHMM | cut -f2 -d:`
CURRENT_TIME=`expr $CURRENT_HOUR \* 60`
CURRENT_TIME=`expr $CURRENT_TIME + $CURRENT_MINUTE`


FIRST_ORDER=`echo $TWOHOUR_SCHEDULE_TIME | awk ' {print $1} ' -`
ORDER_HHMM=$FIRST_ORDER
FIRST_ORDER=`echo $FIRST_ORDER | tr -d ':'`
LAST_ORDER=`echo $TWOHOUR_SCHEDULE_TIME | awk ' {print $NF} ' - | tr -d ':'`

CURRENT_HHMM=`echo $CURRENT_HHMM | tr -d ':'`

if [ $CURRENT_HHMM -gt $FIRST_ORDER ]; then
	if [ $CURRENT_HHMM -lt $LAST_ORDER ]; then
		for SCHEDULED_HHMM in $TWOHOUR_SCHEDULE_TIME
		do
			SCHEDULED_HHMM_TR=`echo $SCHEDULED_HHMM | tr -d ':'`
			if [ $SCHEDULED_HHMM_TR -gt $CURRENT_HHMM ]; then
				ORDER_HHMM=$SCHEDULED_HHMM
				break
			fi
		done
	fi
fi

ORDER_HOUR=`echo $ORDER_HHMM | cut -f1 -d:`
ORDER_MINUTE=`echo $ORDER_HHMM | cut -f2 -d:`
ORDER_TIME=`expr $ORDER_HOUR \* 60`
ORDER_TIME=`expr $ORDER_TIME + $ORDER_MINUTE`

ORDER_MINUTES=`expr $ORDER_TIME - $CURRENT_TIME`

# Provjerimo je li minutaza negativna
# Ako jeste to znaci da je narudzba sljedeci dan pa je korigujemo za
# onoliko minuta koliko ih ima u 24 sata
if [ $ORDER_MINUTES -lt '0' ]; then
	ORDER_MINUTES=`expr $ORDER_MINUTES + 1440`
fi

if [ $ORDER_MINUTES -eq '0' ]; then
	ORDER_MINUTES=1
fi

#date +%H:%M
#echo $TWOHOUR_SCHEDULE_TIME
#echo $CURRENT_HOUR:$CURRENT_MINUTE $ORDER_HOUR:$ORDER_MINUTE $ORDER_MINUTES $FIRST_ORDER $LAST_ORDER

echo "Next execution time is $ORDER_HOUR:$ORDER_MINUTE which is in $ORDER_MINUTES minutes"
$HOME/client 'Billing 2Hour' $ORDER_MINUTES

# END PONOVNO NARUCIVANJE

exit 0
# END PROCESA

)
