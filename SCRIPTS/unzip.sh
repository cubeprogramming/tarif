#!/bin/sh
#
# Skripta koja unzipa datoteke u cdr-in direktorij
# Primjer poziva: unzip.sh "tt*_20041028*.zip" ../../../cdr-in 10

echo "Pocetak raspakiranja"

if [ $# -lt 2 ] 
then
    echo "Niste upisali sve parametre";
    exit 1;
fi

lista=$1;
dir=$2;
counter=0

for datoteka in $lista
do 
#    (( counter++ )) ovo je jednako sa:
    counter=`expr $counter + 1`
    wzunzip -yO -o $datoteka $dir 

    if [ "$3" != "" ]
    then
	if [ $3 -eq $counter ]; then
	    echo $counter "files extracted" 
	    exit;
	fi
    fi
done