whenever sqlerror exit FAILURE;
set heading off;
set feedback off;
set flush off;
exec tft_day_handling.tft_day_handling(&3);
exec tft_day_handling.aggregate('&1','&2');
exit;
!