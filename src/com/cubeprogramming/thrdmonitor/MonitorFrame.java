package com.cubeprogramming.thrdmonitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
//import com.borland.cx.*;
import javax.swing.border.*;
//import com.borland.jbcl.layout.*;
import javax.swing.event.*;

/**
 * GUI okru�enja za pra�enje rada programa u debuging modu.
 */
public class MonitorFrame extends JFrame {
  private StartMonitorFrame startMonitor;
  JPanel contentPane;
  JPanel TopBar = new JPanel();
  JButton Logo = new JButton();
  JLabel Naslov = new JLabel();
  JPanel BotomPane = new JPanel();
  JButton Close = new JButton();
  JSplitPane VerticalSplit = new JSplitPane();
  TitledBorder titledBorder1;
  JSplitPane HorizontalSplitTop = new JSplitPane();
  JSplitPane HorizontalSplitBotom = new JSplitPane();
  JPanel LeftUp = new JPanel();
  JPanel RightUp = new JPanel();
  JPanel LeftDown = new JPanel();
  JPanel RightDown = new JPanel();
  JLabel Label1 = new JLabel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel3 = new JLabel();
  JScrollPane jScrollPane1 = new JScrollPane();
  JTextArea MainLog = new JTextArea();
  JScrollPane jScrollPane2 = new JScrollPane();
  JScrollPane jScrollPane3 = new JScrollPane();
  JScrollPane jScrollPane4 = new JScrollPane();
  JTextArea AnalysisLog = new JTextArea();
  JTextArea SignalsSent = new JTextArea();
  JTextArea SignalsReceived = new JTextArea();
  BorderLayout borderLayout2 = new BorderLayout();
  BorderLayout borderLayout3 = new BorderLayout();
  BorderLayout borderLayout4 = new BorderLayout();
  BorderLayout borderLayout5 = new BorderLayout();
  BorderLayout borderLayout1 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();


  public MonitorFrame(StartMonitorFrame startMonitor) {
    this.startMonitor = startMonitor;

    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    contentPane = (JPanel) this.getContentPane();
    contentPane.setLayout(borderLayout1);
    titledBorder1 = new TitledBorder("Tarif - Process Monitor");
    BotomPane.setBorder(BorderFactory.createEtchedBorder());
    Close.setVerifyInputWhenFocusTarget(true);
    Close.setText("Close");
    Close.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Close_actionPerformed(e);
      }
    });
    VerticalSplit.setOrientation(JSplitPane.VERTICAL_SPLIT);
    VerticalSplit.setBorder(null);
    VerticalSplit.setOpaque(false);
    VerticalSplit.setDividerSize(3);
    HorizontalSplitTop.setBorder(null);
    HorizontalSplitTop.setContinuousLayout(false);
    HorizontalSplitTop.setDividerSize(3);
    HorizontalSplitTop.setLastDividerLocation(255);
    HorizontalSplitBotom.setBorder(null);
    HorizontalSplitBotom.setDoubleBuffered(false);
    HorizontalSplitBotom.setDividerSize(3);
    HorizontalSplitBotom.setLastDividerLocation(255);
    LeftUp.setLayout(borderLayout2);
    RightUp.setLayout(borderLayout3);
    LeftDown.setLayout(borderLayout4);
    RightDown.setLayout(borderLayout5);
    Label1.setFont(new java.awt.Font("Dialog", 1, 11));
    Label1.setMaximumSize(new Dimension(43, 20));
    Label1.setMinimumSize(new Dimension(43, 20));
    Label1.setPreferredSize(new Dimension(43, 20));
    Label1.setHorizontalAlignment(SwingConstants.CENTER);
    Label1.setText("Main Log");
    jLabel1.setFont(new java.awt.Font("Dialog", 1, 11));
    jLabel1.setForeground(Color.black);
    jLabel1.setMaximumSize(new Dimension(63, 20));
    jLabel1.setMinimumSize(new Dimension(63, 20));
    jLabel1.setPreferredSize(new Dimension(63, 20));
    jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel1.setText("Analysis Log");
    jLabel2.setFont(new java.awt.Font("Dialog", 1, 11));
    jLabel2.setMaximumSize(new Dimension(65, 20));
    jLabel2.setMinimumSize(new Dimension(65, 20));
    jLabel2.setPreferredSize(new Dimension(65, 20));
    jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel2.setHorizontalTextPosition(SwingConstants.TRAILING);
    jLabel2.setText("Signals Sent");
    jLabel3.setFont(new java.awt.Font("Dialog", 1, 11));
    jLabel3.setMaximumSize(new Dimension(83, 20));
    jLabel3.setMinimumSize(new Dimension(83, 20));
    jLabel3.setPreferredSize(new Dimension(83, 20));
    jLabel3.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel3.setHorizontalTextPosition(SwingConstants.TRAILING);
    jLabel3.setText("Signals Received");
    jScrollPane1.setAutoscrolls(true);
    jScrollPane1.setBorder(null);
    jScrollPane2.setBorder(null);
    jScrollPane3.setBorder(null);
    jScrollPane4.setBorder(null);
    TopBar.setBackground(Color.lightGray);
    TopBar.setMaximumSize(new Dimension(2048, 57));
    TopBar.setMinimumSize(new Dimension(640, 57));
    TopBar.setPreferredSize(new Dimension(1000, 57));
    Naslov.setMaximumSize(new Dimension(344, 57));
    Naslov.setMinimumSize(new Dimension(344, 57));
    Naslov.setPreferredSize(new Dimension(344, 57));
    Logo.setBorderPainted(false);
    Logo.setFocusPainted(false);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    flowLayout1.setHgap(0);
    flowLayout1.setVgap(0);
    MainLog.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentShown(ComponentEvent e) {
        MainLog_componentResized(e);
      }
      public void componentResized(ComponentEvent e) {
        MainLog_componentResized(e);
      }
    });
    AnalysisLog.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        AnalysisLog_componentResized(e);
      }
      public void componentShown(ComponentEvent e) {
        AnalysisLog_componentResized(e);
      }
    });
    SignalsSent.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        SignalsSent_componentResized(e);
      }
      public void componentShown(ComponentEvent e) {
        SignalsSent_componentResized(e);
      }
    });
    SignalsReceived.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        SignalsReceived_componentResized(e);
      }
      public void componentShown(ComponentEvent e) {
        SignalsReceived_componentResized(e);
      }
    });
    contentPane.add(TopBar, BorderLayout.NORTH);
    TopBar.add(Logo, null);
    TopBar.add(Naslov, null);
    TopBar.setBorder(BorderFactory.createEtchedBorder());
    TopBar.setLayout(flowLayout1);
    contentPane.add(VerticalSplit, BorderLayout.CENTER);
    VerticalSplit.add(HorizontalSplitTop, JSplitPane.LEFT);
    VerticalSplit.add(HorizontalSplitBotom, JSplitPane.RIGHT);
    contentPane.add(BotomPane, BorderLayout.SOUTH);
    BotomPane.add(Close, null);
    contentPane.setMinimumSize(new Dimension(640, 480));
    contentPane.setPreferredSize(new Dimension(1010, 740));
    contentPane.setToolTipText("");
    this.setTitle("TARIF-PROCESS MONITOR");
    this.setSize(new Dimension(1010, 740));
    Logo.setEnabled(true);
    Logo.setBorder(null);
    Logo.setIcon(new ImageIcon(MonitorFrame.class.getResource("Tcomlogo.gif")));
    Logo.setMargin(new Insets(0, 0, 0, 0));
    Logo.setSelected(false);
    Logo.setText("");
    Naslov.setFont(new java.awt.Font("Dialog", 1, 24));
    Naslov.setHorizontalAlignment(SwingConstants.CENTER);
    Naslov.setHorizontalTextPosition(SwingConstants.CENTER);
    Naslov.setText("TARIFF - PROCESS MONITOR");
    HorizontalSplitTop.add(LeftUp, JSplitPane.LEFT);
    HorizontalSplitTop.add(RightUp, JSplitPane.RIGHT);
    HorizontalSplitBotom.add(LeftDown, JSplitPane.LEFT);
    HorizontalSplitBotom.add(RightDown, JSplitPane.RIGHT);
    LeftUp.add(Label1, BorderLayout.NORTH);
    RightUp.add(jLabel1, BorderLayout.NORTH);
    LeftDown.add(jLabel2, BorderLayout.NORTH);
    RightDown.add(jLabel3, BorderLayout.NORTH);
    LeftUp.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(MainLog, null);
    LeftDown.add(jScrollPane3, BorderLayout.CENTER);
    jScrollPane3.getViewport().add(SignalsSent, null);
    RightDown.add(jScrollPane4, BorderLayout.CENTER);
    jScrollPane4.getViewport().add(SignalsReceived, null);
    RightUp.add(jScrollPane2, BorderLayout.CENTER);
    jScrollPane2.getViewport().add(AnalysisLog, null);
    VerticalSplit.setDividerLocation(160);
    HorizontalSplitTop.setDividerLocation(270);
    HorizontalSplitBotom.setDividerLocation(270);
  }

  void Close_actionPerformed(ActionEvent e) {
    startMonitor.UI = false;
    //this.setVisible(false);
    dispose();
  }

  protected void processWindowEvent(WindowEvent e) {
   super.processWindowEvent(e);
   VerticalSplit.setDividerLocation(0.5);
   HorizontalSplitTop.setDividerLocation(0.5);
   HorizontalSplitBotom.setDividerLocation(0.5);
   if (e.getID() == WindowEvent.WINDOW_CLOSING) {
     Close_actionPerformed(null);
     //dispose();
   }
  }

  int MainLogIncr;

  void MainLog_componentResized(ComponentEvent e) {
    Rectangle viewRect = jScrollPane1.getViewport().getViewRect();
    MainLogIncr = MainLog.getScrollableUnitIncrement(viewRect,SwingConstants.VERTICAL,1);
  }

  int AnalysisLogIncr;

  void AnalysisLog_componentResized(ComponentEvent e) {
    Rectangle viewRect = jScrollPane2.getViewport().getViewRect();
    AnalysisLogIncr = MainLog.getScrollableUnitIncrement(viewRect,SwingConstants.VERTICAL,1);
  }

  int SignalsSentIncr;

  void SignalsSent_componentResized(ComponentEvent e) {
    Rectangle viewRect = jScrollPane3.getViewport().getViewRect();
    SignalsSentIncr = MainLog.getScrollableUnitIncrement(viewRect,SwingConstants.VERTICAL,1);
  }

  int SignalsReceivedIncr;

  void SignalsReceived_componentResized(ComponentEvent e) {
    Rectangle viewRect = jScrollPane4.getViewport().getViewRect();
    SignalsReceivedIncr = MainLog.getScrollableUnitIncrement(viewRect,SwingConstants.VERTICAL,1);
  }


}//~class MonitorFrame