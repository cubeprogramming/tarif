/* Generated by Together */

package tarif.rating;

/**
 * Predstavlja record u tabeli TVAS i TINVAS
 */
public class Vas {
  public Vas(long zona_id, String bbroj_ind) {
    this.zona_id = zona_id;
    this.bbroj_ind = bbroj_ind;
  }

  public String bbroj_ind;
  public long zona_id;
}
