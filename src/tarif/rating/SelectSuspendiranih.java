/* Generated by Together */

package tarif.rating;

import oracle.jdbc.*;
import java.sql.*;
import java.util.*;
import tarif.utils.*;
import tarif.*;
import com.cubeprogramming.utils.*;


/**
 * Klasa koja slu�i za �itanje slogova iz TSTRAFFIC
 */
public class SelectSuspendiranih {
  //Lokalne variable
  private MetaSlog metaSlog;
  private PreparedStatement query;
  private Connection connection;
  private ResultSet rset = null;
  private String sql = "SELECT record_id, " +
                              "naziv_datoteke, " +
                              "call_id, " +
                              "mrezna_grupa, " +
                              "msisdn, " +
                              "sifra_servisa, " +
                              "sifra_sservisa, " +
                              "pozvani_broj, " +
                              "datumv, " +
                              "trajanje, " +
                              "otrajanje, " +
                              "impulsi, " +
                              "iznos, " +
                              "bbroj_prefiks, " +
                              "zona_id, " +
                              "katpoziva_id, " +
                              "plan_id, " +
                              "tarifa_id, " +
                              "a_category, " +
                              "b_category, " +
                              "type_of_b_num, " +
                              "charged_party, " +
                              "origin_for_charging, " +
                              "tariff_class, " +
                              "charging_case, " +
                              "charging_case_origin, " +
                              "description, " +
                              "flex_uus, " +
                              "ERROR_CODE, " +
                              "type_of_asubscriber " +
                       "FROM tstraffic";

  /**
   * Konstruktor priprema sastement za selektiranje
   * @param connection - konekcija kroz koju ce se vrsiti selekt
   * @param buffVal - velicina buffera za ResultSet
   * @throws ExitException - propagira se SQLException kroz ovaj exception i dodaje se Exit status
   */
  public SelectSuspendiranih(Connection connection, int buffVal) throws ExitException {
    this(connection,buffVal,"");
  }//~SelectSuspendiranih()

  /**
   * Konstruktor priprema sastement za selektiranje
   * @param connection - konekcija kroz koju ce se vrsiti selekt
   * @param buffVal - velicina buffera za ResultSet
   * @param ociklus - obra�unski ciklus za koji se izvla�e suspendirani
   * @throws ExitException - propagira se SQLException kroz ovaj exception i dodaje se Exit status
   */
  public SelectSuspendiranih(Connection connection, int buffVal, String ociklus) throws ExitException {
    try {
      if ( (ociklus != null) && (ociklus.length() == 7))
        sql +=
            " where to_char(datumv, 'MONRRRR','NLS_DATE_LANGUAGE=American') = '" +
            ociklus + "'";

      prepareStatement(connection, buffVal);
    } catch (SQLException sqlException) {
      throw new ExitException(Globals.EXIT_SQLERR,sqlException);
    }
  }//~SelectSuspendiranih()

  /**
   * Konstruktor priprema sastement za selektiranje
   * @param connection - konekcija kroz koju ce se vrsiti selekt
   * @param buffVal - velicina buffera za ResultSet
   * @param date - dan za koji �eliko izvu�i suspendirane
   * @throws ExitException - propagira se SQLException kroz ovaj exception i dodaje se Exit status
   */
  public SelectSuspendiranih(Connection connection, int buffVal, Timestamp date) throws ExitException {
    try {
      if (date != null) {
        sql +=" where trunc(datumv) = trunc(?)";

        prepareStatement(connection, buffVal);
        query.setTimestamp(1,date);
      }
    } catch (SQLException sqlException) {
      throw new ExitException(Globals.EXIT_SQLERR,sqlException);
    }

  }//~SelectSuspendiranih()

  /**
   * Konstruktor priprema sastement za selektiranje
   * @param connection - konekcija kroz koju ce se vrsiti selekt
   * @param buffVal - velicina buffera za ResultSet
   * @throws SQLException - U slu�aju da je do�lo do gre�ke prilikom pripremanja SQL-a
   */
  private void prepareStatement(Connection connection, int buffVal) throws SQLException {
    this.connection = connection;
    query = connection.prepareStatement(sql,
                                        ResultSet.TYPE_FORWARD_ONLY,
                                         ResultSet.CONCUR_UPDATABLE);

    query.setFetchSize(buffVal);

  }//~prepareStatement()

  /**
     * Metoda koja vr�i selekt suspendiranih
     * @throws ExitException - propagira se SQLException kroz ovaj exception i dodaje se Exit status
     * @return ResultSet ako se radio o parcijalnom slogu
   */
  public ResultSet select() throws ExitException {
    try {
      if (!connection.isClosed()) {
         rset = query.executeQuery();
         return rset;
      }
    } catch (SQLException sqlException) {
      try {
        connection.rollback();
      } catch (SQLException e) {
        throw new ExitException(Globals.EXIT_SQLERR,sqlException);
      }
      throw new ExitException(Globals.EXIT_SQLERR,sqlException);
    }
    return null;
  }//~select()

  /**
   * Puni memorijsku strukturu MetaSloga
   * @param metaSlog - memorijska struktura koja se istovremeno puni iz ResultSet-a
   * @throws ExitException - propagira se SQLException kroz ovaj exception i dodaje se Exit status
   */
  public void fillMeta(MetaSlog metaSlog) throws ExitException {
    try {
      metaSlog.naziv_datoteke = rset.getString("naziv_datoteke").trim();
      metaSlog.call_id = new Long(rset.getString("call_id").trim());
      metaSlog.mrezna_grupa = rset.getString("mrezna_grupa").trim();
      String msisdn = rset.getString("msisdn").trim();
      metaSlog.msisdn = new Long(msisdn);
      metaSlog.sifra_servisa = rset.getString("sifra_servisa").trim();
      metaSlog.sifra_sservisa = rset.getString("sifra_sservisa").trim();
      metaSlog.pozivni_broj = rset.getString("pozvani_broj").trim();
      metaSlog.datumv = rset.getTimestamp("datumv");
      metaSlog.ptrajanje = new Long(rset.getLong("trajanje"));
      metaSlog.trajanje = tarif.utils.DateConversion.getTrajanje(metaSlog.ptrajanje);
      metaSlog.otrajanje = new Long(rset.getLong("otrajanje"));
      metaSlog.impulsi = new Long(rset.getLong("impulsi"));
      metaSlog.iznos = new Float(rset.getFloat("iznos"));
      metaSlog.bbroj_prefiks = rset.getString("bbroj_prefiks").trim();
      metaSlog.zona_id = new Long(rset.getLong("zona_id"));
      metaSlog.katpoziva_id = new Long(rset.getLong("katpoziva_id"));
      metaSlog.plan_id = new Long(rset.getLong("plan_id"));
      metaSlog.tarifa_id = new Long(rset.getLong("tarifa_id"));
      metaSlog.a_category = rset.getString("a_category").trim();
      metaSlog.b_category = rset.getString("b_category").trim();
      metaSlog.type_of_b_num = rset.getString("type_of_b_num").trim();
      metaSlog.charged_party = rset.getString("charged_party").trim();
      metaSlog.origin_for_charging = rset.getString("origin_for_charging").trim();
      metaSlog.tariff_class = rset.getString("tariff_class").trim();
      metaSlog.charging_case = rset.getString("charging_case").trim();
      metaSlog.charging_case_origin = rset.getString("charging_case_origin").trim();
      metaSlog.description = rset.getString("description").trim();
      metaSlog.flex_uus = new Long(rset.getLong("flex_uus"));
      metaSlog.error_code = rset.getString("ERROR_CODE").trim();
      metaSlog.type_of_a_num = rset.getString("type_of_asubscriber").trim();
    } catch (SQLException sqlException) {
      try {
        connection.rollback();
      } catch (SQLException e) {
        throw new ExitException(Globals.EXIT_SQLERR,sqlException);
      }
      throw new ExitException(Globals.EXIT_SQLERR,sqlException);
    }
  }//~fillMeta()

  /**
   * Zatvara logicku konekciju prema bazi
   * @throws ExitException - propagira se SQLException kroz ovaj exception i dodaje se Exit status
   */
  public void closeConnection() throws ExitException{
    try {
      if (!connection.isClosed()) {
        //connection.commit();
        connection.close();
      }
    } catch (SQLException sqlException) {
      throw new ExitException(Globals.EXIT_SQLERR,sqlException);
    }
  }//~closeConnection()

}//~class SelectSuspendiranih
