package tarif;


/**
 * Record sa informacijama o dodatnim vrstama usluga
 */
public class VrstaUsluge {
  //Globalen variable
  public int paket_id;
  public Long pvrsta_usluge_id;

  public VrstaUsluge(int paket_id,
                     Long pvrsta_usluge_id) {
    this.paket_id = paket_id;
    this.pvrsta_usluge_id = pvrsta_usluge_id;
  }//~VrstaUsluge()

}//~class VrstaUsluge