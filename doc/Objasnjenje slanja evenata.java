import com.cubeprogramming.thrdmonitor.*;
import com.cubeprogramming.utils.*;

/**
 * Glavna (kontrolna) nit
 */
public class Main implements ThreadListener, FreeResources {
  //Lokalne variable
  private int waitTimeout = Monitor.DEFAULT_TIMEOUT;
  private ThreadGroup myGroup = new ThreadGroup("myGroup");
  private Monitor monitor = Monitor.getMonitor(myGroup);
  //Globalne variable
  public static final String MONITOR = "-m"; //Thread monitor GUI startup
  static public String LOG_DIR = "C:\\mojdir\\logs";
  static public String FILE_IN = "C:\\mojdir\\filein";
  static public int EXIT_SUCCESS = 0;
  static public int EXIT_FAILURE = 1;
  static public int EXIT_IOERR = 100;
  static public int exitStatus = EXIT_SUCCESS;

  /**
   * Metoda koja pokrece program
   * @param argv -argumenti komandne linije
   */
  public static void main(String[] argv) {
    new Main(argv);
   }//~main()

  /**
   * Obrada ulaznih parametara, inicijalizcija i pozivanje svih drugih objekata.
   * @param argv -Parametri komandne linije iz main
   */
  public Main(String[] argv) {
    //Procesira argumente komandne linije
    procesArgs(argv);

    System.out.println("**** Tarifiranje startano u: " + new java.util.Date( )  +
                       " - POCETAK RADA PROGRAMA *****");
    if (monitor.monitorFrame.getUI())
      monitor.monitorFrame.writeMainLog("**** Tarifiranje startano u: " + new java.util.Date( )
         + " *****");

    //Registrira Main objekt kao Event listener za Monitor evente
    monitor.addThreadListener(this);

    //Povezuje Monitor sa Main objektom
    monitor.setFreeObject(this);

    try {
      //Inicijalizira LOG path za monitor log
      monitor.monitorFrame.setLogPath(LOG_DIR + System.getProperty("file.separator"));

      //Inicilalizira monitor log za DEBUG - moram smisliti
      monitor.monitorFrame.writeAnalysisLog("START ANALYSIS -- " + new java.util.Date( ));

      fillFileList();

    }catch (Exception e){
      System.err.println(e.getMessage());
      exitStatus = EXIT_IOERR;
    }
  }//~Main()

  /**
   * Procesira argumente komandne linije: setup connection string, version i setup ID
   * @param argv -argumenti komandne linije
   */
  private void procesArgs(String[] argv) {
    for (int i = 0; i < argv.length; i++) {
      if (argv[i].substring(0,2).equalsIgnoreCase(MONITOR)) {
        monitor.monitorFrame.setOutput(true);
        if (argv[i].substring(2).indexOf("L") != -1) // -mL uklju�uje log
          monitor.monitorFrame.setLog(true);
        if (argv[i].substring(2).indexOf("G") != -1) // -mG uklju�uje GUI
          monitor.monitorFrame.setUI(true);
      }
    }//~for
  }//~procesArgs


  /**
   * Metoda koja �ita trenutni snaphsot CDR_IN direktorija i instancira threadove
   * @throws ExitException ukoliko je do�lo do exceptiona u nekom threadu
   */
  private void fillFileList() {
    //Formiranje liste ulaznih datoteka
    File inDir = new File(FILE_IN);
    File[] inFiles = inDir.listFiles();

    //Maksimalni broj niti koje se od jednom smije instancirati
    int maxProcess = 5;

    for (int i = 0; i < inFiles.length; i++) {
      //Pokreni threadove do maximalnog broja i pokre�i sljede�i kako prvi zavr�i
      if (i > maxProcess) {
        monitor.waitFor();
      }
      CustomThrd customThrd = new CustomThrd(myGroup, inFiles[i]);
    }
  }//~fillFileList()

  /**
   * Event hendler funkcija koja dobija signal iz niti
   * @param thrdEvent event objekt proslije�en iz niti
   */
  public void notified(ThreadEvent thrdEvent) {
    monitor.monitorFrame.writeMainLog("Notification received: " + thrdEvent.toString());
    if (thrdEvent.isExitException()) {
      File inFile = ( (CustomThrd)thrdEvent.getSourceThread()).inFile;
      Exception err = (ExitException)thrdEvent.getThreadException();
      System.out.println(inFile.getName() + thrdEvent.getExitStatus() + err.getMessage());
      //Iza�i iz programa
      exitStatus = thrdEvent.getExitStatus();
      System.exit(exitStatus);
    } else if (thrdEvent.isException()) {
      File inFile = ( (CustomThrd)thrdEvent.getSourceThread()).inFile;
      Exception err = thrdEvent.getThreadException();
      System.out.println(inFile.getName() + thrdEvent.getExitStatus() + err.getMessage());
      //Iza�i iz programa
      exitStatus = thrdEvent.getExitStatus();
      System.exit(exitStatus);
    } else {
      File inFile = ( (CustomThrd) thrdEvent.getSourceThread()).inFile;
      System.out.println(inFile.getName() + thrdEvent.getExitStatus() + "Datoteka uspjesno obradjena");
    }
  }//~notified()

  public void free() throws Exception {
    System.out.println("**** Tarifiranje zavrseno u: " + new java.util.Date( )
                       + " - KRAJ RADA PROGRAMA - Exit status: " + exitStatus );
  }//~free()

}//~class Main


/**
 * Primjer niti koja komunicira sa glavnom Main niti.
 */
public class CustomThrd extends Thread{
  //Local variables
  private Monitor monitor = Monitor.getMonitor(this.getThreadGroup());
  //Global varables
  public File inFile;


  /**
   * Konstruktor
   * Ime niti jednako je imenu datoteke koju obra�uje.
   * @param inFile - ulazna datoteka
   * @param thrdGrp Thread Group kojem pripada ovaj thread
   */
  public CustomThrd(ThreadGroup thrdGrp, File inFile) {
    super(thrdGrp,inFile.getName());
    this.inFile = inFile;
    this.start();
  }//~CustomThrd()

  /**
   * Metoda koja se automatski poziva kada se nit instancira
   * @throws ExitException - sadrzi broj izlaznog statusa i poruku o gresci
   */
  public void run() {
    try {
      //Parsira ulaznu datoteku
      parseFile();

      monitor.notifyListeners(new ThreadEvent(this,Main.EXIT_SUCCESS));
    } catch (Exception e) {
      if (e instanceof ExitException)
        monitor.notifyListeners(new ThreadEvent(this,(ExitException)e));
      else
        monitor.notifyListeners(new ThreadEvent(this,Main.EXIT_FAILURE,e));
    }
  }//~run()

  /**
   * Metoda koja parsira cijelu datoteku
   * @throws IOException - ukoliko je doslo do greske prilikom citanja datoteke
   * @throws ExitException - ukoliko je doslo do greske prilikom citanja datoteke
   */
  private void parseFile() throws IOException, ExitException {
    try {
      //Upisuje poruke u log
      monitor.monitorFrame.writeAnalysisLog(inFile.getName()+ "Parsiram datoteku");
      //Pusti druge niti da di�u
      Thread.currentThread().yield();
      //Ukoliko je do�lo do pojave gre�ke u kodu izbaci exit exception
      throw new ExitException(Main.EXIT_IOERR,"Greska kod parsiranja");

    } catch (Exception e) { //Hvata sve neuhva�ene gre�ke
      throw new ExitException(Main.EXIT_FAILURE,e);
    }
  }//~parseFile()

}//~class CustomThrd
