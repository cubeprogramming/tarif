CREATE OR REPLACE TRIGGER "ARMALI".MCEVENTLOG_SEQ 
before insert on MCEVENTLOG
referencing NEW as NEW 
for each row 
begin 
	 select MCEVENTLOG_SEQ.nextval into :new.ID from dual; 
end;
/
