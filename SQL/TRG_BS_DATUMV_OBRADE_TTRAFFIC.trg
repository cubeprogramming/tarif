CREATE OR REPLACE TRIGGER ARATE.TRG_BS_DATUMV_OBRADE_TTRAFFIC
BEFORE INSERT
ON ARATE.TTRAFFIC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        29.10.04     Dalibor Blazvic        1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     
      Sysdate:         29.10.04
      Date and Time:   29.10.04, 13:20:00
      Username:         Arate
      Table Name:       TTRATING
      Trigger Options:  AFTER INSERT FOR EACH ROW
******************************************************************************/
BEGIN
   :new.DATUMV_OBRADE := tft_utils.Get_Udr_Time(:new.DATUMV);
   
   EXCEPTION
     WHEN OTHERS THEN
       raise_application_error(SQLCODE-20000, SQLERRM||' Greska u trigeru: TRG_BS_DATUMV_OBRADE_TTRAFFIC'||USER);
END ;
/
