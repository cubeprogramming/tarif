select * from user_tablespaces
where tablespace_name = 'DADO_TEMP'

-- 1. backup

create table TTRAFFIC_17112004 tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_17112004 tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_17112004 tablespace DADO_TEMP
as select * from TSTRAFFIC

-- 2. backup

create table TTRAFFIC_19112004 tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_19112004 tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_19112004 tablespace DADO_TEMP
as select * from TSTRAFFIC

-- 3. backup - Poslje performance testa

create table TTRAFFIC_22112004 tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_22112004 tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_22112004 tablespace DADO_TEMP
as select * from TSTRAFFIC

create table MCEVENTLOG_22112004 tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 4. backup - Ispravljeno zaokruživanje

create table TTRAFFIC_23122004 tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_23122004 tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_23122004 tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_23122004 tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_23122004 tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 5. backup - Ispravljeno određivanje katpoziva_id

create table TTRAFFIC_12012005 tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_12012005 tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_12012005 tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_12012005 tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_12012005 tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 6. backup - Ispravljeno pojavljuvanje duplih slogova kod usluga

create table TTRAFFIC_14012005 tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_14012005 tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_14012005 tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_14012005 tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_14012005 tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 7. backup - Test time bandova

create table TTRAFFIC_14012005_band tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_14012005_band tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_14012005_band tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_14012005_band tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_14012005_band tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 8. backup - Test parcijalnih

create table TTRAFFIC_14012005_partial tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_14012005_partial tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_14012005_partial tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_14012005_partial tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_14012005_partial tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 9. backup - Test time bandova

create table TTRAFFIC_20012005_band tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_20012005_band tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_20012005_band tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_20012005_band tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_20012005_band tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 10. backup - Test parcijalnih

create table TTRAFFIC_20012005_partial tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_20012005_partial tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_20012005_partial tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_20012005_partial tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_20012005_partial tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 11. backup - Test parcijalnih (Irenin test)

create table TTRAFFIC_18022005_partial tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_18022005_partial tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_18022005_partial tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_18022005_partial tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_18022005_partial tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 12. backup - Test parcijalnih (Irenin test-finalizacija)

create table TTRAFFIC_18022005_finalize tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_18022005_finalize tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_18022005_finalize tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_18022005_finalize tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_18022005_finalize tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 13. backup - Test parcijalnih (Irenin test br.2)

create table TTRAFFIC_22022005_partial tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_22022005_partial tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_22022005_partial tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_22022005_partial tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_22022005_partial tablespace DADO_TEMP
as select * from MCEVENTLOG

-- 14. backup - Test parcijalnih (Irenin test br.2-finalizacija)

create table TTRAFFIC_22022005_finalize tablespace DADO_TEMP
as select * from TTRAFFIC

create table TPTRAFFIC_22022005_finalize tablespace DADO_TEMP
as select * from TPTRAFFIC

create table TSTRAFFIC_22022005_finalize tablespace DADO_TEMP
as select * from TSTRAFFIC

create table TTRATING_22022005_finalize tablespace DADO_TEMP
as select * from TTRATING

create table MCEVENTLOG_22022005_finalize tablespace DADO_TEMP
as select * from MCEVENTLOG