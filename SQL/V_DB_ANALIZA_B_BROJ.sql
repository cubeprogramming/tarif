CREATE OR REPLACE VIEW ARATE.V_DB_ANALIZA_B_BROJ
AS SELECT distinct
	   k.zona_id,
	   k.plan_id,
	   k.ID kombinacija_id,
	   f.KOEFICIJENT koeficijent1,
	   t.KOEFICIJENT koeficijent2,
	   k.tarifa_id,
	   to_date('01.01.1970 ' || to_char(o.VRIJEME_OD,'HH24:MI:SS'),'DD.MM.YYYY HH24:MI:SS') VRIJEME_OD,
	   to_date('01.01.1970 ' || to_char(o.VRIJEME_DO,'HH24:MI:SS'),'DD.MM.YYYY HH24:MI:SS') VRIJEME_DO,
	   kd.DATUM_OD,
	   kd.DATUM_DO,
	   kd.NAZIV,
	   fu.cijena,
	   fu.tip_obracuna,
	   fu.DATUM_START,
	   fu.DATUM_STOP
FROM tuzorak u,
	 tvremenski_opseg o,
	 tkombinacija k,
	 tfaktor f, 
	 ttarifa t,
	 fusluga fu,
	 (select KLASA_DANA_ID,
	   		 DATUM_OD,
	   		 DATUM_DO,
	   		 to_char(null) naziv
      from ttip_datuma
	  union
	  select KLASA_DANA_ID,
	         to_date(null) DATUM_OD,
	   		 to_date(null) DATUM_DO,
	   		 naziv
	  from ttip_dana) kd	 
WHERE u.KLASA_DANA_ID=kd.KLASA_DANA_ID 
AND u.VOPSEG_ID=o.id
AND k.UZORAK_ID=u.id
and k.faktor_id=f.id
and t.id=k.tarifa_id
and fu.ID = k.usluga_id
order by k.zona_id,
	   k.plan_id,
	   f.KOEFICIJENT,
	   fu.tip_obracuna,
	   nvl(kd.datum_od,to_date('01.01.1970','DD.MM.YYYY')) desc

