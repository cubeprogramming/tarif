CREATE OR REPLACE TRIGGER ARATE.TBONUS_SERVICES_SEQ
BEFORE INSERT
ON ARATE.TBONUS_SERVICES 
REFERENCING NEW AS NEW
FOR EACH ROW
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        06.06.05      Dalibor Blazevic       1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     Autoincrement field
      Sysdate:         06.06.05
      Date and Time:   06.06.05, 13:59:28, and 06.06.05 13:59:28
      Username:        Dalibor Blazevic (set in TOAD Options, Proc Templates)
      Table Name:       (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
begin 
	 select SEQ_GENERAL.nextval into :new.ID from dual; 
end;



/


