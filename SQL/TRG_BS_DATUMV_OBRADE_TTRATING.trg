CREATE OR REPLACE TRIGGER ARATE.TRG_BS_DATUMV_OBRADE_TTRATING
BEFORE INSERT
ON ARATE.TTRATING 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        28.10.04     Dalibor Blazvic        1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     
      Sysdate:         28.10.04
      Date and Time:   28.10.04, 15:20:00
      Username:         Arate
      Table Name:       TTRATING
      Trigger Options:  AFTER INSERT FOR EACH ROW
******************************************************************************/
BEGIN
   :new.DATUMV_OBRADE := tft_utils.Get_Udr_Time(:new.DATUMV);
   
   EXCEPTION
     WHEN OTHERS THEN
       raise_application_error(SQLCODE-20000, SQLERRM||' Greska u trigeru: TRG_BS_DATUMV_OBRADE_TTRATING'||USER);
END ;
/
