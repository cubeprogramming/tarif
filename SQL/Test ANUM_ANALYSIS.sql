declare
	p_mg VARCHAR2(5) := '033';
	p_msisdn VARCHAR2(10) := '675599';
	p_date DATE := SYSDATE;
	p_pretid VARCHAR2(20);
	p_korid VARCHAR2(20);
	p_paket NUMBER;
	p_bgrupa NUMBER;
begin
	 ANUM_ANALYSIS(p_mg,p_msisdn,p_date,
                          p_pretid,
                          p_korid,
                          p_paket,
                          p_bgrupa);
	dbms_output.put_line(p_pretid);
	dbms_output.put_line(p_korid);
	dbms_output.put_line(to_char(p_paket));
	dbms_output.put_line(to_char(p_bgrupa));
end;

select *
from ppretplatnik
where mrezna_grupa = '033'