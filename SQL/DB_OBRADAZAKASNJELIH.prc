CREATE OR REPLACE PROCEDURE P_DB_ObradaZakasnjelih IS
/******************************************************************************
   NAME:       DB_ObradaZakasnjelih
   PURPOSE: Mjenja status zaka�njelih slogova prije spajanja parcijalnih slogova   

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13.12.04    Dalibor Bla�evi�      1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     DB_ObradaZakasnjelih
      Sysdate:         13.12.04
      Date and Time:   13.12.04, 16:04:15, and 13.12.04 16:04:15
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   
   -- Update zaka�njelih slogova kojima nije poznat zavr�etak
update tptraffic tp set
  tp.STATUS = 'z'
where (tp.status = '0'
   or tp.status = 'N')
  and exists (select 1
              from tptraffic tpo
			  where tpo.status = '1'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV >= tpo.DATUMV)
  and exists (select 1
              from tptraffic tpo
			  where status = '1'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV <= (tpo.DATUMV + (get_sekunde(trajanje,'US')/86400) ) );
				
-- Update zaka�njelih slogova kojima je poznat zavr�etak
update tptraffic tp set
  tp.STATUS = 'Z'
where (tp.status = '0'
   or tp.status = 'N')
  and exists (select 1
              from tptraffic tpo
			  where tpo.status = '2'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV >= tpo.DATUMV)
  and exists (select 1
              from tptraffic tpo
			  where status = '2'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV <= (tpo.DATUMV + (get_sekunde(trajanje,'US')/86400) ) );
   
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       raise_application_error(SQLCODE, SQLERRM||' Greska u proceduri: P_DB_ObradaZakasnjelih');
END P_DB_ObradaZakasnjelih;
/
