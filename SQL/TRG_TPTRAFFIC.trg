CREATE OR REPLACE TRIGGER ARATE.TRG_TPTRAFFIC
INSTEAD OF UPDATE
ON ARATE.V_TPTRAFFIC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
/******************************************************************************
   NAME: TRG_TPTRAFFIC      
   PURPOSE: Omogu�iti update iz Java ResultSeta preko OCI drivera   

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        21.12.04    Dalibor Bla�evi� 1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     
      Sysdate:         21.12.04
      Date and Time:   21.12.04, 15:58:14, and 21.12.04 15:58:14
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:       (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN 
   update tptraffic set
      status = :new.status
   where record_id = :new.record_id;

   EXCEPTION
     WHEN OTHERS THEN
      raise_application_error(SQLCODE, SQLERRM||' Greska u trigeru: TRG_TPTRAFFIC');
END ;
/


