SELECT distinct
	   k.zona_id,
	   k.plan_id,
	   k.ID kombinacija_id,
	   f.KOEFICIJENT koeficijent1,
	   t.KOEFICIJENT koeficijent2,
	   k.tarifa_id,
	   o.vrijeme_od,
	   o.vrijeme_do,
	   kd.DATUM_OD,
	   kd.DATUM_DO,
	   kd.NAZIV,
	   fu.cijena,
	   fu.tip_obracuna
FROM tuzorak u,
	 tvremenski_opseg o,
	 tkombinacija k,
	 tfaktor f, 
	 ttarifa t,
	 fusluga fu,
	 (select KLASA_DANA_ID,
	   		 DATUM_OD,
	   		 DATUM_DO,
	   		 to_char(null) naziv
      from ttip_datuma
	  union
	  select KLASA_DANA_ID,
	         to_date(null) DATUM_OD,
	   		 to_date(null) DATUM_DO,
	   		 naziv
	  from ttip_dana) kd	 
WHERE u.KLASA_DANA_ID=kd.KLASA_DANA_ID 
AND u.VOPSEG_ID=o.id
AND k.UZORAK_ID=u.id
and k.faktor_id=f.id
and t.id=k.tarifa_id
and fu.ID = k.usluga_id
order by k.zona_id,
	   k.plan_id,
	   f.KOEFICIJENT,
	   fu.tip_obracuna,
	   nvl(kd.datum_od,to_date('01.01.1970','DD.MM.YYYY')) desc

select a.ZONA_ID,
	   a.PLAN_ID,
	   a.KOMBINACIJA_ID,
	   a.KOEFICIJENT1,
	   a.KOEFICIJENT2,
	   a.TARIFA_ID,
	   a.VRIJEME_OD,
	   a.VRIJEME_DO,
	   a.DATUM_OD,
	   a.DATUM_DO,
	   a.NAZIV,
	   a.CIJENA,
	   a.TIP_OBRACUNA
from V_DB_ANALIZA_B_BROJ a
--where a.NAZIV = 'SUNDAY'
where a.plan_id=:p_plan_id
  AND a.zona_id=:p_zona_id
     

select distinct 
	   naziv
from ttip_dana
order by naziv	 


select * from nls_database_parameters 

select * from nls_session_parameters 