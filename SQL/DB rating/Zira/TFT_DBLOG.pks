CREATE OR REPLACE PACKAGE TFT_DBLOG IS
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * /
	Package: PACKAGE TFT_DBLOG, CREATED: 04.11.2000, Zira Ltd. - Software Department
	Copyright (c) Zira Ltd. 1999, 2000, 2001. All Rights Reserved.
	NAME & DESCRIPTION
		PACKAGE TFT_DBLOG - 	Package odrzava tablicu MDBEVENTLOG. Koriste ga procedure TFT_FINAL, TFT_LPARTIAL,
		                      TFT_AGG_PULSE, TFT_AGG_SUSP.
    PROCEDURE START_TRANSACTION - Procedura za Insert u MDBEVENTLOG. Svi potrebni podaci se predaju iz 
                                  parent procedure.
		PROCEDURE END_TRANSACTION   - Procedura za update MDBEVENTLOG i postavljanje STATUS na 'O' = OK.
		                              Ova procedura se poziva samo u slucaju uspjesnog izvrsenja.
    PROCEDURE WRITE_ERROR		    - Procedura za pisanje greske u MDBEVENTLOG. Ujedno se postavlja i 
                                  STATUS='F' = FAILURE                          
	NOTES
		Package ne koristi autonomne transakcije i potrebno ga je pazljivo koristiti kod pozivanja iz procedura.
		U narednoj verziji ce se upotrijebiti PRAGMA AUTONOMOUS TRANSACTION.
		/21.05.2001 omoguceno da package koristi autonomne transakcije. nihad
	MODIFIED   (DD.MM.RRRR)
		Software Department, 18.06.2001 - Paket prebacen i prilaogodjen za htbisa
		Software Department-Rijeka, 04.12.2000 - 	Paket modificiran, dodano 
																							EXECUTE immediate 'set transaction use rollback segment RBIG';																		;
																							nakon commit iskaza u proceduri : START_TRANSACTION;
		Software Department, 04.11.2000 - Paket kreiran
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
PROCEDURE START_TRANSACTION
 (p_ID 							IN NUMBER
 ,p_PROGRAMID 			IN VARCHAR2
 ,p_START_DATETIME 	IN DATE
 ,p_MIN_DATETIMEP  	IN DATE
 ,p_MAX_DATETIMEP  	IN DATE
 ,p_MIN_RECORDID  	IN NUMBER
 ,p_MAX_RECORDID  	IN NUMBER
 );
PROCEDURE END_TRANSACTION
 (p_ID 							IN NUMBER
 ,p_STOP_DATETIME 	IN DATE
 );
PROCEDURE WRITE_ERROR
 (p_ID 							IN NUMBER
 ,p_PROGRAMID				IN NUMBER
 ,p_ERROR_DATETIME 	IN DATE
 ,p_DESCRIPTION IN VARCHAR2
 );
 PROCEDURE UPDATE_TRANSACTION
(
p_ID						IN NUMBER,
p_PROCESS_ID		IN NUMBER
);
END TFT_DBLOG;
/

