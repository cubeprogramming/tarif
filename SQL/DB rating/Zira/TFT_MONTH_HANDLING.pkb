CREATE OR REPLACE PACKAGE BODY HTBISA.Tft_Month_Handling AS
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   PROCEDURE aggregate(
      p_degree    IN NUMBER,
      p_id        IN NUMBER,
      p_ciklus    IN VARCHAR2,
      p_final_mode IN VARCHAR2
   ) IS
   BEGIN


/*   	  if v_ok = 'O' then
--finalna odbrada duplih
         final_double(p_degree, p_id, p_ciklus);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod finalizacije TMTRAFFIC-a',
                status = 'O',
                stop_datetime = sysdate
          where id = p_id;
      end if;
*/
      IF v_ok = 'O' THEN
--finalizacija
         FINAL(p_degree, p_id, p_ciklus, p_final_mode);
      ELSE
         UPDATE MITBPROCESSLOG
            SET status_report =
                   status_report || CHR(13) || CHR(10) || SYSDATE
                   || ' GRESKA! Proces zaustavljen kod finalizacije TMTRAFFIC-a',
                STATUS = 'O',
                stop_datetime = SYSDATE
          WHERE ID = p_id;
      END IF;

      IF v_ok = 'O' THEN

         final_tbrecord(p_degree, p_id, p_ciklus);
      ELSE
         UPDATE MITBPROCESSLOG
            SET status_report =
                   status_report || CHR(13) || CHR(10) || SYSDATE
                   || ' GRESKA! Proces zaustavljen kod finaliziranja TBRECORD tablice ',
                STATUS = 'O',
                stop_datetime = SYSDATE
          WHERE ID = p_id;
      END IF;

      IF v_ok = 'O' THEN

         final_sms(p_ciklus, p_id);
      ELSE
         UPDATE MITBPROCESSLOG
            SET status_report =
                   status_report || CHR(13) || CHR(10) || SYSDATE
                   || ' GRESKA! Proces zaustavljen kod trazenja duplih u TSMSTRAFFIC ',
                STATUS = 'O',
                stop_datetime = SYSDATE
          WHERE ID = p_id;
      END IF;

   END;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   PROCEDURE final_double(p_degree IN NUMBER, p_id IN NUMBER, p_ciklus IN VARCHAR2)
   IS

    v_datmin TTRAFFIC.datumv_obrade%TYPE;
      v_datmax TTRAFFIC.datumv_obrade%TYPE;
      v_dstart TTRAFFIC.datumv_obrade%TYPE;
      v_recmin TTRAFFIC.record_id%TYPE;
      v_recmax TTRAFFIC.record_id%TYPE;
      v_recmaxold TTRAFFIC.record_id%TYPE;
      v_id NUMBER := 0;
      v_idalt NUMBER := -TO_NUMBER(TO_CHAR(SYSDATE, 'rrrrddmmhh24miss'));
      v_progid NUMBER := Get_Programid('FinalDouble');

   BEGIN
      BEGIN
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         IF p_degree != 0 THEN
            EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         END IF;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         EXECUTE IMMEDIATE 'set transaction use rollback segment RBIG';

         v_dstart := SYSDATE;
         v_id := Bg_Get_Local_Seqno('MDBEVENTLOG_SEQ');


         UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || ': FinalDouble-Start'
          WHERE ID = p_id;


         Tft_Dblog.start_transaction(v_id, v_progid, v_dstart, v_dstart,
            v_dstart, 0, 0);

         UPDATE MDBEVENTLOG
            SET processlog_id = p_id
          WHERE ID = v_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS THEN
            v_ok := 'F';

            IF v_id = 0 THEN
               v_id := v_idalt;
            END IF;

            Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
            RAISE;
      END;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Prepis duplih u tablicu TFT_DUPLI_SMALL. CDR-ovi se u cjelosti
prepisuju i ostaju u tablici do kraja mjeseca, kada ih finalna
procedura TFT_DUPLI_HANDLING prepisuje u TFT_DUPLI i brise.
Vidjeti TFT_DUPLI_HADNLING
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      BEGIN
         --dupli koji sada dolaze a postoje u TBRECORD-u
EXECUTE IMMEDIATE
         'insert into tft_dupli_small
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus)
            SELECT t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   t.flex_uus
            FROM   TMTRAFFIC t,
                   (SELECT mrezna_grupa, msisdn, LTRIM(TO_NUMBER(call_id)) call_id, datumv,
                           pozvani_broj, trajanje
                    FROM   TMTRAFFIC
                    INTERSECT
                    SELECT mrezna_grupa, msisdn, TO_CHAR(call_id), datumv,
                           pozvani_broj, trajanje
                    FROM   TFTRAFFIC PARTITION ('
                           || p_ciklus
                           || ' )) d
            WHERE  t.mrezna_grupa = d.mrezna_grupa AND t.msisdn = d.msisdn
                   AND t.datumv = d.datumv AND t.pozvani_broj = d.pozvani_broj
                   AND LTRIM(TO_NUMBER(t.call_id)) = d.call_id AND t.trajanje = d.trajanje';

         DELETE FROM TMTRAFFIC
               WHERE record_id IN(SELECT record_id
                                  FROM   TFT_DUPLI_SMALL);

         UPDATE MITBPROCESSLOG
            SET status_report =
                   status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                   || 'Okoncana obrada duplih iz presjeka TMTRAFFIC-a i TFTRAFFIC-a'
          WHERE ID = p_id;

         --dupli koji upadaju u jednom krugu
		 INSERT INTO TFT_DUPLI_SMALL
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus)
            SELECT t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   t.flex_uus
            FROM   TMTRAFFIC t,
                   (SELECT   mrezna_grupa, msisdn, call_id, datumv,
                             pozvani_broj, trajanje
                    FROM     TMTRAFFIC
                    GROUP BY mrezna_grupa,
                             msisdn,
                             call_id,
                             datumv,
                             pozvani_broj,
                             trajanje
                    HAVING   COUNT(*) > 1) d
            WHERE  t.mrezna_grupa = d.mrezna_grupa AND t.msisdn = d.msisdn
                   AND t.datumv = d.datumv AND t.pozvani_broj = d.pozvani_broj
                   AND t.call_id = d.call_id AND t.trajanje = d.trajanje;

         DELETE FROM TMTRAFFIC
               WHERE record_id IN(SELECT b.record_id
                                  FROM   TFT_DUPLI_SMALL A, TFT_DUPLI_SMALL b
                                  WHERE  A.mrezna_grupa = b.mrezna_grupa
                                         AND A.msisdn = b.msisdn
                                         AND A.call_id = b.call_id
                                         AND A.datumv = b.datumv
                                         AND A.trajanje = b.trajanje
                                         AND A.pozvani_broj = b.pozvani_broj
                                         AND A.record_id < b.record_id);

         UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || 'Okoncana obrada duplih iz TMTRAFFIC-a samog'
          WHERE ID = p_id;

         COMMIT;
         Tft_Dblog.end_transaction(v_id, SYSDATE);
      EXCEPTION
         WHEN OTHERS THEN
            v_ok := 'F';
            ROLLBACK;

            UPDATE MITBPROCESSLOG
               SET status_report =
                      status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                      || ': Greska kod lociranja duplih slogova mjesecne finalizacije '
             WHERE ID = p_id;

            Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
            RAISE;
      END;

      BEGIN
         v_id := Bg_Get_Local_Seqno('MDBEVENTLOG_SEQ');

         SELECT NVL(MIN(datumv), SYSDATE), NVL(MAX(datumv), SYSDATE),
                COUNT(*), NVL(SUM(impulsi), 0)
         INTO   v_datmin, v_datmax,
                v_recmin, v_recmax
         FROM   TFT_DUPLI_SMALL
         WHERE  record_id > v_recmaxold;

         Tft_Dblog.start_transaction(v_id, v_progid, v_dstart, v_datmin,
            v_datmax, v_recmin, v_recmax);

         UPDATE MDBEVENTLOG
            SET processlog_id = p_id
          WHERE ID = v_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_ok := 'F';

            IF v_id = 0 THEN
               v_id := v_idalt;
            END IF;

            Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
            RAISE;
      END;

        UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || ': SmallDouble-End'
          WHERE ID = p_id;

         Tft_Dblog.end_transaction(v_id, SYSDATE);
         COMMIT;
      EXCEPTION
         WHEN OTHERS THEN
            v_ok := 'F';
            ROLLBACK;

            UPDATE MITBPROCESSLOG
               SET status_report = status_report || CHR(13) || CHR(10)
                                   || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                   || ': Greska kod brisanja duplih slogova '
             WHERE ID = p_id;

            Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
            RAISE;
      END;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   PROCEDURE FINAL(
      p_degree    IN NUMBER,
      p_id        IN NUMBER,
      p_ciklus    IN VARCHAR2,
      p_final_mode IN VARCHAR2
   ) IS
   BEGIN
      Tft_Day_Handling.FINAL(p_degree, p_id, p_final_mode);
   END;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   PROCEDURE final_tbrecord(
      p_degree IN NUMBER,
      p_id    IN NUMBER,
      p_ciklus IN VARCHAR2
   ) IS
      v_datmin TTRAFFIC.datumv_obrade%TYPE;
      v_datmax TTRAFFIC.datumv_obrade%TYPE;
      v_dstart TTRAFFIC.datumv_obrade%TYPE;
      v_recmin TTRAFFIC.record_id%TYPE;
      v_recmax TTRAFFIC.record_id%TYPE;
      v_id NUMBER := 0;
      v_idalt NUMBER := -TO_NUMBER(TO_CHAR(SYSDATE, 'rrrrddmmhh24miss'));
      v_progid NUMBER := Get_Programid('FinalTbrecord');
   BEGIN
      BEGIN
         ROLLBACK;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         IF p_degree != 0 THEN
            EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         END IF;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         SELECT MIN(datumv), MAX(datumv), COUNT(*), SUM(impulsi)
         INTO   v_datmin, v_datmax, v_recmin, v_recmax
         FROM   TBRECORD;

         v_dstart := SYSDATE;
         v_id := Bg_Get_Local_Seqno('MDBEVENTLOG_SEQ');

         UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || ': Final Tbrecord Start'
          WHERE ID = p_id;

         Tft_Dblog.start_transaction(v_id, v_progid, v_dstart, v_dstart,
            v_dstart, 0, 0);

         UPDATE MDBEVENTLOG
            SET processlog_id = p_id
          WHERE ID = v_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS THEN
            v_ok := 'F';

            IF v_id = 0 THEN
               v_id := v_idalt;
            END IF;

            Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
            RAISE;
      END;

      ROLLBACK;

      EXECUTE IMMEDIATE 'set transaction use rollback segment RBIG';

--update kategorije poziva za obradu bonusa kod poluautomatskog prometa
	  UPDATE TBRECORD
	  SET katpoziva_id = '11113'
	  WHERE sifra_servisa = 'OP'
	  AND katpoziva_id = '10604'
	  AND datumv < TRUNC(LAST_DAY(TO_DATE(p_ciklus, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;

	  UPDATE TBRECORD
	  SET katpoziva_id = '16102'
	  WHERE sifra_servisa = 'OP'
	  AND katpoziva_id = '16101'
	  AND datumv < TRUNC(LAST_DAY(TO_DATE(p_ciklus, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;

	  UPDATE TBRECORD
	  SET katpoziva_id = '16204'
	  WHERE sifra_servisa = 'OP'
	  AND katpoziva_id = '16201'
	  AND datumv < TRUNC(LAST_DAY(TO_DATE(p_ciklus, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;


--nulti korak je arhiviranje tbrecord-a
      INSERT INTO ATBRECORD
         SELECT *
         FROM   TBRECORD
		 WHERE 	datumv < TRUNC(LAST_DAY(TO_DATE(p_ciklus, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;
--izvlacimo one koji ne pripadaju trenutnom ciklusu
     INSERT INTO TBRECORDDC SELECT * FROM TBRECORD
	 WHERE datumv >= TRUNC(LAST_DAY(TO_DATE(p_ciklus, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;


--finaliziramo samo one koji pripadaju trenutnom ciklusu
--prvi korak je finalizacija TBRECORD-a
      INSERT INTO TFTRAFFIC
         SELECT record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                sifra_servisa, sifra_sservisa, pozvani_broj, datumv, trajanje,
                otrajanje, impulsi, iznos, pcijena, bbroj_prefiks, zona_id,
                katpoziva_id, plan_id, tarifa_id, pretplatnik_id, korisnik_id,
                bgrupa_id, paket_id, Get_Udr_Time(datumv, datumv_obrade),
                flex_uus flex_uus
         FROM   TBRECORD
         WHERE  parent_ind != '1'
		 AND 	datumv < TRUNC(LAST_DAY(TO_DATE(p_ciklus, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;


--drugi korak je zakljucavanje TBONUS-a
      UPDATE TBONUS
         SET cycle_switch = change_time
       WHERE ociklus = p_ciklus;

--treci korak je truncate tbrecord-a?!?!?!?
      EXECUTE IMMEDIATE 'truncate table tbrecord drop storage';

	  COMMIT;
	  EXECUTE IMMEDIATE 'set transaction use rollback segment RBIG';

--vracamo trenutni ciklus natrag u tbrecord

	  INSERT INTO TBRECORD SELECT * FROM TBRECORDDC;

	  EXECUTE IMMEDIATE 'truncate table tbrecorddc drop storage';

      UPDATE MITBPROCESSLOG
         SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                             || ': Final Tbrecord -End'
       WHERE ID = p_id;

      Tft_Dblog.end_transaction(v_id, SYSDATE);
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         v_ok := 'F';

         UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || ': Greska kod Final TBrecord'
          WHERE ID = p_id;

         Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
         RAISE;
   END;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
PROCEDURE Final_Sms (p_PARTITION VARCHAR2, p_id NUMBER) IS

 v_datmin  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_datmax  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_dstart  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_recmin  TTRAFFIC.RECORD_ID%TYPE;
 v_recmax  TTRAFFIC.RECORD_ID%TYPE;

 v_id      NUMBER:=0;
 v_idalt   NUMBER:=-TO_NUMBER(TO_CHAR(SYSDATE,'rrrrddmmhh24miss'));
 v_progid  NUMBER:=Get_Programid('DoubleBDR'); -- Dupli BDR Program

BEGIN

 BEGIN

  EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL 8';

	EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DML PARALLEL 8';

	EXECUTE IMMEDIATE 'set transaction use rollback segment RBIG';

	SELECT MIN(datumv), MAX(datumv), COUNT(*), SUM(impulsi)
         INTO   v_datmin, v_datmax, v_recmin, v_recmax
         FROM   TSMSTRAFFIC;

         v_dstart := SYSDATE;
         v_id := Bg_Get_Local_Seqno('MDBEVENTLOG_SEQ');

         UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || ': Final SMS Start'
          WHERE ID = p_id;

         Tft_Dblog.start_transaction(v_id, v_progid, v_dstart, v_dstart,
            v_dstart, 0, 0);

         UPDATE MDBEVENTLOG
            SET processlog_id = p_id
          WHERE ID = v_id;

   EXCEPTION WHEN OTHERS THEN
    IF v_id = 0 THEN v_id:=v_idalt; END IF;
    Tft_Dblog.WRITE_ERROR(v_id, v_progid, SYSDATE, SQLERRM );
   RAISE;
 END;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Prepis duplih u tablicu TFT_DUPLI. CDR-ovi se u cjelosti prepisuju
i ostaju u tablici za slucaj da nesto krene lose kod brisanja.
Dupli se traze na cijelom prometu.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BEGIN
EXECUTE IMMEDIATE
'insert into tft_dupli
(
RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,
SIFRA_SERVISA  ,SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,
OTRAJANJE			 ,IMPULSI        ,IZNOS          ,PCIJENA				 ,BBROJ_PREFIKS  ,
ZONA_ID        ,KATPOZIVA_ID	 ,PLAN_ID        ,TARIFA_ID      ,PRETPLATNIK_ID ,
KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID       ,DATUMV_OBRADE  ,FLEX_UUS
)
SELECT t.record_id, t.naziv_datoteke,t.call_id,t.mrezna_grupa,
       t.msisdn,t.sifra_servisa,t.sifra_sservisa,t.pozvani_broj,
       t.datumv,t.trajanje,t.otrajanje,t.impulsi,t.iznos,t.pcijena,
       t.bbroj_prefiks,t.zona_id,t.katpoziva_id,t.plan_id,t.tarifa_id,
       t.pretplatnik_id,t.korisnik_id,t.bgrupa_id,t.paket_id,
       t.datumv_obrade,t.flex_uus
FROM   TSMSTRAFFIC  t,
 (SELECT mrezna_grupa, msisdn, call_id, datumv, pozvani_broj, trajanje, sifra_sservisa
	FROM TSMSTRAFFIC
	GROUP BY mrezna_grupa, msisdn, call_id, datumv, pozvani_broj, trajanje, sifra_sservisa
	HAVING COUNT(*) > 1) d
WHERE t.mrezna_grupa=d.mrezna_grupa
AND   t.msisdn=d.msisdn
AND   t.datumv=d.datumv
AND   t.pozvani_broj=d.pozvani_broj
AND   t.call_id=d.call_id
AND   t.trajanje=d.trajanje
AND   NVL(t.sifra_sservisa,0)=NVL(d.sifra_sservisa,0)';

COMMIT;



  Tft_Dblog.WRITE_ERROR(v_id, v_progid, SYSDATE, 'Priprema duplih SMS BDR-ova okon�ana.'||CHR(10)||'Izvje��e u slijede�em slogu log-a' );
  Tft_Dblog.END_TRANSACTION(v_id, SYSDATE);

  SELECT MIN(datumv_obrade), MAX(datumv_obrade), COUNT(*), NVL(SUM(impulsi),0)
  INTO   v_datmin, v_datmax, v_recmin, v_recmax
  FROM   TFT_DUPLI
  WHERE  sifra_servisa IN ('33','34','38');

BEGIN
 EXECUTE IMMEDIATE 'select MDBEVENTLOG_SEQ.NEXTVAL from dual' INTO v_id;

 Tft_Dblog.START_TRANSACTION(	v_id, v_progid, v_dstart, v_datmin, v_datmax, v_recmin, v_recmax);

   EXCEPTION WHEN OTHERS THEN
    IF v_id = 0 THEN v_id:=v_idalt; END IF;
    Tft_Dblog.WRITE_ERROR(v_id, v_progid, SYSDATE, SQLERRM );
   RAISE;
END;

		EXCEPTION WHEN OTHERS THEN
 	  Tft_Dblog.WRITE_ERROR(v_id, v_progid, SYSDATE, SQLERRM );
 	  RAISE;


END;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Pronadjeni dupli se brisu samo iz tabele na principu:
Zadnji duplicirani se brisu, a oni ranije finalizirani ostaju.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BEGIN

EXECUTE IMMEDIATE
'delete  from TSMSTRAFFIC  where record_id in ( select b.record_id '||
'from tft_dupli a, tft_dupli b '||
'where a.mrezna_grupa=b.mrezna_grupa '||
'and a.msisdn=b.msisdn '||
'and a.call_id=b.call_id '||
'and a.datumv=b.datumv '||
'and a.trajanje=b.trajanje '||
'and a.pozvani_broj=b.pozvani_broj '||
'and a.record_id < b.record_id)';


    --tft_consolidation.double_cdr;
		EXCEPTION WHEN OTHERS THEN
 	  Tft_Dblog.WRITE_ERROR(v_id, v_progid, SYSDATE, SQLERRM );
 	  RAISE;

END;

 -- Klaudio (ne treba vise)
 -- Tft_Telex.sms_popust(p_PARTITION);  --rebonusiram

 INSERT INTO TFTRAFFIC
         SELECT record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                sifra_servisa, sifra_sservisa, pozvani_broj, datumv, trajanje,
                otrajanje, impulsi, iznos, pcijena, bbroj_prefiks, zona_id,
                katpoziva_id, plan_id, tarifa_id, pretplatnik_id, korisnik_id,
                bgrupa_id, paket_id, Get_Udr_Time(datumv, datumv_obrade),
                flex_uus flex_uus
         FROM   TSMSTRAFFIC
         WHERE  datumv < TRUNC(LAST_DAY(TO_DATE(p_partition, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;


 COMMIT;

 		UPDATE MITBPROCESSLOG
           SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                             || ': Final SMS -End'
       	WHERE ID = p_id;


 Tft_Dblog.END_TRANSACTION(v_id, SYSDATE);

 EXECUTE IMMEDIATE 'truncate table tsmstraffic drop storage';

    EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         v_ok := 'F';

         UPDATE MITBPROCESSLOG
            SET status_report = status_report || CHR(13) || CHR(10) || TO_CHAR(SYSDATE,'dd.mm.rrrr hh24:mi:ss')
                                || ': Greska kod Finaliziranja TSMSTRAFFIC'
          WHERE ID = p_id;

         Tft_Dblog.write_error(v_id, v_progid, SYSDATE, SQLERRM);
         RAISE;

END Final_Sms;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
END Tft_Month_Handling;
/
