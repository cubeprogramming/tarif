CREATE OR REPLACE FUNCTION get_abroj (p_mg     IN VARCHAR2,
                    p_msisdn IN VARCHAR2,
                    p_date   IN DATE,
                    p_rez    IN VARCHAR2) RETURN NUMBER IS


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * /
	FUNCTION get_abroj, CREATED: 27.11.2000, Zira Ltd. - Software Department

	Copyright (c) Zira Ltd. 1999, 2000, 2001. All Rights Reserved.

	NAME, DESCRIPTION
		FUNCTION get_abroj - Funkcija pronalazi broj koji je napravio odredjeni traffic.

  PARAMETERS
    p_mg     - Mrezna grupa
		p_msisdn - A broj koji je napravio traffic
 	  p_date   - Datum
	NOTES

 	MODIFIED   (DD.MM.RRRR)

		Software Department, 11.06.2001 Funkcija prebacena na HTBISA, bez promjena
		Software Department, 27.11.2000 - Funkcija kreirana

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



v_korisnik    NUMBER:=0;
v_pretplatnik NUMBER:=0;
v_paket_id    NUMBER:=0;
BEGIN

BEGIN
SELECT /*+ INDEX(PPRETPLATNIK PPRETPLATNIK_RATING_IDX) */ KORISNIK_ID,ID, paket_id
INTO  v_korisnik,v_pretplatnik, v_paket_id
FROM	PPRETPLATNIK
WHERE MSISDN = p_msisdn
AND   MREZNA_GRUPA = p_mg
AND   DATUM_UKLJUCENJA <= p_date
AND   nvl(DATUM_ISKLJUCENJA, p_date) >= p_date;


   exception when no_data_found then -- ako nije u ppretplatnik onda bi trebo biti u ppretphist
			BEGIN
						SELECT KORISNIK_ID,PRETPLATNIK_ID, PAKET_ID INTO v_korisnik,v_pretplatnik, v_paket_id
            FROM  PPRETHIST
            WHERE MSISDN = p_msisdn
            AND   MREZNA_GRUPA = p_mg
            AND   DATUM_UKLJUCENJA <= p_date
            AND   DATUM_ISKLJUCENJA >= p_date
            AND   STATUS_TRANS = '0';

			exception when no_data_found then --

				     BEGIN
								SELECT P.KORISNIK_ID,P.ID, p.paket_id
								INTO v_korisnik,v_pretplatnik, v_paket_id
                FROM PPRETPLATNIK P
                WHERE P.ID  = (
                SELECT  nvl(B.PRETPLATNIK_ID,0)
                FROM PBROJ B
                WHERE   B.MREZNA_GRUPA = p_mg
                AND   ((B.MSISDN_DG <= p_msisdn AND   B.MSISDN_GG >= p_msisdn) OR B.MSISDN_KB = p_msisdn)
                AND   B.DATUM_UKLJUCENJA <= p_date
                AND   nvl(B.DATUM_ISKLJUCENJA, p_date) >= p_date);

				     exception when no_data_found then

				     	 BEGIN
                	 SELECT P.KORISNIK_ID,P.PRETPLATNIK_ID, p.paket_id
                   INTO v_korisnik,v_pretplatnik, v_paket_id
                   FROM PPRETHIST P
                   WHERE P.PRETPLATNIK_ID  = (SELECT
                                          nvl(BH.PRETPLATNIK_ID,0) FROM PBROJHIST BH
                                   WHERE  BH.MREZNA_GRUPA = p_mg
                                   AND   ((BH.MSISDN_DG <= p_msisdn AND   BH.MSISDN_GG >= p_msisdn) OR BH.MSISDN_KB = p_msisdn)
                                   AND    BH.DATUM_UKLJUCENJA <= p_date
                                   AND    nvl(BH.DATUM_ISKLJUCENJA, p_date) >= p_date)
                   AND STATUS_TRANS = '0';

               exception when no_data_found then

               BEGIN

          				 SELECT P.KORISNIK_ID,P.ID, p.paket_id
                   INTO v_korisnik,v_pretplatnik, v_paket_id
                   FROM PPRETPLATNIK P
                   WHERE P.ID  = (SELECT
                                          nvl(BH.PRETPLATNIK_ID,0) FROM PBROJHIST BH
                                   WHERE  BH.MREZNA_GRUPA = p_mg
                                   AND   ((BH.MSISDN_DG <= p_msisdn AND   BH.MSISDN_GG >= p_msisdn) OR BH.MSISDN_KB = p_msisdn)
                                   AND    BH.DATUM_UKLJUCENJA <= p_date
                                   AND    nvl(BH.DATUM_ISKLJUCENJA, p_date) >= p_date);

               exception when no_data_found then   v_korisnik:=0; v_pretplatnik := 0;
               END;
               END;
               END;
               END;

END;

  IF p_rez = 'K' THEN

  	return (v_korisnik);

  ELSIF p_rez = 'P' THEN

  	return (v_pretplatnik);
	ELSIF p_rez = 'S' THEN
    return (v_paket_id);
  END IF;
END GET_ABROJ;
/
