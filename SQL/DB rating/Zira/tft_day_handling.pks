CREATE OR REPLACE package tft_day_handling is
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   v_ok varchar2(1) := 'O';

   procedure aggregate(
      p_degree      number,
      p_id          number,
      p_ciklus      varchar2,
      p_final_mode in varchar2
   );

   procedure final(p_degree in number, p_id in number, p_mode in varchar2);

   procedure candidate4resusp(p_degree in number, p_id in number);

   procedure candidate4bd(
      p_degree in number,
      p_id     in number,
      p_ociklus in varchar2
   );

   procedure returnfrombd(
      p_korisnik_id in number,
      p_korisnik_ok in number,
      p_ociklus    in varchar2
   );

   procedure return2start(p_korisnik_id in number, p_ociklus in varchar2);
end tft_day_handling;
