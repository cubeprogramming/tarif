CREATE OR REPLACE package tft_hour_handling is
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /

   Header: PACKAGE tft_hour_handling, CREATED: 01.08.2003, Zira Ltd. - Software Department

   Copyright (c) Zira Ltd. 1999, 2000, 2001. All Rights Reserved.

   NAME, DESCRIPTION
      PACKAGE tft_hour_handling -

   PARAMETERS
      p_partition - Particija za mjesec  za koji ce se izvrsiti agregacija

   NOTES
      Procedura koristi Parallel Query Option. Cijela transakcija ima jedan COMMIT, sto znaci
      da se RedoLogs i Rollback Segment moraju pripremiti za nju. Transakcija se izvodi jednom mjesecno
      Transakcija se navodi na Rollback Segment RBIG.

   MODIFIED   (DD.MM.RRRR)

    Software Department, 15.06.2001 - Procedura modifikovana i prebacena na HTBISA, da odgovara novoj semi

    21.11.2000 - Support za UUS handling. Agregiranje se razlikuje za Event i Duration based
                 Charging Categories.
      Software Department, 04.11.2000 - Procedura kreirana

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   v_ok varchar2(1):='O';
   procedure aggregate(
      p_degree number,
      p_id         number,
      p_ciklus     varchar2
   );


   procedure load_tbtraffic(p_degree in number, p_id in number);

   procedure double_small(p_degree in number, p_id in number);

   procedure prebonus_handling(p_degree in number, p_id in number, p_ciklus in varchar2);

   procedure ccategory_handling(p_degree in number, p_id in number);

   procedure bonus_handling(
      p_degree in number,
      p_id    in number,
      p_ciklus in varchar2
   );

   procedure final_small(p_degree in number, p_id in number);

   type traffrec is record(
      record_id number,
      naziv_datoteke varchar2(30),
      call_id varchar2(10),
      mrezna_grupa varchar2(10),
      msisdn number(10),
      sifra_servisa varchar2(3),
      sifra_sservisa varchar2(3),
      pozvani_broj varchar2(23),
      datumv date,
      trajanje number(8),
      otrajanje number(8),
      impulsi number(6),
      iznos number(14, 2),
      pcijena number(14, 2),
      bbroj_prefiks varchar2(15),
      zona_id number,
      katpoziva_id number,
      plan_id number,
      tarifa_id number,
      pretplatnik_id number,
      korisnik_id number,
      bgrupa_id number(2),
      paket_id number(2),
      datumv_obrade date,
      flex_uus number
   );

   type traffcur is ref cursor
      return traffrec;

   procedure insertstavke(
      p_recor_orig number,
      p_rec       traffrec,
      p_record_id number,
      p_datumv    date,
      p_trajanje  number,
      p_otrajanje number,
      p_impulsi   number,
      p_iznos     number,
      p_pcijena   number,
      p_katpoziva number,
      p_flex_uus  number,
      p_parent_ind number
   );

   function get_correction_needed(
      p_paket_id    in number,
      p_katpoziva_id in number,
      p_tarifa_id   in number,
      p_tip         in varchar2
   )
      return number;

   procedure updatesusp(p_ciklus varchar2);

   procedure first60bonus(
      p_record_original      number,
      p_trec                 traffrec,
      v_amount         in out number,
      p_ociklus              varchar2,
      p_cijeli_ind           number
   );

   function issunday(p_datumv date, p_trajanje number)
      return varchar2;

   procedure tft_record_cut(
      p_rec       traffrec,
      p_datumv  in date,
      p_trajanje in number
   );

function isCorrectPart(p_sifra_sservisa varchar2, p_datumv date, p_trajanje number, p_predati_datum date)
return number;

procedure UDRMidnightCut(p_id number);

end tft_hour_handling;
