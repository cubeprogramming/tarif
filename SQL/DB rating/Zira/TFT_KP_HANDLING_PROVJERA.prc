CREATE OR REPLACE PROCEDURE HTBISA.tft_kp_handling_provjera (p_PARTITION VARCHAR2, p_DATUM DATE) IS

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	Header: PROCEDURE tft_kp_handling_provjera, CREATED: 21.05.2001, Zira Ltd. - Software Department

	Copyright (c) Zira Ltd. 1999, 2000,2001. All Rights Reserved.

	NAME, DESCRIPTION
	PROCEDURE tft_kp_handling       - 	Procedura sluzi za kontrolu  KRIVO PRIPISANIH.
																	Ovdje se insertuju podaci o krivo pripisanom prometu
																	u tablicu TFT_KRIVI_PRIPIS.

 	PARAMETERS
		p_PARTITION - Parametar koji se odnosi na particiju iz koje se brisu dupli slogovi.
		v_datum - datum koji se vraca u glavnu proceduru i kao globalni parametar predaje proceduri za ciscenje
		tabele TFT_KRIVI_PRIPIS

	NOTES
		Svi "krivo tarifirani" CDR-ovi se spremaju u poseban store (tablica TFT_KRIVI_PRIPIS) i
		cuvaju se do sljedeceg pokretanja procedure za obradu krivo pripisanih (nacelno je to
		do sljedeceg obracunskog ciklusa).

	MODIFIED   (DD.MM.RRRR)

		Software Department, 18.06.2001 - Procedura modifikovana za htbisa
		Software Department, 21.05.2001 - Procedura kreirana

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


--v_razlog1 TFT_KRIVI_PRIPIS.OPIS%TYPE := 'ISKLJUCENJE';
--v_razlog2 TFT_KRIVI_PRIPIS.OPIS%TYPE := 'POSTOJI NOVI RBK';
--v_razlog3 TFT_KRIVI_PRIPIS.OPIS%TYPE := 'UKLJUCENJE';
--v_razlog4 TFT_KRIVI_PRIPIS.OPIS%TYPE := 'POSTOJI DRUGI RBK';

 v_datmin  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_datmax  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_dstart  TTRAFFIC.DATUMV_OBRADE%TYPE:=p_datum;
 v_recmin  TTRAFFIC.RECORD_ID%TYPE;
 v_recmax  TTRAFFIC.RECORD_ID%TYPE;


 v_id      number:=0;
 v_idalt   number:=-to_number(to_char(sysdate,'rrrrddmmhh24miss'));
 v_progid  number:=get_programid('ErrCustomerID'); -- Krivi Pripis Program

 V_tekst varchar2(4000);
BEGIN

 BEGIN

  EXECUTE immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL 8';

	EXECUTE immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL 8';

	EXECUTE immediate 'set transaction use rollback segment RBIG';

  --v_dstart:=sysdate;


 EXECUTE immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual' INTO v_id;

 TFT_DBLOG.START_TRANSACTION(	v_id, v_progid, v_dstart, v_dstart, v_dstart, 0, 0);

   exception when others then
    IF v_id = 0 THEN v_id:=v_idalt; END IF;
    TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
   Raise;
 END;

BEGIN

 --TRUNCATE tablice tft_krivi_pripis (nova verzija nece raditi TRUNCATE)
 --EXECUTE IMMEDIATE
 --'TRUNCATE TABLE TFT_KRIVI_PRIPIS REUSE STORAGE';

dbms_output.put_line('Pocetak');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 1. Trazenje CDR-ova korisnika koji su nastali nakon sto je korisnik iskljucen

Ovi CDR-ovi, ako postoje, mozda i pripadaju bas tom korisniku, ali je podatak o iskljucenju
stigao kasno, pa CDR-ovi nisu otisli u suspendirane, ili je podatak o iskljucenju koji
je stigao kasno neprecizan (Dupla pogreska).
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

EXECUTE IMMEDIATE
'insert into tft_krivi_pripis
(
 RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,SIFRA_SERVISA  ,
 SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,OTRAJANJE      ,IMPULSI        ,
 IZNOS          ,PCIJENA        ,BBROJ_PREFIKS  ,ZONA_ID        ,KATPOZIVA_ID		,PLAN_ID        ,
 TARIFA_ID      ,PRETPLATNIK_ID ,KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID      ,DATUMV_OBRADE  ,
 FLEX_UUS				,OCIKLUS				,STATUS					,BD_IND					,BD_MODE)
 select t.record_id,
      	t.naziv_datoteke,
      	t.call_id,
      	t.mrezna_grupa,
      	t.msisdn,
      	t.sifra_servisa,
      	t.sifra_sservisa,
      	t.pozvani_broj,
      	t.datumv,
      	t.trajanje,
      	t.otrajanje,
      	t.impulsi,
      	t.iznos,
      	t.pcijena,
      	t.bbroj_prefiks,
      	t.zona_id,
      	t.katpoziva_id,
      	t.plan_id,
      	t.tarifa_id,
      	t.pretplatnik_id,
      	t.korisnik_id,
      	t.bgrupa_id,
      	t.paket_id,
      	t.datumv_obrade, flex_uus,'||''''||
      	p_PARTITION||''''||','||''''||'0'||''''||', '||''''||'1'||''''||','||''''||'D'||''''||'
from 		tftraffic partition ('||p_PARTITION||') t,
     	 (select korisnik_id, id, datum_iskljucenja
     	 	from ppretplatnik
      	where datum_iskljucenja is not null) p
where 	t.korisnik_id = p.korisnik_id
and     t.pretplatnik_id = p.id
and   	datumv > p.datum_iskljucenja';

--insert into pom (tekst) values (V_tekst);
commit;
--dbms_output.put_line(V_tekst);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Ostao sam u ppretplatnik tabeli, znaci samo sam iskljucen. Da li postoje CDR-ovi koji
su nastali nakon mog iskljucenja?
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /


dbms_output.put_line('Korak 1 - Kraj');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 2. Trazenje CDR-ova koji su pripisani korisniku koji se sada nalazi u povijesti

Korisnik se sada nalazi u povijesti jer se pojavio novi RBK na tom prikljucku, ali
u prometu postoje CDR-ovi koji su pripisani tom korisniku jer se korisnik u vrijeme
tarifiranja nije nalazio u povijesti. Informacija je kasnila.
Za razrjesenje se moze koristiti kolona korisnik_id_ok
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
EXECUTE IMMEDIATE
'insert into tft_krivi_pripis
(
 RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,SIFRA_SERVISA  ,
 SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,OTRAJANJE      ,IMPULSI        ,
 IZNOS          ,PCIJENA        ,BBROJ_PREFIKS  ,ZONA_ID        ,KATPOZIVA_ID		,PLAN_ID        ,
 TARIFA_ID      ,PRETPLATNIK_ID ,KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID      ,DATUMV_OBRADE  ,
 FLEX_UUS				,OCIKLUS				,STATUS					,BD_IND					,BD_MODE)
 select t.record_id,
      	t.naziv_datoteke,
      	t.call_id,
      	t.mrezna_grupa,
      	t.msisdn,
      	t.sifra_servisa,
      	t.sifra_sservisa,
      	t.pozvani_broj,
      	t.datumv,
      	t.trajanje,
      	t.otrajanje,
      	t.impulsi,
      	t.iznos,
      	t.pcijena,
      	t.bbroj_prefiks,
      	t.zona_id,
      	t.katpoziva_id,
      	t.plan_id,
      	t.tarifa_id,
      	t.pretplatnik_id,
      	t.korisnik_id,
      	t.bgrupa_id,
      	t.paket_id,
      	t.datumv_obrade, flex_uus,'||''''||
      	p_PARTITION||''''|| ', '||''''||'0'||''''||', '||''''||'2'||''''||','||''''||'U'||''''||'
from 		tftraffic partition ('||p_PARTITION||') t,
     	 (select korisnik_id, pretplatnik_id, min(datum_iskljucenja) datum_iskljucenja
     	 	from pprethist a
      	where status_trans='||''''||'0'||''''||'
		and exists (select 1 from ppretplatnik b where b.mrezna_grupa=a.mrezna_grupa
					   		   	 	  			   	 and b.msisdn = a.msisdn
													 and b.id != a.pretplatnik_id
													 and b.korisnik_id!=a.korisnik_id)
		group by korisnik_id, pretplatnik_id) p
where 	t.korisnik_id = p.korisnik_id
and     t.pretplatnik_id = p.pretplatnik_id
and   	t.datumv >= p.datum_iskljucenja';

commit;

--korak 2.b. proslim korakom zaobisli smo slogove kod kojih ne postoji pretplatnik id koji je naslijedio
--na novom rbk-u te se mora i taj slucaj pokriti
--ovaj slucaj kaze, daj mi iskljucene koji sada u pretplatniku nemaju nikoga da ih naslijedi na tom broju
EXECUTE IMMEDIATE
'insert into tft_krivi_pripis
(
 RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,SIFRA_SERVISA  ,
 SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,OTRAJANJE      ,IMPULSI        ,
 IZNOS          ,PCIJENA        ,BBROJ_PREFIKS  ,ZONA_ID        ,KATPOZIVA_ID		,PLAN_ID        ,
 TARIFA_ID      ,PRETPLATNIK_ID ,KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID      ,DATUMV_OBRADE  ,
 FLEX_UUS				,OCIKLUS				,STATUS					,BD_IND					,BD_MODE)
 select t.record_id,
      	t.naziv_datoteke,
      	t.call_id,
      	t.mrezna_grupa,
      	t.msisdn,
      	t.sifra_servisa,
      	t.sifra_sservisa,
      	t.pozvani_broj,
      	t.datumv,
      	t.trajanje,
      	t.otrajanje,
      	t.impulsi,
      	t.iznos,
      	t.pcijena,
      	t.bbroj_prefiks,
      	t.zona_id,
      	t.katpoziva_id,
      	t.plan_id,
      	t.tarifa_id,
      	t.pretplatnik_id,
      	t.korisnik_id,
      	t.bgrupa_id,
      	t.paket_id,
      	t.datumv_obrade, flex_uus,'||''''||
      	p_PARTITION||''''|| ', '||''''||'0'||''''||', '||''''||'2'||''''||','||''''||'U'||''''||'
from 		tftraffic partition ('||p_PARTITION||') t,
     	 (select korisnik_id, pretplatnik_id, min(datum_iskljucenja) datum_iskljucenja
     	 	from pprethist a
      	where status_trans='||''''||'0'||''''||'
		and not exists (select 1 from ppretplatnik b where b.mrezna_grupa=a.mrezna_grupa
					   		   	 	  			   	 and b.msisdn = a.msisdn)
		group by korisnik_id, pretplatnik_id) p
where 	t.korisnik_id = p.korisnik_id
and     t.pretplatnik_id = p.pretplatnik_id
and   	t.datumv >= p.datum_iskljucenja';

commit;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
Otisao sam u prethist, znaci dosao je novi rbk i potjerao me tamo. Da li postoji neki
CDR koji je moj, a nastao je nakon sto sam ja iskljucen?
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

dbms_output.put_line('Korak 2 - Kraj');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 3. Ovaj korak nije neophodan.

Naime, desi se update na datum_ukljucenja. Tarifiranje radi sa informacijom o jednom
datumu ukljucenja koja se kasnije promijeni. Promet mozda pripada korisniku, a mozda i ne.

Ovaj upit moze raditi vrlo dugo i treba ga pustiti ako bas ima dovoljno vremena.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
EXECUTE IMMEDIATE
'insert into tft_krivi_pripis
(
 RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,SIFRA_SERVISA  ,
 SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,OTRAJANJE      ,IMPULSI        ,
 IZNOS          ,PCIJENA        ,BBROJ_PREFIKS  ,ZONA_ID        ,KATPOZIVA_ID		,PLAN_ID        ,
 TARIFA_ID      ,PRETPLATNIK_ID ,KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID      ,DATUMV_OBRADE  ,
 FLEX_UUS				,OCIKLUS				,STATUS					,BD_IND					,BD_MODE)
 select t.record_id,
      	t.naziv_datoteke,
      	t.call_id,
      	t.mrezna_grupa,
      	t.msisdn,
      	t.sifra_servisa,
      	t.sifra_sservisa,
      	t.pozvani_broj,
      	t.datumv,
      	t.trajanje,
      	t.otrajanje,
      	t.impulsi,
      	t.iznos,
      	t.pcijena,
      	t.bbroj_prefiks,
      	t.zona_id,
      	t.katpoziva_id,
      	t.plan_id,
      	t.tarifa_id,
      	t.pretplatnik_id,
      	t.korisnik_id,
      	t.bgrupa_id,
      	t.paket_id,
      	t.datumv_obrade, flex_uus,'||''''||
      	p_PARTITION||''''|| ', '||''''||'0'||''''||', '||''''||'3'||''''||','||''''||'D'||''''||'
from 		tftraffic partition ('||p_PARTITION||') t,
     	 (select korisnik_id, id, datum_ukljucenja, paket_id
      	from ppretplatnik
      	where datum_aktiviranja != datum_ukljucenja
		and datum_ukljucenja < trunc(last_day(to_date('||''''||p_PARTITION||''''||', ''MONRRRR'', ''NLS_DATE_LANGUAGE = American'')))+1-1/86400)  p
where 	t.korisnik_id = p.korisnik_id
and     t.pretplatnik_id = p.id
and     t.datumv < p.datum_ukljucenja
and     t.paket_id=p.paket_id';  --dodala paket_id
/**/

--ovdje dodati dio koji zaobilazi SPOM
commit;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
Popravljen mi je datum ukljucenja. Da li postoje CDR-ovi koji su nastali prije nego
sam ja ukljucen. Ako postoje onda sigurno nisu moji.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


dbms_output.put_line('Korak 3 - Kraj');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 4. Trazenje CDR-ova koji su pripisani korisniku koji je zapravo bio POGRESAN, a
informacija o brisanju je dosla naknadno!!!

Korisnik je obrisan. Informacija o tome je dosla nakon sto je tarifiranje proslo i
pripisalo tom korisniku promet. Korisnik je bio aktivan u periodu:
         >>  datum_ukljucenja - sysdate transakcije brisanja  <<
u tom periodu mu je tarifiranje pripisivalo promet, tako da taj dio treba popraviti.
Nakon prelaska broja u PPRETHIST sa statusom transakcije '9' vise nije moglo doci
do pripisa prometa jer se o tome brine Analiza A-broja i zaobilazi takve korisnike.
Za razrjesenje se moze koristiti kolona korisnik_id_ok, tj. tretman ove greske se svodi
na tretiranje razloga 2 'POSTOJI NOVI RBK', ali je uveden razlog 4 zbog razlikovanja
ova dva razloga.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
EXECUTE IMMEDIATE
'insert into tft_krivi_pripis
(
 RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,SIFRA_SERVISA  ,
 SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,OTRAJANJE      ,IMPULSI        ,
 IZNOS          ,PCIJENA        ,BBROJ_PREFIKS  ,ZONA_ID        ,KATPOZIVA_ID		,PLAN_ID        ,
 TARIFA_ID      ,PRETPLATNIK_ID ,KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID      ,DATUMV_OBRADE  ,
 FLEX_UUS				,OCIKLUS				,STATUS					,BD_IND					,BD_MODE)
 select t.record_id,
      	t.naziv_datoteke,
      	t.call_id,
      	t.mrezna_grupa,
      	t.msisdn,
      	t.sifra_servisa,
      	t.sifra_sservisa,
      	t.pozvani_broj,
      	t.datumv,
      	t.trajanje,
      	t.otrajanje,
      	t.impulsi,
      	t.iznos,
      	t.pcijena,
      	t.bbroj_prefiks,
      	t.zona_id,
      	t.katpoziva_id,
      	t.plan_id,
      	t.tarifa_id,
      	t.pretplatnik_id,
      	t.korisnik_id,
      	t.bgrupa_id,
      	t.paket_id,
      	t.datumv_obrade, flex_uus,'||''''||
      	p_PARTITION||''''|| ', '||''''||'0'||''''||', '||''''||'4'||''''||','||''''||'U'||''''||'
from 		tftraffic partition ('||p_PARTITION||') t,
     	 (select korisnik_id, pretplatnik_id, datum_ukljucenja, datum_trans
     	 	from pprethist
      	where status_trans='||''''||'9'||''''||') p
where 	t.korisnik_id = p.korisnik_id
and     t.pretplatnik_id = p.pretplatnik_id
and   	t.datumv >= p.datum_ukljucenja
and     t.datumv <= p.datum_trans';

commit;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
Za brisanje u KTKU-u smo doznali dana: datum_trans. Do dana: datum_trans ja sam bio
ziv i normalan pretplatnik.
Svi CDR-ovi iz perioda dok nismo doznali da se desilo brisanje su sumnjivi. Oni koji su
iz perioda nakon ukljucenja su sigurno prosli u TFTRAFFIC i njih treba provjeriti
da li su za brisanje ili su za update.

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


dbms_output.put_line('Korak 4 - Kraj');

  TFT_DBLOG.WRITE_ERROR(v_id, v_progid, null, 'Priprema krivo pripisanih okoncana.'||chr(10)||'Izvje��e u slijedecem slogu log-a' );

END;

  TFT_DBLOG.END_TRANSACTION(v_id, sysdate);

	exception when others then
  TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
 	Raise;

END tft_kp_handling_provjera;
/

