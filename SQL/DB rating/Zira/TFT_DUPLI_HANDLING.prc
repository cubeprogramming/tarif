CREATE OR REPLACE PROCEDURE TFT_DUPLI_HANDLING (p_PARTITION VARCHAR2)

IS

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	Header: PROCEDURE tft_dupli_handling, CREATED: 26.01.2001, Zira Ltd. - Software Department

	Copyright (c) Zira Ltd. 1999, 2001. All Rights Reserved.

	NAME, DESCRIPTION
		PROCEDURE tft_dupli_handling - 	Procedura sluzi za kontrolu postojanja duplih slogova i
																		za brisanje istih iz particije prometa koja se prenosi kao parametar.
																		U prvom koraku se kontrolise postojanje duplih slogova i njihov prepis
																		u tablicu TFT_DUPLI, dok se u drugom koraku vrsi njihovo brisanje
																		iz zeljene particije prometa.

 	PARAMETERS
		p_PARTITION - Paramatear koji se odnosi na particiju iz koje se brisu dupli slogovi.

	NOTES
		Truncate tablice TFT_DUPLI se uvijek radi prije pokretanja INSERTA duplih slogova.

	MODIFIED   (DD.MM.RRRR)
		Software Department, 18.06.2001 - Procedura prebacena na htbisa i modifikovana
		Software Department, 26.01.2001 - Procedura kreirana

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 v_datmin  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_datmax  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_dstart  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_recmin  TTRAFFIC.RECORD_ID%TYPE;
 v_recmax  TTRAFFIC.RECORD_ID%TYPE;

 v_id      number:=0;
 v_idalt   number:=-to_number(to_char(sysdate,'rrrrddmmhh24miss'));
 v_progid  number:=get_programid('DoubleBDR'); -- Dupli BDR Program

BEGIN

 BEGIN

  EXECUTE immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL 8';

	EXECUTE immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL 8';

	EXECUTE immediate 'set transaction use rollback segment RBIG';

  v_dstart:=sysdate;

 EXECUTE immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual' INTO v_id;

 TFT_DBLOG.START_TRANSACTION(	v_id, v_progid, v_dstart, v_dstart, v_dstart, 0, 0);

   exception when others then
    IF v_id = 0 THEN v_id:=v_idalt; END IF;
    TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
   Raise;
 END;

 	-- TRUNCATE tablice tft_dupli
EXECUTE IMMEDIATE
'TRUNCATE TABLE TFT_DUPLI REUSE STORAGE';

	commit;
	EXECUTE immediate 'set transaction use rollback segment RBIG';
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Prepis duplih u tablicu TFT_DUPLI. CDR-ovi se u cjelosti prepisuju
i ostaju u tablici za slucaj da nesto krene lose kod brisanja.
Dupli se traze na cijelom prometu.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BEGIN
EXECUTE IMMEDIATE
'insert into tft_dupli
(
RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,
SIFRA_SERVISA  ,SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,
OTRAJANJE			 ,IMPULSI        ,IZNOS          ,PCIJENA				 ,BBROJ_PREFIKS  ,
ZONA_ID        ,KATPOZIVA_ID	 ,PLAN_ID        ,TARIFA_ID      ,PRETPLATNIK_ID ,
KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID       ,DATUMV_OBRADE  ,FLEX_UUS
)
select t.record_id, t.naziv_datoteke,t.call_id,t.mrezna_grupa,
       t.msisdn,t.sifra_servisa,t.sifra_sservisa,t.pozvani_broj,
       t.datumv,t.trajanje,t.otrajanje,t.impulsi,t.iznos,t.pcijena,
       t.bbroj_prefiks,t.zona_id,t.katpoziva_id,t.plan_id,t.tarifa_id,
       t.pretplatnik_id,t.korisnik_id,t.bgrupa_id,t.paket_id,
       t.datumv_obrade,t.flex_uus
from   tftraffic partition ('||p_PARTITION||') t,
 (select mrezna_grupa, msisdn, call_id, datumv, pozvani_broj, trajanje, sifra_sservisa
	from tftraffic partition ('||p_PARTITION||')
	where naziv_datoteke not like ''tt98%''
	group by mrezna_grupa, msisdn, call_id, datumv, pozvani_broj, trajanje ,sifra_sservisa
	having count(*) > 1) d
where t.mrezna_grupa=d.mrezna_grupa
and   t.msisdn=d.msisdn
and   t.datumv=d.datumv
and   t.pozvani_broj=d.pozvani_broj
and   t.call_id=d.call_id
and   t.trajanje=d.trajanje
and   nvl(t.sifra_sservisa,0)=nvl(d.sifra_sservisa,0)
and   nvl(t.sifra_servisa,0)!=''TLG''';

-- 1.08.2002 uvjet sifra_sservisa dodan zbog UUS-ova - Irena
-- 11.11.2002 na uvjet sifra_sservisa dosan nvl - Irena

	commit;
	EXECUTE immediate 'set transaction use rollback segment RBIG';
--Dodatak za FGSM
EXECUTE IMMEDIATE
'insert into tft_dupli
(
RECORD_ID      ,NAZIV_DATOTEKE ,CALL_ID        ,MREZNA_GRUPA   ,MSISDN         ,
SIFRA_SERVISA  ,SIFRA_SSERVISA ,POZVANI_BROJ   ,DATUMV         ,TRAJANJE       ,
OTRAJANJE			 ,IMPULSI        ,IZNOS          ,PCIJENA				 ,BBROJ_PREFIKS  ,
ZONA_ID        ,KATPOZIVA_ID	 ,PLAN_ID        ,TARIFA_ID      ,PRETPLATNIK_ID ,
KORISNIK_ID    ,BGRUPA_ID      ,PAKET_ID       ,DATUMV_OBRADE  ,FLEX_UUS
)
select t.record_id, t.naziv_datoteke,t.call_id,t.mrezna_grupa,
       t.msisdn,t.sifra_servisa,t.sifra_sservisa,t.pozvani_broj,
       t.datumv,t.trajanje,t.otrajanje,t.impulsi,t.iznos,t.pcijena,
       t.bbroj_prefiks,t.zona_id,t.katpoziva_id,t.plan_id,t.tarifa_id,
       t.pretplatnik_id,t.korisnik_id,t.bgrupa_id,t.paket_id,
       t.datumv_obrade,t.flex_uus
from   tftraffic partition ('||p_PARTITION||') t,
 (select mrezna_grupa, msisdn, datumv, pozvani_broj, trajanje, paket_id
	from tftraffic partition ('||p_PARTITION||')
	where naziv_datoteke like ''tt98%''
	group by mrezna_grupa, msisdn, datumv, pozvani_broj, trajanje, paket_id
	having count(*) > 1) d
where t.mrezna_grupa=d.mrezna_grupa
and   t.msisdn=d.msisdn
and   t.datumv=d.datumv
and   t.pozvani_broj=d.pozvani_broj
and   t.paket_id=d.paket_id
and   t.trajanje=d.trajanje
and   nvl(t.sifra_servisa,0)!=''TLG''';

commit;

  TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, 'Priprema duplih BDR-ova okon?ana.'||chr(10)||'Izvje��e u slijede�em slogu log-a' );
  TFT_DBLOG.END_TRANSACTION(v_id, sysdate);

  select min(datumv_obrade), max(datumv_obrade), count(*), nvl(sum(impulsi),0)
  into   v_datmin, v_datmax, v_recmin, v_recmax
  from   TFT_DUPLI;

begin
 EXECUTE immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual' INTO v_id;

 TFT_DBLOG.START_TRANSACTION(	v_id, v_progid, v_dstart, v_datmin, v_datmax, v_recmin, v_recmax);

   exception when others then
    IF v_id = 0 THEN v_id:=v_idalt; END IF;
    TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
   Raise;
end;

		exception when others then
 	  TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
 	  Raise;


END;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Pronadjeni dupli se brisu samo iz aktivne particije na principu:
Zadnji duplicirani se brisu, a oni ranije finalizirani ostaju.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BEGIN

EXECUTE IMMEDIATE
'delete /*+ INDEX(TFTRAFFIC,ATFTRECORD_ID_IDX) */ from tftraffic partition ('||p_PARTITION||') where record_id in ( select b.record_id '||
'from tft_dupli a, tft_dupli b '||
'where a.mrezna_grupa=b.mrezna_grupa '||
'and a.msisdn=b.msisdn '||
'and a.call_id=b.call_id '||
'and a.datumv=b.datumv '||
'and a.paket_id=b.paket_id '||
'and a.naziv_datoteke not like ''tt98%'''||
'and a.trajanje=b.trajanje '||
'and a.pozvani_broj=b.pozvani_broj '||
'and a.record_id < b.record_id)';

-- Delete za FGSM
EXECUTE IMMEDIATE
'delete /*+ INDEX(TFTRAFFIC,ATFTRECORD_ID_IDX) */ from tftraffic partition ('||p_PARTITION||') where record_id in ( select b.record_id '||
'from tft_dupli a, tft_dupli b '||
'where a.mrezna_grupa=b.mrezna_grupa '||
'and a.msisdn=b.msisdn '||
'and a.datumv=b.datumv '||
'and a.paket_id=b.paket_id '||
'and a.naziv_datoteke like ''tt98%'''||
'and a.trajanje=b.trajanje '||
'and a.pozvani_broj=b.pozvani_broj '||
'and a.record_id < b.record_id)';

 	  commit;

--ubacujemo male duple ovdje, da se ne bi obrisali iz tftraffic-a

	  insert into tft_dupli (RECORD_ID,NAZIV_DATOTEKE,CALL_ID,MREZNA_GRUPA,MSISDN,
				 SIFRA_SERVISA,SIFRA_SSERVISA,POZVANI_BROJ,DATUMV,
				 TRAJANJE,OTRAJANJE,IMPULSI,IZNOS,PCIJENA,BBROJ_PREFIKS,
				 ZONA_ID,PLAN_ID,TARIFA_ID,PRETPLATNIK_ID,KORISNIK_ID,
				 BGRUPA_ID,PAKET_ID,DATUMV_OBRADE,FLEX_UUS,KATPOZIVA_ID)
	  select 		 RECORD_ID,NAZIV_DATOTEKE,CALL_ID,MREZNA_GRUPA,MSISDN,
				 SIFRA_SERVISA,SIFRA_SSERVISA,POZVANI_BROJ,DATUMV,
				 TRAJANJE,OTRAJANJE,IMPULSI,IZNOS,PCIJENA,BBROJ_PREFIKS,
				 ZONA_ID,PLAN_ID,TARIFA_ID,PRETPLATNIK_ID,KORISNIK_ID,
				 BGRUPA_ID,PAKET_ID,DATUMV_OBRADE,FLEX_UUS,KATPOZIVA_ID
	  from tft_dupli_small
	  where datumv < trunc(last_day(to_date(p_partition, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;

	  delete from tft_dupli_small
	  where datumv < trunc(last_day(to_date(p_partition, 'MONRRRR', 'NLS_DATE_LANGUAGE = American')))+1;

	  commit;

	  TFT_DBLOG.END_TRANSACTION(v_id, sysdate);

		exception when others then
 	  TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
 	  Raise;

END;



END TFT_DUPLI_HANDLING;
/

