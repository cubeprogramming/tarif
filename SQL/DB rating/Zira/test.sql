select to_char(sysdate, 'MONRRRR',
                            'NLS_DATE_LANGUAGE=American') ociklus from dual
		 union
		 select to_char(add_months(sysdate, -1), 'MONRRRR',
                            'NLS_DATE_LANGUAGE=American') ociklus from dual
							where sysdate between trunc(sysdate, 'MONTH')
                     and trunc(sysdate, 'MONTH') + get_udroffset;
					 

select distinct 
  		 	korisnik_id, 
  		 	ociklus 
     from tbonus b
  	 where ociklus = 'JAN20005'
       and exists ( select 1 from tbtraffic@itbos a where a.korisnik_id = b.korisnik_id
	   and a.datumv < b.change_time
	   and a.datumv between to_date('JAN2005', 'MONRRRR','NLS_DATE_LANGUAGE=American')
                        and trunc(last_day(
					 	  	  to_date('JAN2005','MONRRRR','NLS_DATE_LANGUAGE=American')))
                           	    + 1 - 1 / 86400);		
								
select ociklus
         from (select distinct to_char(datumv, 'MONRRRR','NLS_DATE_LANGUAGE=American') ociklus
               from tbtraffic)
         order by to_date(ociklus, 'MONRRRR', 'NLS_DATE_LANGUAGE=American');			
		 
select trunc(sysdate) from dual		

select (get_sekunde(10000, 'US') - 1) / 86400 trajanje,
	   (to_date('25.01.2005 18:00:00','DD.MM.YYYY HH24:MI:SS')+ (get_sekunde(10000, 'US') - 1) / 86400) ptrajane,
	   to_char( (to_date('25.01.2005 18:20:00','DD.MM.YYYY HH24:MI:SS')+ (get_sekunde(10000, 'US') - 1) / 86400), 'HH24') sati,
	   to_number(to_char( (to_date('25.01.2005 18:00:00','DD.MM.YYYY HH24:MI:SS')+ (get_sekunde(10000, 'US') - 1) / 86400), 'HH24')) sati_n
from dual									  								 