CREATE OR REPLACE PROCEDURE "ANUM_ANALYSIS"   (p_mg     IN VARCHAR2,
                          p_msisdn IN VARCHAR2,
                          p_date   IN DATE,
                          p_pretid OUT VARCHAR2,
                          p_korid  OUT VARCHAR2,
                          p_paket  OUT NUMBER,
                          p_bgrupa OUT NUMBER) IS


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * /
	PROCEDURE anum_analysis, CREATED: 01.09.2001, Zira Ltd. - Software Department

	Copyright (c) Zira Ltd. 1999, 2000, 2001. All Rights Reserved.

	NAME, DESCRIPTION
		PROCEDURE ARATE.anum_analysis - Procedura koja izvrsava kompletnu analizu A-broja

  PARAMETERS
    p_mg     - Mrezna grupa
		p_msisdn - A broj koji je napravio traffic
 	  p_date   - Datum
	NOTES

 	MODIFIED   (DD.MM.RRRR)

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



v_korisnik    NUMBER:=0;
v_pretplatnik NUMBER:=0;
v_paket       NUMBER:=0;
v_bgrupa      NUMBER:=0;

BEGIN

BEGIN
SELECT /*+ INDEX(PPRETPLATNIK PPRETPLATNIK_RATING_IDX) */ KORISNIK_ID,ID,PAKET_ID,BGRUPA_ID
INTO  v_korisnik,v_pretplatnik,v_paket,v_bgrupa
FROM	PPRETPLATNIK
WHERE MSISDN = p_msisdn
AND   MREZNA_GRUPA = p_mg
AND   DATUM_UKLJUCENJA <= p_date
AND   nvl(DATUM_ISKLJUCENJA, p_date) >= p_date;


   exception when no_data_found then -- ako nije u ppretplatnik onda bi trebAo biti u pprethist
			BEGIN
						SELECT /*+ INDEX(PPRETHIST PPRETHIST_RATING_IDX) */
						       KORISNIK_ID,PRETPLATNIK_ID,PAKET_ID,BGRUPA_ID
						INTO v_korisnik,v_pretplatnik,v_paket,v_bgrupa
            FROM  PPRETHIST
            WHERE MSISDN = p_msisdn
            AND   MREZNA_GRUPA = p_mg
            AND   DATUM_UKLJUCENJA <= p_date
            AND   DATUM_ISKLJUCENJA >= p_date
            AND   STATUS_TRANS = '0';

			exception when no_data_found then -- Ili je mozda raspon u pitanju, ali rasponi nemaju UNIQUE constr.
				                                -- a u MSISDN_KB je spremljen kratki broj ili nositelj raspona, dakle
				                                -- najvjerovatniji A-broj u CDR-u, pa idemo ovako da pokusamo NE IZGUBITI VRIJEME

									BEGIN

										SELECT P.KORISNIK_ID,P.ID,PAKET_ID,BGRUPA_ID
										INTO v_korisnik,v_pretplatnik,v_paket,v_bgrupa
                		FROM PPRETPLATNIK P
                		WHERE P.ID  = (	SELECT  /*+ INDEX(PBROJ PBROJ_RATING_IDX) */
                		                        DISTINCT(nvl(B.PRETPLATNIK_ID,0))
                										FROM    PBROJ B
                										WHERE   B.MREZNA_GRUPA = p_mg
                										AND     (B.MSISDN_KB = p_msisdn OR (B.MSISDN_DG <= p_msisdn AND   B.MSISDN_GG >= p_msisdn))
                										AND     B.DATUM_UKLJUCENJA <= p_date
                										AND     nvl(B.DATUM_ISKLJUCENJA, p_date) >= p_date);

               exception when no_data_found then

               BEGIN

          				 SELECT P.KORISNIK_ID,P.ID,PAKET_ID,BGRUPA_ID
                   INTO   v_korisnik,v_pretplatnik,v_paket,v_bgrupa
                   FROM   PPRETPLATNIK P
                   WHERE  P.ID  = (SELECT  /*+ INDEX(PBROJHIST PBROJHIST_RATING_IDX) */
                                          DISTINCT nvl(BH.PRETPLATNIK_ID,0)
                                   FROM   PBROJHIST BH
                                   WHERE  BH.MREZNA_GRUPA = p_mg
                                   AND    (BH.MSISDN_KB = p_msisdn OR (BH.MSISDN_DG <= p_msisdn AND   BH.MSISDN_GG >= p_msisdn))
                                   AND    BH.DATUM_UKLJUCENJA <= p_date
                                   AND    nvl(BH.DATUM_ISKLJUCENJA, p_date) >= p_date);

               exception when no_data_found then

               BEGIN

          				 SELECT P.KORISNIK_ID,P.PRETPLATNIK_ID,PAKET_ID,BGRUPA_ID
                   INTO   v_korisnik,v_pretplatnik,v_paket,v_bgrupa
                   FROM   PPRETHIST P
                   WHERE  P.PRETPLATNIK_ID = (SELECT  /*+ INDEX(PBROJHIST PBROJHIST_RATING_IDX) */
                                                      DISTINCT nvl(BH.PRETPLATNIK_ID,0)
                                   						FROM    PBROJHIST BH
                                   						WHERE   BH.MREZNA_GRUPA = p_mg
                                   						AND     (BH.MSISDN_KB = p_msisdn OR (BH.MSISDN_DG <= p_msisdn AND   BH.MSISDN_GG >= p_msisdn))
                                   						AND     BH.DATUM_UKLJUCENJA <= p_date
                                   						AND     nvl(BH.DATUM_ISKLJUCENJA, p_date) >= p_date)
                    AND   STATUS_TRANS='0';

               exception when no_data_found then   v_korisnik:=0; v_pretplatnik := 0; v_paket:=0; v_bgrupa:=0;
               END;
               END;
               END;
               END;



END;

  p_pretid:=to_char(v_pretplatnik);
  p_korid :=to_char(v_korisnik);
  p_paket :=v_paket;
  p_bgrupa:=v_bgrupa;

END anum_analysis;
/
