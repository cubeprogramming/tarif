CREATE OR REPLACE PACKAGE HTBISA.TFT_MONTH_HANDLING AS

 v_ok varchar2(1) := 'O';

procedure final_double(p_degree in number, p_id in number, p_ciklus in varchar2);

procedure aggregate(p_degree in number, p_id in number, p_ciklus in varchar2, p_final_mode in varchar2);

procedure final(p_degree in number, p_id in number, p_ciklus in varchar2, p_final_mode in varchar2);

procedure final_tbrecord(p_degree in number, p_id in number, p_ciklus in varchar2);

procedure Final_Sms (p_PARTITION VARCHAR2, p_id in number);

END TFT_MONTH_HANDLING;
/
