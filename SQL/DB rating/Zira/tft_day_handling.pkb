/**
  * Dnevne obrade na kraju dana
  */
CREATE OR REPLACE package body tft_day_handling is

  /**
    * Procedura koja redom pokre�e sve procedure za dnevnu obradu
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
	* @param p_final_mode - da li je uklju�en mod finalizacije
    */		
   procedure aggregate(p_degree      number,
      		 		   p_id          number,
      				   p_ciklus      varchar2,
      				   p_final_mode in varchar2)
	is
      v_id number := p_id;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_min_record_id number;
      v_max_record_id number;
      v_count number;
      v_sqlerr varchar2(4000);
   begin
   	  --alter sesije da se fino upise datum u log i brojanje minimalnog i maksimalnog record id-a
      execute immediate 'alter session set nls_date_format = ''dd.mm.rrrr hh24:mi:ss''';

      select min(record_id), max(record_id), count(*)
      into   v_min_record_id, v_max_record_id, v_count
      from   ttraffic;

      --otvaranje loga

	  if v_ok = 'O' then  --status propagacije
	  --load bonusnog prometa za MiniHalo paket
         final(p_degree, v_id, p_final_mode);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen tokom dnevne finalizacije ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

      commit;
      rollback;                          --radi cannot read modify bla bla ora

      execute immediate 'ALTER SESSION DISABLE PARALLEL QUERY ';

      execute immediate 'ALTER SESSION DISABLE PARALLEL DML';


      if v_ok = 'O' then
	     --kontrolisanje duplog prometa u tbtrafficu
         candidate4resusp(0, v_id);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod kandidiranja za resuspenziju kod Mini Halo paketa',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

      if v_ok = 'O' then
	     --priprema za BDWriter
         candidate4bd(0, v_id, p_ciklus);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod Krivog pripisa Mini Halo paketa ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;


	  if v_ok = 'O' then
      update mitbprocesslog
         set status_report = status_report || chr(13) || chr(10) || sysdate
                             || ' Dnevna obrada zavr?ena ',
             status = 'O',
             stop_datetime = sysdate
       where id = v_id;
	   end if;

      commit;
   exception
      when others then
         rollback;
         v_sqlerr := sqlerrm;

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10)
                                || ' Greska na krovnoj proceduri ' || v_sqlerr
          where id = v_id;

         commit;
   --raise;
   end aggregate;

   
   /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
   Header: PROCEDURE tft_final, CREATED: 14.11.2000, Zira Ltd. - Software Department

   Copyright (c) Zira Ltd. 1999, 2000, 2001. All Rights Reserved.

   NAME /
      PROCEDURE tft_final   - Procedura izvrsava finalizaciju tarifiranja DATUM PO DATUM, tako da se
                              finalizirati moze i tokom noci bez nadzora. Sve informacije o izvrsenju ce se
                              naci u MBDEVENTLOG.
                              U MCEVENTLOG se obradjenim datotekama status postavlja na 'C' - Complete
                              RESTART mod se bazira na nastavku rada, tj. ukoliko tokom rada finalizacije na
                              nekom od datum dodje do greske, zapoceti proces se mopze ponovo pokrenuti sa
                              navodjenjem PROCESSLOG_ID koji nije uspjesno zavrsen.
                              RESTART MOD MOZE IZAZVATI NEZELJENE POSLJEDICE UKOLIKO SE NE POKRECE KOREKTNO!!!

   PARAMETERS
      p_MOD   - Dozvoljene vrijednosti: 'N' Normalni mod; 'R' Restart mod
      p_PID   - MITBPROCESSLOG.ID za koji ce se finalizacija vezati ili po kome ce se realizirati RESTART

   NOTES
      Procedura koristi Parallel Query Option. Cijela transakcija ima jedan COMMIT, sto znaci
      da se RedoLogs i Rollback Segment moraju pripremiti za nju.Transakcija se navodi na
      Rollback Segment RBIG.

   MODIFIED   (DD.MM.RRRR)

    Software Department, 18.07.2001 - Proceduri dodat RESTART MODE
      Software Department, 18.06.2001 - Procedura prebacena na htbisa i modifikovana da odgovara novoj shemi
      Software Department-Rijeka, 04.12.2000 - Procedura kreirana, promijenjen default mjesec
      Software Department-Rijeka, 14.01.2001 - Finalizacija na bazi datuma prisutnih u TTRAFFIC
      Software Department, 14.11.2000 - Procedura kreirana

   /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   procedure final(p_degree in number, p_id in number, p_mode in varchar2) is
      v_command varchar2(50);
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_dstart ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
      v_id number := 0;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_progid number := get_programid('FinalDay');      
	  
	  -- RATINGEND Program

      cursor cdat is
         select distinct trunc(datumv) datum
         from            tmtraffic
         order by        1;

      cursor cdatr is
         select distinct trunc(datumv) datum
         from            tmtraffic
         minus
         select distinct trunc(min_datetimep) datum
         from            mdbeventlog
         where           status = 'O' and processlog_id = p_id
         order by        1;
   begin
      rollback;

      execute immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL 16';

      execute immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL 16';

      execute immediate 'set transaction use rollback segment RBIG';

      if p_mode = 'N' then
         begin
            for cdat_rec in cdat loop
               begin
                  v_dstart := sysdate;

                  select min(datumv), max(datumv), count(*), sum(impulsi)
                  into   v_datmin, v_datmax, v_recmin, v_recmax
                  from   tmtraffic
                  where  datumv between cdat_rec.datum
                                    and cdat_rec.datum + 1 - 1 / 86400;

                  execute immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual'
                  into              v_id;

                  tft_dblog.start_transaction(v_id, v_progid, v_dstart,
                     cdat_rec.datum, v_datmax, v_recmin, v_recmax);

                  update mdbeventlog
                     set processlog_id = p_id
                   where id = v_id;

                  commit;
               exception
                  when others then
                     if v_id = 0 then
                        v_id := v_idalt;
                     end if;

                     tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
                     raise;
               end;

               commit;

               execute immediate 'set transaction use rollback segment RBIG';

               begin
                  insert      /*+ parallel(tftraffic, 16) */into tftraffic
                     select /*+ parallel(tmtraffic, 16) */
                            record_id, naziv_datoteke, call_id, mrezna_grupa,
                            msisdn, sifra_servisa, sifra_sservisa,
                            pozvani_broj, datumv, trajanje, otrajanje,
                            impulsi, iznos, pcijena, bbroj_prefiks, zona_id,
                            katpoziva_id, plan_id, tarifa_id, pretplatnik_id,
                            korisnik_id, bgrupa_id, paket_id,
                            get_udr_time(datumv, datumv_obrade), flex_uus
                     from   tmtraffic
                     where  datumv between cdat_rec.datum
                                       and cdat_rec.datum + 1 - 1 / 86400;

                  --tft_consolidation_kc.final(Cdat_rec.datum);
                  commit;
               exception
                  when others then
                     tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
                     raise;
               end;

               tft_dblog.end_transaction(v_id, sysdate);
			   /* * * * * Prvom prilikom prepraviti TFT_DBLOG.END_TRANSACTION da napravi i ovaj UPDATE !!!!!!!! * * * */
            end loop;

            update mceventlog
               set progstatus_code = 'C'
             where program_id = 2                            -- Rating program
                   and file_name_in in(select distinct naziv_datoteke
                                       from            ttraffic);

            commit;
            v_command := 'truncate table tmtraffic drop storage';

            execute immediate v_command;
         exception
            when others then
               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;
      elsif p_mode = 'R' and p_id is not null then
         begin
            for cdatr_rec in cdatr loop
               v_dstart := sysdate;

               begin
                  select id
                  into   v_id
                  from   mdbeventlog
                  where  processlog_id = p_id
                         and trunc(min_datetimep) = cdatr_rec.datum
                         and status = 'F';

                  update mdbeventlog
                     set start_datetime = sysdate,
                         error_description = error_description || 'Restart:',
                         status = 'R'
                   where processlog_id = p_id
                         and trunc(min_datetimep) = cdatr_rec.datum
                         and status = 'F';

                  commit;
               exception
                  when no_data_found then
                     begin
                        select min(datumv), max(datumv), count(*),
                               sum(impulsi)
                        into   v_datmin, v_datmax, v_recmin,
                               v_recmax
                        from   tmtraffic
                        where  datumv between cdatr_rec.datum
                                          and cdatr_rec.datum + 1 - 1 / 86400;

                        execute immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual'
                        into              v_id;

                        tft_dblog.start_transaction(v_id, v_progid, v_dstart,
                           cdatr_rec.datum, v_datmax, v_recmin, v_recmax);

                        update mdbeventlog
                           set processlog_id = p_id
                         where id = v_id;

                        commit;
                     exception
                        when others then
                           if v_id = 0 then
                              v_id := v_idalt;
                           end if;

                           tft_dblog.write_error(v_id, v_progid, sysdate,
                              sqlerrm);
                           raise;
                     end;
               end;

               begin
                  insert      /*+ parallel(tftraffic, 16) */into tftraffic
                     select /*+ parallel(tmtraffic, 16) */
                            record_id, naziv_datoteke, call_id, mrezna_grupa,
                            msisdn, sifra_servisa, sifra_sservisa,
                            pozvani_broj, datumv, trajanje, otrajanje,
                            impulsi, iznos, pcijena, bbroj_prefiks, zona_id,
                            katpoziva_id, plan_id, tarifa_id, pretplatnik_id,
                            korisnik_id, bgrupa_id, paket_id,
                            get_udr_time(datumv, datumv_obrade), flex_uus
                     from   tmtraffic
                     where  datumv between cdatr_rec.datum
                                       and cdatr_rec.datum + 1 - 1 / 86400;

                  --tft_consolidation_kc.final(Cdat_rec.datum);
                  commit;
               exception
                  when others then
                     tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
                     raise;
               end;

               tft_dblog.end_transaction(v_id, sysdate);

               update mdbeventlog
                  set processlog_id = p_id
                where id = v_id;
            end loop;

            update mceventlog
               set progstatus_code = 'C'
             where program_id = 2                            -- Rating program
                   and file_name_in in(select distinct naziv_datoteke
                                       from            ttraffic);

            --tft_consolidation_kc.final_upload;
            commit;
            v_command := 'truncate table tmtraffic drop storage';

            execute immediate v_command;
         exception
            when others then
               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;
      end if;
   end final;

   /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
   Procedura se bavi pronalazenjem novoukljucenih, za sada samo Mini Halo paketa, da bi smo bili sto
   precizniji na pokazu bonusa. Inace ovaj dio moze posluziti i za povecanje ukupne azurnosti sustava.
   BGW pravila pisanja log-ova i stavki u tablice ppretplatnik i kkorisnik:
   1. Ukoliko je min_datetimep null to znaci da nije bilo promjena u TKOR-u
   2. TKOR.DATPRO se prepisuje u DATUM_OBRADE u obje tablice
   3. PPRETPLATNIK.MSISDN nosi sysdate transakcije
   /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   procedure candidate4resusp(p_degree in number, p_id in number) is
/* @todo Preraditi tako da za novo uklju�enog korisnika bonus baziranih usluga,
    pogleda da li postoji promet u TSTRAFFIC i ako postoji onda treba izvr�iti resuspenziju */ 
   
      cursor ctkor is
         select id, centar_id, min(nvl(min_datetimep, max_datetimep)) mindat,
                max(max_datetimep) maxdat
         from   mbgeventlog
         where  trunc(start_datetime, 'DD') = trunc(sysdate, 'DD')
                and rgroup_name = 'TKOR_TRAS' and table_name like '%TKOR%'
                and max_datetimep != nvl(min_datetimep, max_datetimep)
                and status = 'O'
		 group by id, centar_id;

      cursor cpret(p_datmin in date, p_datmax in date, p_centar_id in number) is
         select id, korisnik_id, mrezna_grupa, msisdn, datum_ukljucenja,
                sysdate dattrans, '1' stat,
                decode(
                   paket_id,
                   52, 'tt98',
                   'tt' || ltrim(mrezna_grupa, '0')
                ) dat
         from   ppretplatnik
         where  datum_obrade between p_datmin and p_datmax and status = '1'
                and datum_iskljucenja is null and centar_id = p_centar_id
                and trunc(datum_msisdn, 'DD') = trunc(sysdate, 'DD')
                and paket_id in(50, 52);

      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_dstart ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
      v_id number := 0;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_progid number := get_programid('Candidate4Resusp');
                                                         
	  -- RATINGEND Program
	  
	  -- ovaj cursor ce imati vise redova, samo ako je BGW pustan vise puta u jednom danu
	  -- procedure ocekuje da se pokrece svaki dan
	  -- za sada nije planiran truncate ove tablice jer se ne ocekuje da ona drasticno raste
	  -- svako naredno pokretanje resuspenzija treba popeglati status u ovoj tablici
	  -- jer jednom kandidirani za resuspenzije trebaju biti procisceni do kraja

   begin
      begin
         rollback;

		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         if p_degree != 0 then
            execute immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            execute immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         end if;

		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         execute immediate 'set transaction use rollback segment RBIG';

         v_dstart := sysdate;
         v_id := bg_get_local_seqno('MDBEVENTLOG_SEQ');

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': Candidate4Resusp Start'
          where id = p_id;

         tft_dblog.start_transaction(v_id, v_progid, v_dstart, v_dstart,
            v_dstart, 0, 0);

         update mdbeventlog
            set processlog_id = p_id
          where id = v_id;

         commit;
      exception
         when others then
            v_ok := 'F';

            if v_id = 0 then
               v_id := v_idalt;
            end if;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      begin
	  -- prije nego pripremim nove kandidate sve postojece ponistim jer su oni vec procisceni
         update ttnpretplatnik
            set status = '0'
          where status = '1';

         for ctkor_rec in ctkor loop
            for cpret_rec in cpret(ctkor_rec.mindat, ctkor_rec.maxdat,
                               ctkor_rec.centar_id) loop
               insert into ttnpretplatnik
                    values (cpret_rec.id, cpret_rec.korisnik_id,
                            cpret_rec.mrezna_grupa, cpret_rec.msisdn,
                            cpret_rec.datum_ukljucenja, cpret_rec.dattrans,
                            cpret_rec.stat, cpret_rec.dat, ctkor_rec.id);
            end loop;
			-- bas DANAS im se nesto desilo, imaju Mini Halo i ukljuceni su, a nastali su iz DANASNJIH pokretanja BGW-a
         end loop;

		 -- za ove treba deaktivirati buduce punjenje sa SuspLoad4Bonus jer su oni postali pravi korisnici
         update tspretplatnik
            set status = '0'
          where (mrezna_grupa, msisdn) in(select mrezna_grupa, msisdn
                                          from   ttnpretplatnik
                                          where  status = '1')
                and status = '1';

		 --uklanjanje stavke iz tbonusa i arhiviranje
         insert into atsbonus
                     (archive_time, mrezna_grupa, msisdn, ociklus, bonus_type,
                      amount_min, amount_dub, change_time)
            select sysdate, mrezna_grupa, msisdn, ociklus, bonus_type,
                   amount_min, amount_dub, change_time
            from   tsbonus
            where  (mrezna_grupa, msisdn) in(select mrezna_grupa, msisdn
                                             from   ttnpretplatnik
                                             where  status = '1');

         delete from tsbonus
               where (mrezna_grupa, msisdn) in(select mrezna_grupa, msisdn
                                               from   ttnpretplatnik
                                               where  status = '1');
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report = status_report || chr(13) || chr(10)
                                   || sysdate
                                   || ': Greska kod pripreme za resuspenziju '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      tft_dblog.end_transaction(v_id, sysdate);
   end candidate4resusp;

   /**
    * Priprema za ubacivanje u BDWriter
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
    */
   procedure candidate4bd(p_degree in number,
      		 			  p_id     in number,
      					  p_ociklus in varchar2) 
   is
      cursor ctkor is                             --ipak ga necu upotrijebiti
         select id, centar_id, nvl(min_datetimep, max_datetimep) mindat,
                max_datetimep maxdat
         from   mbgeventlog
         where  trunc(start_datetime, 'DD') = trunc(sysdate, 'DD')
                and rgroup_name = 'TKOR_TRAS' and table_name like '%TKOR%'
                and max_datetimep != nvl(min_datetimep, max_datetimep);

      -- raspon TKOR.DATPRO koji se desio danas

      -- Cursor za select svih slogova koji ukazuju na potrebu za update TFTRAFFIC-a
      cursor cupdate is
         select distinct korisnik_id, korisnik_ok
         from            tft_bd_day
         where           bd_mode = 'U' and status = '0'
                         and korisnik_ok != korisnik_id
                         and paket_id = paket_ok;

	  -- Cursor za select svih slogova koje treba ukloniti iz TFTRAFFIC-a i vratiti u resuspendiranje
      cursor cdelete is
         select distinct korisnik_id korisnik_id
         from            tft_bd_day
         where           bd_mode = 'D' and status = '0' and korisnik_ok = 0;

      cursor cforcedelete is
         select distinct korisnik_id, korisnik_ok
         from            tft_bd_day
         where           bd_mode = 'M' and status = '0'
                         --and korisnik_ok != korisnik_id
                         and paket_id != paket_ok;

      cursor cdeletetraff is
         select  record_id
         from            tft_bd_day
         where           bd_mode = 'R' and status = '0';

      cursor ctest(v_korisnik_id in number) is
         select   min(datumv) mintime, max(datumv) maxtime,
                  bonusn.change_time ncutofftime,
                  bonusp.change_time pcutofftime, bonusp.bflag bflag_p,
                  bonusn.bflag bflag_n
         from     tft_bd_day t,
                  (select korisnik_id, sign(amount_min - 3600) bflag,
                          change_time
                   from   tbonus
                   where  bonus_type = 'N'
                   and ociklus = p_ociklus) bonusn,
                  (select korisnik_id, sign(amount_min - 3600) bflag,
                          change_time
                   from   tbonus
                   where  bonus_type = 'P'
                   and ociklus = p_ociklus) bonusp
         where    t.korisnik_id = v_korisnik_id
                  and t.korisnik_id = bonusp.korisnik_id
                  and t.korisnik_id = bonusn.korisnik_id
                  and bonusn.korisnik_id = bonusp.korisnik_id
                  and t.status = '0'                        --samo neobradjeni
         group by bonusn.change_time,
                  bonusp.change_time,
                  bonusp.bflag,
                  bonusn.bflag;

      cursor ctest_ok(v_korisnik_id in number) is
         select   min(datumv) mintime, max(datumv) maxtime,
                  bonusn.change_time ncutofftime,
                  bonusp.change_time pcutofftime, bonusp.bflag bflag_p,
                  bonusn.bflag bflag_n
         from     tft_bd_day t,
                  (select korisnik_id, sign(amount_min - 3600) bflag,
                          change_time
                   from   tbonus
                   where  bonus_type = 'N') bonusn,
                  (select korisnik_id, sign(amount_min - 3600) bflag,
                          change_time
                   from   tbonus
                   where  bonus_type = 'P') bonusp
         where    t.korisnik_ok = v_korisnik_id
                  and t.korisnik_ok = bonusp.korisnik_id
                  and t.korisnik_ok = bonusn.korisnik_id
                  and bonusn.korisnik_id = bonusp.korisnik_id
                  and t.status = '0'                        --samo neobradjeni
         group by bonusn.change_time,
                  bonusp.change_time,
                  bonusp.bflag,
                  bonusn.bflag;

      v_id number := bg_get_local_seqno('MDBEVENTLOG_seq');
      v_progid number := get_programid('BDDay');
      v_datmin date;
      v_datmax date;
      v_recmin number;
      v_recmax number;
      v_stmt varchar2(4000);
   begin
      begin                    --transakcija pripreme krivo pripsanih slogova
         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': Poetak pripreme krivo pripisanih slogova'
          where id = p_id;

         select min(datumv), max(datumv), count(*), sum(impulsi)
         into   v_datmin, v_datmax, v_recmin, v_recmax
         from   tbrecord;

         tft_dblog.start_transaction(v_id, v_progid, sysdate, v_datmin,
            v_datmax, v_recmin, v_recmax);

         update mdbeventlog
            set processlog_id = p_id
          where id = v_id;

         commit;

		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
		 KORAK 1. Ostao sam u ppretplatnik tabeli, znaci samo sam iskljucen. Da li postoje CDR-ovi koji
		 su nastali nakon mog iskljucenja?
		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         insert into tft_bd_day
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus, ociklus, parent_ind,
                      status, bd_ind, bd_mode)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   flex_uus, p_ociklus, t.parent_ind, '0', '1', 'D'
            from   tbrecord t,
                   (select korisnik_id, id, datum_iskljucenja
                    from   ppretplatnik
                    where  datum_iskljucenja is not null
                           and paket_id in(50, 52)) p
            where  t.korisnik_id = p.korisnik_id and t.pretplatnik_id = p.id
                   and datumv > p.datum_iskljucenja and t.parent_ind != '0';

         -- vidi uvjet!!!!!!! uvijet vidjen trebao bi da je dobar
         --commit;
         DBMS_OUTPUT.put_line('Korak 1 - Kraj');

		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
		 KORAK 2. Trazenje CDR-ova koji su pripisani korisniku koji se sada nalazi u povijesti

		 Korisnik se sada nalazi u povijesti jer se pojavio novi RBK na tom prikljucku, ali
		 u prometu postoje CDR-ovi koji su pripisani tom korisniku jer se korisnik u vrijeme
		 tarifiranja nije nalazio u povijesti. Informacija je kasnila.
		 Za razrjesenje se moze koristiti kolona korisnik_id_ok
		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         insert into tft_bd_day
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus, ociklus, parent_ind,
                      status, bd_ind, bd_mode)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   flex_uus, p_ociklus, t.parent_ind, '0', '2', ' '
            from   tbrecord t,
                   (select korisnik_id, pretplatnik_id, min(datum_iskljucenja) datum_iskljucenja
                    from   pprethist
                    where  status_trans = '0' and paket_id in(50, 52)
					and trunc(datum_trans, 'MM')=trunc (sysdate, 'MM')
					group by korisnik_id, pretplatnik_id) p
            where  t.korisnik_id = p.korisnik_id
                   and t.pretplatnik_id = p.pretplatnik_id
                   and t.datumv >= p.datum_iskljucenja and t.parent_ind != '0';

		    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
			Otisao sam u prethist, znaci dosao je novi rbk i potjerao me tamo. Da li postoji neki
			CDR koji je moj, a nastao je nakon sto sam ja iskljucen?
			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

         /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
         KORAK 3. Ovaj korak nije neophodan.

         Naime, desi se update na datum_ukljucenja. Tarifiranje radi sa informacijom o jednom
         datumu ukljucenja koja se kasnije promijeni. Promet mozda pripada korisniku, a mozda i ne.

         Ovaj upit moze raditi vrlo dugo i treba ga pustiti ako bas ima dovoljno vremena.
         /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         insert into tft_bd_day
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus, ociklus, parent_ind,
                      status, bd_ind, bd_mode)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   flex_uus, p_ociklus, t.parent_ind, '0', '3', ' '
            from   tbrecord t,
                   (select korisnik_id, id, datum_ukljucenja
                    from   ppretplatnik
                    where  datum_aktiviranja != datum_ukljucenja
                           and paket_id in(50, 52)) p
            where  t.korisnik_id = p.korisnik_id and t.pretplatnik_id = p.id
                   and t.datumv < p.datum_ukljucenja and parent_ind != '0';

         --commit;
		 
		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
		 Popravljen mi je datum ukljucenja. Da li postoje CDR-ovi koji su nastali prije nego
		 sam ja ukljucen. Ako postoje onda sigurno nisu moji.
		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         DBMS_OUTPUT.put_line('Korak 3 - Kraj');

		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
		 KORAK 4. Trazenje CDR-ova koji su pripisani korisniku koji je zapravo bio POGRESAN, a
		 informacija o brisanju je dosla naknadno!!!

		 Korisnik je obrisan. Informacija o tome je dosla nakon sto je tarifiranje proslo i
		 pripisalo tom korisniku promet. Korisnik je bio aktivan u periodu:
		          >>  datum_ukljucenja - sysdate transakcije brisanja  <<
		 u tom periodu mu je tarifiranje pripisivalo promet, tako da taj dio treba popraviti.
		 Nakon prelaska broja u PPRETHIST sa statusom transakcije '9' vise nije moglo doci
		 do pripisa prometa jer se o tome brine Analiza A-broja i zaobilazi takve korisnike.
		 Za razrjesenje se moze koristiti kolona korisnik_id_ok, tj. tretman ove greske se svodi
		 na tretiranje razloga 2 'POSTOJI NOVI RBK', ali je uveden razlog 4 zbog razlikovanja
		 ova dva razloga.
		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         insert into tft_bd_day
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus, ociklus, parent_ind,
                      status, bd_ind, bd_mode)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   flex_uus, p_ociklus, t.parent_ind, '0', '4', ' '
            from   tbrecord t,
                   (select korisnik_id, pretplatnik_id, datum_ukljucenja,
                           datum_trans
                    from   pprethist
                    where  status_trans in('9', 'S') and paket_id in(50, 52)) p
            where  t.korisnik_id = p.korisnik_id
                   and t.pretplatnik_id = p.pretplatnik_id
                   and t.datumv >= p.datum_ukljucenja
                   and t.datumv <= p.datum_trans and t.parent_ind != '0';

         --commit;

         /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
         Za brisanje u KTKU-u ili za rollback u SPOM-usmo doznali dana: datum_trans.
         Do dana: datum_trans ja sam bio ziv i normalan pretplatnik.
         Svi CDR-ovi iz perioda dok nismo doznali da se desilo brisanje su sumnjivi. Oni koji su
         iz perioda nakon ukljucenja su sigurno prosli u TFTRAFFIC i njih treba provjeriti
         da li su za brisanje ili su za update.
         /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

         /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
         KORAK 5. Korisnik je promijenio paket, i postao jedan od Novih. To doznajemo
         naknadno (nakon pocetka obracunskog ciklusa), tj. DANAS. Sav njegov promet u TFTRAFFIC
         nije dobar. Treba ga izbaciti iz TFTRAFFIC-a i ponovo ubaciti BDWriter
         Ako je nesto za njega vec bonusirano, ocekujemo da ce PRE_BONUS_PROCESSING to uociti
         i aktivirati kompletno re-bonusiranje za korisnika!!!!. Ovaj dio ocekuje da se pokrece
         dnevno!!!
         /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         execute immediate 'insert into tft_bd_day
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus, ociklus, parent_ind,
                      status, bd_ind, bd_mode)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   flex_uus,'
                           || '''' || p_ociklus || '''' || ', ' || ' ''1'' '
                           || ', ' || ' ''0'' ' || ',' || ' ''5'' ' || ', '
                           || ' ''R'' ' || ' from   tftraffic partition ('
                           || p_ociklus
                           || ') t,
										(select distinct p.id id, p.korisnik_id korisnik, p.paket_id paket
										 from ppretplatnik p, pprethist h
										 where p.mrezna_grupa=h.mrezna_grupa
										 and p.msisdn=h.msisdn
										 and p.datum_msisdn between trunc(sysdate, ''DD'' ) and trunc(sysdate, ''DD'' )+1-1/86400
										 and p.datum_ukljucenja between trunc(sysdate, ''MM'' )+1/86400
										 	 					and add_months(trunc(sysdate, ''MM'' ),1)-1/86400
										 and p.paket_id > h.paket_id
										 and p.paket_id >= 50
										 and h.status_trans=''9''
										 ) p
            where  t.korisnik_id = p.korisnik
                   and t.pretplatnik_id = p.id
                   and t.paket_id!=p.paket';

		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
		 KORAK 6. Trazenje CDR-ova koji su pripisani korisniku koji je bio POTS, izdat mu racun greskom
		 a trebao je biti MiniHalo.

		 Informacija da je to gre�ka kroz KTKU bi se trebala propagirati kroz KTKU na na�in da se
		 stari POTS isklju�i, uklju�i se na novom RBK-u paket_id 50. Tako �emo locirati ove slogove
		 ovog mjeseca u trenutnoj particiji i znacemo da oni pripadaju tom korisniku
		 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         execute immediate 'insert into tft_bd_day
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus, ociklus, parent_ind,
                      status, bd_ind, bd_mode)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   flex_uus,'
                           || '''' || p_ociklus || '''' || ', ''1'', '
                           || ' ''0'' , ''5'', ''R'' '
                           || 'from   tftraffic partition (' || p_ociklus
                           || ') t,
                   (select korisnik_id, pretplatnik_id, min(datum_iskljucenja) datum_iskljucenja
                    from   pprethist a
                    where  status_trans = ''0'' and exists
					(select 1 from ppretplatnik b where b.mrezna_grupa=a.mrezna_grupa
					   		   	 	  			   	 and b.msisdn = a.msisdn
													 and b.paket_id in (50, 52)
													 and b.id != a.pretplatnik_id
													 and b.korisnik_id != a.korisnik_id)
					and trunc(datum_trans, ''MM'')=trunc (sysdate, ''MM'')
					group by korisnik_id, pretplatnik_id
				   ) p
            where  t.korisnik_id = p.korisnik_id
                   and t.pretplatnik_id = p.pretplatnik_id
                   and t.datumv >= trunc(sysdate, ''MM'')
				   and t.datumv >= p.datum_iskljucenja ';
      --insert into stmt values (v_stmt);
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Greska kod lociranja krivo pripisanih slogova '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
      KORAK 7. Popravka korisnik_id kolone u tablici TFT_BD_BAY
      /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      begin
         update tft_bd_day
            set korisnik_ok = get_abroj(mrezna_grupa, msisdn, datumv, 'K'),
                pretplatnik_ok = get_abroj(mrezna_grupa, msisdn, datumv, 'P'),
                paket_ok = get_abroj(mrezna_grupa, msisdn, datumv, 'S')
          where status = '0';

		  --and     ociklus=p_PARTITION;
         update tft_bd_day                                         -- NO_OWNER
            set bd_mode = 'D'
          where korisnik_ok = 0 and status = '0' and bd_ind != '5';

         update tft_bd_day                         -- ADD2ANOTHER same package
            set bd_mode = 'U'
          where korisnik_ok != 0 and paket_id = paket_ok and status = '0' and bd_ind != '5';

         update tft_bd_day                    -- ADD2ANOTHER different package
            set bd_mode = 'M'
          where korisnik_ok != 0 and paket_id != paket_ok and status = '0'
                and bd_mode != 'R';
		 --and    ociklus=p_PARTITION;
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Greska kod pripreme krivo pripisanih slogova '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
      KORAK 8. Rjesavanje slucaja NO_OWNER
      /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      begin
         for cdelete_rec in cdelete loop
            for ctest_rec in ctest(cdelete_rec.korisnik_id) loop
               --ovaj cursor treba da vraca jedan i samo jedan row!!!
               if (
                   ctest_rec.bflag_p = 0 and ctest_rec.bflag_n = 0
                   and ctest_rec.mintime > ctest_rec.ncutofftime
                   and ctest_rec.mintime > ctest_rec.pcutofftime
                  ) then
                  -- svi ovi CDR su nastali nakon prelaska granica bonusa, oni sigurno nemaju djecu
                  -- samo brisi iz TBRECORD i vrati na FS, jer nisu niciji

                  --brisanje potencijalne djece tih slogova koja imaju parent_id = 0
                  delete from tbrecord
                        where parent_id in(
                                 select record_id
                                 from   tbrecord
                                 where  korisnik_id = cdelete_rec.korisnik_id
                                        and datumv between ctest_rec.mintime
                                                       and ctest_rec.maxtime)
                              and parent_ind = '0';

                  --brisanje tih slogova
                  delete from tbrecord
                        where korisnik_id = cdelete_rec.korisnik_id
                              and datumv between ctest_rec.mintime
                                             and ctest_rec.maxtime;
               else
			   -- ovako za sada, ne mogu da mislim sta se sve moze desiti i odlucujem da ponovim bonus za korisnika
                  -- Return2Tbtraffic: delete from tbrecord, insert u Tbtraffic, delete Tbrecors i update Tbonus

                  --slogove koji nisu nicije vlasnistvo moramo obrisati svakako, a zatim
                  --ono sto ostane ubaciti u proces rebonusiranja, ok!!!!

                  --brisanje potencijalne djece tih slogova
                  delete from tbrecord
                        where parent_id in(
                                 select record_id
                                 from   tbrecord
                                 where  korisnik_id = cdelete_rec.korisnik_id
                                        and datumv between ctest_rec.mintime
                                                       and ctest_rec.maxtime)
                              and parent_ind = '0';

                  --brisanje tih slogova
                  delete from tbrecord
                        where korisnik_id = cdelete_rec.korisnik_id
                              and datumv between ctest_rec.mintime
                                             and ctest_rec.maxtime;

                  return2start(cdelete_rec.korisnik_id, p_ociklus);
                  null;
               end if;
            end loop;
         end loop;
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report = status_report || chr(13) || chr(10)
                                   || sysdate
                                   || ': Greska kod obrade NOOWNER slogova '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	  KORAK 9. Rjesavam slucaj ADD2ANOTHER same package
	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      begin
         for cupdate_rec in cupdate loop
		 -- Rjesava pitanja korisnik_id
            for ctest_rec in ctest(cupdate_rec.korisnik_id) loop
               --ovaj cursor treba da vraca jedan i samo jedan row!!!
               if (
                   ctest_rec.bflag_p = 0 and ctest_rec.bflag_n = 0
                   and ctest_rec.mintime > ctest_rec.ncutofftime
                   and ctest_rec.mintime > ctest_rec.pcutofftime
                  ) then
                  --ako promet
                  delete from tbrecord
                        where parent_id in(
                                 select record_id
                                 from   tbrecord
                                 where  korisnik_id = cupdate_rec.korisnik_id
                                        and datumv between ctest_rec.mintime
                                                       and ctest_rec.maxtime)
                              and parent_ind = '0';

                  --ovih uopste ne bi trebalo biti

                  --posto ovi cdr-ovi ne utjecu na bonus korisnika
                  --samim tim ne bi trebalo biti iscjepanih cdr-ova

                  --update slogova
                  delete from tbrecord
                        where korisnik_id = cupdate_rec.korisnik_id
                              and datumv between ctest_rec.mintime
                                             and ctest_rec.maxtime;

                  --nisu moji, prebaci ih onome ciji su
                  --tj. vrati novom korisniku promet u TBTRAFFIC i zgazi mu sve bonuse
                  returnfrombd(cupdate_rec.korisnik_id,cupdate_rec.korisnik_ok, p_ociklus);
               --samo ih i brises
               else
                  --dakle slogovi afektiraju bonus staroga korisnika
                  --sada ces starom da obrises, a da vratis natrag sa novim id-om, ok?
                     -- Return2Tbtraffic: insert u Tbtraffic, delete Tbrecord i update Tbonus
                  delete from tbrecord
                        where parent_id in(
                                 select record_id
                                 from   tbrecord
                                 where  korisnik_id = cupdate_rec.korisnik_id
                                        and datumv between ctest_rec.mintime
                                                       and ctest_rec.maxtime)
                              and parent_ind = '0';

                  delete from tbrecord
                        where korisnik_id = cupdate_rec.korisnik_id
                              and datumv between ctest_rec.mintime
                                             and ctest_rec.maxtime;

                  --vrati promet orginalnog korisnika kojeg se sva ova prica ne tice
                  return2start(cupdate_rec.korisnik_id, p_ociklus);
                      --nisu moji, posto si ih updateirao na korisnik_ok samim tim moras
                  --za njega da vratis promet iz tft_bd_day
                  returnfrombd(cupdate_rec.korisnik_id,
                     cupdate_rec.korisnik_ok, p_ociklus);
               end if;
            end loop;
         end loop;
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Greska kod obrade ADD2ANOTHER SAME PACKAGE slogova '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	  KORAK 10. Rjesavam slucaj ADD2ANOTHER different package
	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      begin
         for cforcedelete_rec in cforcedelete loop
		 -- Rjesava pitanja korisnik_id
            for ctest_rec in ctest(cforcedelete_rec.korisnik_id) loop
               --ovaj cursor treba da vraca jedan i samo jedan row!!!
               if (
                   ctest_rec.bflag_p = 0 and ctest_rec.bflag_n = 0
                   and ctest_rec.mintime > ctest_rec.ncutofftime
                   and ctest_rec.mintime > ctest_rec.pcutofftime
                  ) then
                  -- svi ovi CDR su nastali nakon prelaska granica bonusa, oni sigurno nemaju djecu
                  -- svi oni se trebaju otpisati korisnik_id -u
                  -- delete TBrecord je solucija za njih

                  --brisanje potencijalne djece tih slogova
                  delete from tbrecord
                        where parent_id in(
                                 select record_id
                                 from   tbrecord
                                 where  korisnik_id =
                                                 cforcedelete_rec.korisnik_id
                                        and datumv between ctest_rec.mintime
                                                       and ctest_rec.maxtime)
                              and parent_ind = '0';

                  --brisanje tih slogova
                  delete from tbrecord
                        where korisnik_id = cforcedelete_rec.korisnik_id
                              and datumv between ctest_rec.mintime
                                             and ctest_rec.maxtime;

                  null;
               else
			   -- ovi slogovi utjecu na bonus orginalnog korisnika, te treba izvrsiti rebonusiranje
                 -- : insert u Tbtraffic, delete Tbrecord i update Tbonus

                  --brisanje potencijalne djece tih slogova
                  delete from tbrecord
                        where parent_id in(
                                 select record_id
                                 from   tbrecord
                                 where  korisnik_id =
                                                 cforcedelete_rec.korisnik_id
                                        and datumv between ctest_rec.mintime
                                                       and ctest_rec.maxtime)
                              and parent_ind = '0';

                  --brisanje tih slogova
                  delete from tbrecord
                        where korisnik_id = cforcedelete_rec.korisnik_id
                              and datumv between ctest_rec.mintime
                                             and ctest_rec.maxtime;

                  --sve sto je bilo prije vrati
                  return2start(cforcedelete_rec.korisnik_id, p_ociklus);
               end if;
            end loop;
         end loop;
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Greska kod obrade ADD2ANOTHER DIFFERENT PACKAGE slogova '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      begin
         for cdeletetraff_rec in cdeletetraff loop
--       v_stmt := 'delete from tftraffic partition ('|| p_ociklus || ')' || 'where korisnik_id = '|| cdeletetraff_rec.korisnik_id;
            execute immediate 'delete from tftraffic partition ('
                              || p_ociklus || ')' || 'where record_id = '||cdeletetraff_rec.record_id;
/*       execute immediate 'delete from tftraffic partition ('|| p_ociklus || ')' || 'where korisnik_id = '
                              || cdeletetraff_rec.korisnik_id;*/
         end loop;
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Greska kod brisanja prometa is TFTRAFFIC-a za promjenjene pakete '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      begin
         update tft_bd_day
            set bd_mode = 'D'
          where bd_mode in('M', 'R') and status = '0'; -- priprema za BDWriter

                 update tft_bd_day
                    set status = '1'
                  where status = '0' and bd_mode!='D';
         null;
      exception
         when others then
            rollback;

            update mitbprocesslog
               set status_report = status_report || chr(13) || chr(10)
                                   || sysdate
                                   || ': Greska kod pripreme za BDWRITER '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      --tft_hour_handling.bonus_handling(0, v_id, p_ociklus);
      tft_dblog.end_transaction(v_id, sysdate);
   end candidate4bd;

   /**
    * Procedura vra�a TBRECORD u TBTRAFFIC
	* @param p_korisnik_id - ID korisnika
	* @param p_korisnik_ok - da li je korisnik validan
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
    */
   procedure return2start(p_korisnik_id in number, p_ociklus in varchar2) is
   begin
      update tbonus
         set amount_min = 0,
             amount_dub = 0,
             change_time = trunc(change_time, 'MONTH')
       where korisnik_id = p_korisnik_id and ociklus = p_ociklus;

      insert into tbtraffic
                  (record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                   sifra_servisa, sifra_sservisa, pozvani_broj, datumv,
                   trajanje, otrajanje, impulsi, iznos, pcijena,
                   bbroj_prefiks, zona_id, katpoziva_id, plan_id, tarifa_id,
                   pretplatnik_id, korisnik_id, bgrupa_id, paket_id,
                   datumv_obrade, flex_uus)
         select record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                sifra_servisa, sifra_sservisa, pozvani_broj, datumv, trajanje,
                otrajanje, impulsi, get_sekunde(otrajanje, 'US') / 60 * 0.56,
                get_sekunde(otrajanje, 'US') / 60 * 0.56, bbroj_prefiks,
                zona_id, 10604, plan_id, tarifa_id, pretplatnik_id,
                korisnik_id, bgrupa_id, paket_id, datumv_obrade, flex_uus
         from   tbrecord
         where  korisnik_id = p_korisnik_id and ociklus = p_ociklus
                and parent_ind != '0';

      delete from tbrecord
            where korisnik_id = p_korisnik_id and ociklus = p_ociklus;
--      commit;
   end;

   /**
    * Procedura vra�a novom korisniku promet u TBTRAFFIC i zgazi mu sve bonuse
	* @param p_korisnik_id - ID korisnika
	* @param p_korisnik_ok - da li je korisnik validan
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
    */
   procedure returnfrombd(p_korisnik_id in number,
      		 			  p_korisnik_ok in number,
      					  p_ociklus    in varchar2) 
   is
   begin
      update tbonus
         set amount_min = 0,
             amount_dub = 0,
             change_time = trunc(change_time, 'MONTH')
       where korisnik_id = p_korisnik_ok and ociklus = p_ociklus;

      insert into tbtraffic
                  (record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                   sifra_servisa, sifra_sservisa, pozvani_broj, datumv,
                   trajanje, otrajanje, impulsi, iznos, pcijena,
                   bbroj_prefiks, zona_id, katpoziva_id, plan_id, tarifa_id,
                   pretplatnik_id, korisnik_id, bgrupa_id, paket_id,
                   datumv_obrade, flex_uus)
         select record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                sifra_servisa, sifra_sservisa, pozvani_broj, datumv, trajanje,
                otrajanje, impulsi, get_sekunde(otrajanje, 'US') / 60 * 0.56,
                get_sekunde(otrajanje, 'US') / 60 * 0.56, bbroj_prefiks,
                zona_id, 10604, plan_id, tarifa_id, pretplatnik_ok,
                korisnik_ok, bgrupa_id, paket_ok, datumv_obrade, flex_uus
         from   tft_bd_day
         where  korisnik_id = p_korisnik_id and korisnik_ok = p_korisnik_ok
                and ociklus = p_ociklus and bd_mode = 'U'
                and paket_id = paket_ok and parent_ind != '0' and status='0';
   end;
end;
