CREATE OR REPLACE PROCEDURE HTBISA.tft_kp_handling_ciscenje(p_PARTITION VARCHAR2, p_PREDATI_DATUM DATE) IS


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	Header: PROCEDURE tft_kp_handling_ciscenje, CREATED: 21.05.2001, Zira Ltd. - Software Department

	Copyright (c) Zira Ltd. 1999, 2000,2001. All Rights Reserved.

	NAME, DESCRIPTION
		PROCEDURE tft_kp_handling_ciscenje - 	Procedura sluzi za  ispravku KRIVO PRIPISANIH.
																	Ovdje se intervenise nad krivo pripisanim prometom ovisno
																	od slucaja po kojem je napravljen	krivi pripis.

 	PARAMETERS
		p_PARTITION - Parametar koji se odnosi na particiju iz koje se brisu dupli slogovi.
    p_PREDATI_DATUM - vrijeme kada je poceo upis u tabelu TFT_KRIVI_PRIPIS koje se dobija iz
    procedure koja je izvrsila taj upis.

	NOTES
		Svi "krivo tarifirani" CDR-ovi se spremaju u poseban store (tablica TFT_KRIVI_PRIPIS) i
		cuvaju se do sljedeceg pokretanja procedure za obradu krivo pripisanih (nacelno je to
		do sljedeceg obracunskog ciklusa).

	MODIFIED   (DD.MM.RRRR)

		Software Department, 18.06.2001 - Procedura prebacena na htbisa i modifikovana da odgovara shemi
		Software Department, 21.05.2001 - Procedura kreirana

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


-- Cursor za select svih slogova koji ukazuju na potrebu za update TFTRAFFIC-a

cursor cupdate is
	select 	record_id, korisnik_ok,pretplatnik_ok
  from 		tft_krivi_pripis
  where 	bd_mode='U'
  and   	ociklus=p_PARTITION
  and     status='0';
--  and     korisnik_ok != 0;


-- Cursor za select svih slogova koje treba ukloniti iz TFTRAFFIC-a i vratiti u resuspendiranje

cursor cdelete is
	select	record_id
  from		tft_krivi_pripis
	where		bd_mode='D'
	and     ociklus=P_PARTITION
	and     status='0';
--	and     korisnik_ok=0;



 v_datmin  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_datmax  TTRAFFIC.DATUMV_OBRADE%TYPE;
 v_dstart  TTRAFFIC.DATUMV_OBRADE%TYPE := p_predati_datum;
 v_recmin  TTRAFFIC.RECORD_ID%TYPE;
 v_recmax  TTRAFFIC.RECORD_ID%TYPE;

 v_id      number:=0;
 v_idalt   number:=-to_number(to_char(sysdate,'rrrrddmmhh24miss'));
 v_progid  number:=get_programid('ErrCustomerID'); -- Krivi Pripis Program


BEGIN

	select min(datumv_obrade), max(datumv_obrade), count(*), nvl(sum(impulsi),0)
  into   v_datmin, v_datmax, v_recmin, v_recmax
  from   TFT_KRIVI_PRIPIS
  where 	ociklus = p_partition;

	BEGIN

	EXECUTE immediate 'ALTER SESSION DISABLE PARALLEL DML ';

	EXECUTE immediate 'ALTER SESSION DISABLE PARALLEL QUERY ';

	EXECUTE immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual' INTO v_id;


 	TFT_DBLOG.START_TRANSACTION(	v_id, v_progid, sysdate, v_datmin, v_datmax, v_recmin, v_recmax);

   exception when others then
    IF v_id = 0 THEN v_id:=v_idalt; END IF;
    TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
   Raise;

	END;



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 4. Popravka korisnik_id kolone u tablici TFTRAFFIC
Na bazi korisnik_ok se moze izvrsiti popravka u TFTRAFFIC.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BEGIN
update 	tft_krivi_pripis
set			korisnik_ok    = get_abroj(mrezna_grupa, msisdn, datumv,'K'),
        pretplatnik_ok = get_abroj(mrezna_grupa, msisdn, datumv,'P'),
		paket_ok = get_abroj(mrezna_grupa, msisdn, datumv, 'S'),
        bd_mode='D'
where   status='0'
and     ociklus=p_PARTITION;


update tft_krivi_pripis
set    bd_mode='U'
where  korisnik_ok!=0
and    status='0'
and    ociklus=p_PARTITION
and	   plan_id = get_plan_id(paket_ok, datumv);



commit;
dbms_output.put_line('Korak 4 - Kraj');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 5. Popravka na TFTRAFFIC
Ovi slucajevi se rjesavaju sa UPDATE - ovako zbog index-a
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
begin

for cupd_rec in cupdate loop

EXECUTE IMMEDIATE
	'update /*+ INDEX(TFTRAFFIC,ATFTRECORD_ID_IDX) */ tftraffic partition ('||p_PARTITION||') '||
	'set    korisnik_id = '||cupd_rec.korisnik_ok||', pretplatnik_id = '||cupd_rec.pretplatnik_ok||
	' where  record_id = '||cupd_rec.record_id;

  commit;
end loop;

end;
dbms_output.put_line('Korak 5 - Kraj');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
KORAK 6. Popravka na TFTRAFFIC
Ova vrsta problema se rjesava sa DELETE.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
begin

for cdel_rec in cdelete loop

EXECUTE IMMEDIATE
	'delete /*+ INDEX(TFTRAFFIC,ATFTRECORD_ID_IDX) */ from tftraffic partition ('||p_PARTITION||') '||
	'where  record_id = '||cdel_rec.record_id;

  commit;
end loop;


dbms_output.put_line('Korak 6 - Kraj');
end;

END;


 TFT_DBLOG.END_TRANSACTION(v_id, sysdate);

  exception when others then
  TFT_DBLOG.WRITE_ERROR(v_id, v_progid, sysdate, SQLERRM );
  Raise;

END tft_kp_handling_ciscenje;
/

