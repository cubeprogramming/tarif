CREATE OR REPLACE function HTBISA.get_plan_id(p_paket_id in number, p_datumv in date)
   return number is
   v_plan_id number := 0;

begin

   begin
      select plan_id
      into   v_plan_id
      from   ppaket_tplan
      where  p_datumv between datum_start and nvl(datum_stop, sysdate + 1)
             and status = '1' and paket_id = p_paket_id
             and plan_id<90;
   exception
      when others then
         v_plan_id := 9999;
   end;

   return v_plan_id;
end get_plan_id;
/

