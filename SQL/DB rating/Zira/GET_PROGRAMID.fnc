/**
  * Funkcija koja vra�a ID programa na osnovu proslje�enog imena
  * @param p_progname - ime programa
  * @return int - vra�a identifikator programa
  */
CREATE OR REPLACE FUNCTION GET_PROGRAMID(P_PROGNAME  IN  VARCHAR2) RETURN  NUMBER
IS
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * /
	FUNCTION GET_PROGRAMID, CREATED: 04.11.2000, Zira Ltd. - Software Department
 
	Copyright (c) Zira Ltd. 1999, 2000. All Rights Reserved.
 
	NAME & DESCRIPTION
		FUNCTION GET_PROGRAMID - Funkcija vraca Program_id za predato ime programa. Koristi se u DB procedurama i u
		                         Billing skripti. 
		                           
  PARAMETERS
    p_progname     - Naziv programa
    
	NOTES
    Izbjegavanje fiksiranja PROGRAM_ID u kodovima programa sustava.
 	MODIFIED   (DD.MM.RRRR)

		Software Department, 04.11.2000
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 -- PL/SQL Specification
v_progid  MPROGRAM.ID%TYPE;
 
-- PL/SQL Block
BEGIN
	
 BEGIN 
  select id 
  into   v_progid
  from   MPROGRAM
  where  name=p_progname;
  
  exception 
 	
  	when no_data_found then v_progid := 999999; -- Standard.
  	when too_many_rows then v_progid := 888888; -- Standard.
  	
  END;
  
 return (v_progid); 

END;
/
