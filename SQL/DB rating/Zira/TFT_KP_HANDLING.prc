CREATE OR REPLACE PROCEDURE HTBISA.tft_kp_handling (p_PARTITION VARCHAR2)
IS

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	Header: PROCEDURE tft_kp_handling_nova, CREATED: 21.05.2001, Zira Ltd. - Software Department

	Copyright (c) Zira Ltd. 1999, 2000,2001. All Rights Reserved.

	NAME, DESCRIPTION
		PROCEDURE tft_kp_handling_nova - 	Procedura sluzi za kontrolu i ispravku KRIVO PRIPISANIH.
																	U pozivu prve procedure insertuju se podaci o krivo pripisanom prometu
																	u tablicu TFT_KRIVI_PRIPIS, a nakon toga se u pozivu druge procedure intervenise nad
																	krivo pripisanim prometom ovisno od slucaja po kojem je napravljen
																	krivi pripis.

 	PARAMETERS
		p_PARTITION - Parametar koji se odnosi na particiju iz koje se brisu dupli slogovi.

	NOTES
		Svi "krivo tarifirani" CDR-ovi se spremaju u poseban store (tablica TFT_KRIVI_PRIPIS) i
		cuvaju se do sljedeceg pokretanja procedure za obradu krivo pripisanih (nacelno je to
		do sljedeceg obracunskog ciklusa).

	MODIFIED   (DD.MM.RRRR)
		Software Department, 18.06.2001 - Procedura prebacena na HTBISA i modifikovana da odgovara novoj semi
		Software Department, 21.05.2001 - Procedura kreirana

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


v_datum TTRAFFIC.DATUMV_OBRADE%TYPE:=sysdate;

BEGIN



tft_kp_handling_provjera  (p_PARTITION, v_datum);

tft_kp_handling_ciscenje  (p_PARTITION, v_datum);


END tft_kp_handling;
/

