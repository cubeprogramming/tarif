CREATE OR REPLACE FUNCTION bg_get_local_seqno (p_SEQUENCE_NAME IN VARCHAR2)
RETURN NUMBER IS

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	Header: FUNCTION bg_get_local_seqno, CREATED: 04.11.2000, Zira Ltd. - Software Department
 
	Copyright (c) Zira Ltd. 1999, 2000. All Rights Reserved.
 
	NAME & DESCRIPTION
		FUNCTION bg_get_local_seqno - Genericka funkcija koja vraca sljedecu vrijednost sekvence 
																	za proslijedjeni naziv sekvence u okviru sistemske sheme 
																	(sekvenca je lokalnog karaktera).

	PARAMETERS
		p_SEQUENCE_NAME - Naziv sekvence koja se koristi za odgovarajuce transakcije.

	NOTES
		Procedura kreirana za potrebe Billing Gateway-a.

	MODIFIED   (DD.MM.RRRR)
		Software Department, 04.11.2000 - Funkcija kreirana
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

v_seqno NUMBER;

BEGIN

EXECUTE IMMEDIATE
'SELECT	'||p_SEQUENCE_NAME||'.NEXTVAL FROM DUAL' INTO v_seqno;

return(v_seqno);

END bg_get_local_seqno;
/
