/**
  * Satne obrade koje vr�e odvajanje prometa za Mini Halo pakete i bonusiranje
  */
CREATE OR REPLACE package body tft_hour_handling is

   /**
    * Procedura koja redom pokre�e sve procedure za satnu obradu
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
    */
   procedure aggregate(p_degree number, p_id number, p_ciklus varchar2) is
      v_id number := p_id;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_min_record_id number;
      v_max_record_id number;
      v_count number;
      v_sqlerr varchar2(4000);
   begin
   	  --alter sesije da se fino upise datum u log i brojanje minimalnog i maksimalnog record id-a
      execute immediate 'alter session set nls_date_format = ''dd.mm.rrrr hh24:mi:ss''';

      select min(record_id), max(record_id), count(*)
      into   v_min_record_id, v_max_record_id, v_count
      from   ttraffic;

      --otvaranje loga
      commit;

      if v_ok = 'O' then --status propagacije
	     --load bonusnog prometa za MiniHalo paket
         load_tbtraffic(0, v_id);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod load TBTRAFFIC-a, ispitati u MDBEVENTLOG-u ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;


      if v_ok = 'O' then
	  	 -- Re�e slog koji prelazi kraj mjeseca na dva sloga i bri�e stari slog
         udrmidnightcut(v_id);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod rezanja slogova na kraju mjeseca, ispitati u MDBEVENTLOG-u ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

      if v_ok = 'O' then
	     --kontrolisanje duplog prometa u tbtrafficu
         double_small(0, v_id);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod obrade duplih, ispitati u MDBEVENTLOG-u ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

      if v_ok = 'O' then
	  	 --prebonus handling
         prebonus_handling(0, v_id, p_ciklus);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod PreBonusiranja za MiniHalo pakete ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

      if v_ok = 'O' then
	  	 --obracun bonusa
         bonus_handling(0, v_id, p_ciklus);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod procesiranje bonusa za MiniHalo, ispitati u MDBEVENTLOG-u ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

      if v_ok = 'O' then
	  	 --prebacivanje podataka u TMTRAFFIC
         final_small(0, v_id);
      else
         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || ' GRESKA! Proces zaustavljen kod male finalizacije, ispitati u MDBEVENTLOG-u ',
                status = 'F',
                stop_datetime = sysdate
          where id = v_id;
      end if;

	  if v_ok = 'O' then

      update mitbprocesslog
         set status_report = status_report || chr(13) || chr(10) || sysdate
                             || ' Obracun bonusa za MiniHalo i HaloPlus okoncan ',
             status = 'O',
             stop_datetime = sysdate
       where id = v_id;

	   end if;

      commit;
   exception
      when others then
         rollback;
         v_sqlerr := sqlerrm;

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10)
                                || ' Greska na krovnoj proceduri ' || v_sqlerr
          where id = v_id;

         commit;
   --raise;
   end aggregate;

   /**
    * Odvajanje prometa za bonusirane pakete (Mini Halo i EURO25) u TBTRAFFIC
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
    */
   procedure load_tbtraffic(p_degree in number, p_id in number) is
      v_command varchar2(50);
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
      v_id number := 0;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_progid number := get_programid('LoadTBTRAFFIC');
	  v_datum_obrade date := sysdate;
   begin
      begin
         if p_degree != 0 then
            execute immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            execute immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         end if;

         execute immediate 'set transaction use rollback segment RBIG';

         begin
            v_id := bg_get_local_seqno('MDBEVENTLOG_SEQ');

            update mitbprocesslog
               set status_report = status_report || chr(13) || chr(10)
                                   || sysdate || ': Load TBTRAFFIC -Start'
             where id = p_id;

            tft_dblog.start_transaction(v_id, v_progid, sysdate, sysdate,
               sysdate, 0, 0);

            update mdbeventlog
               set processlog_id = p_id
             where id = v_id;

            commit;
         exception
            when others then
               v_ok := 'F';

               if v_id = 0 then
                  v_id := v_idalt;
               end if;

               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;

         begin
            insert into tbtraffic
               select record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus
               from   ttraffic
               where  get_correction_needed(paket_id, katpoziva_id,
                         tarifa_id, 'BONUS') = 1
                      and iscorrectpart(nvl(sifra_sservisa, 'XXX'), datumv,
                            trajanje, v_datum_obrade) = 1;

            -- za sada necu raditi delete, stvar bi trebalo rijesiti selektivnim finaliziranjem u TMTRAFFIC
            commit;

            update mitbprocesslog
               set status_report = status_report || chr(13) || chr(10)
                                   || sysdate || ': Load TBTRAFFIC -End'
             where id = p_id;

            tft_dblog.end_transaction(v_id, sysdate);
         exception
            when others then
               rollback;
               v_ok := 'F';

               update mitbprocesslog
                  set status_report = status_report || chr(13) || chr(10)
                                      || sysdate
                                      || ': Gre�ka kod Load TBTRAFFIC'
                where id = p_id;

               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;
      end;
   end load_tbtraffic;

   /**
    * �i��enje duplih slogova unutar tablice TBTRAFFIC i 
	* na presjeku tablica TBTRAFFIC I TMTRAFFIC
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log 
    */
   procedure double_small(p_degree in number, p_id in number) is
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_dstart ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
      v_recmaxold ttraffic.record_id%type;
      v_id number := 0;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_progid number := get_programid('SmallDouble');
   begin
      begin
         if p_degree != 0 then
            execute immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            execute immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         end if;

         execute immediate 'set transaction use rollback segment RBIG';

         v_dstart := sysdate;
         v_id := bg_get_local_seqno('MDBEVENTLOG_SEQ');

         select nvl(max(record_id), 0)
         into   v_recmaxold
         from   tft_dupli_small;

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': SmallDouble-Start'
          where id = p_id;

         tft_dblog.start_transaction(v_id, v_progid, v_dstart, v_dstart,
            v_dstart, 0, 0);

         update mdbeventlog
            set processlog_id = p_id
          where id = v_id;

         commit;
      exception
         when others then
            v_ok := 'F';

            if v_id = 0 then
               v_id := v_idalt;
            end if;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	  Prepis duplih u tablicu TFT_DUPLI_SMALL. CDR-ovi se u cjelosti
	  prepisuju i ostaju u tablici do kraja mjeseca, kada ih finalna
	  procedura TFT_DUPLI_HANDLING prepisuje u TFT_DUPLI i brise.
	  Vidjeti TFT_DUPLI_HADNLING
	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      begin
         --dupli koji sada dolaze a postoje u TBRECORD-u (tablici sa izbonusiranim prometom)
         insert into tft_dupli_small
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   t.flex_uus
            from   tbtraffic t,
                   (select mrezna_grupa, msisdn, call_id, datumv,
                           pozvani_broj, trajanje
                    from   tbtraffic
                    intersect
                    select mrezna_grupa, msisdn, call_id, datumv,
                           pozvani_broj, trajanje
                    from   tbrecord
                    where  parent_ind != '0') d
            where  t.mrezna_grupa = d.mrezna_grupa and t.msisdn = d.msisdn
                   and t.datumv = d.datumv and t.pozvani_broj = d.pozvani_broj
                   and t.call_id = d.call_id and t.trajanje = d.trajanje;

         delete from tbtraffic
               where record_id in(select record_id
                                  from   tft_dupli_small);

         update mitbprocesslog
            set status_report =
                   status_report || chr(13) || chr(10) || sysdate
                   || 'Okon�ana obrada duplih iz presjeka TBTRAFFIC-a i TBRECORD-a'
          where id = p_id;

         --dupli koji upadaju u jednom krugu
         insert into tft_dupli_small
                     (record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus)
            select t.record_id, t.naziv_datoteke, t.call_id, t.mrezna_grupa,
                   t.msisdn, t.sifra_servisa, t.sifra_sservisa,
                   t.pozvani_broj, t.datumv, t.trajanje, t.otrajanje,
                   t.impulsi, t.iznos, t.pcijena, t.bbroj_prefiks, t.zona_id,
                   t.katpoziva_id, t.plan_id, t.tarifa_id, t.pretplatnik_id,
                   t.korisnik_id, t.bgrupa_id, t.paket_id, t.datumv_obrade,
                   t.flex_uus
            from   tbtraffic t,
                   (select   mrezna_grupa, msisdn, call_id, datumv,
                             pozvani_broj, trajanje
                    from     tbtraffic
                    group by mrezna_grupa,
                             msisdn,
                             call_id,
                             datumv,
                             pozvani_broj,
                             trajanje
                    having   count(*) > 1) d
            where  t.mrezna_grupa = d.mrezna_grupa and t.msisdn = d.msisdn
                   and t.datumv = d.datumv and t.pozvani_broj = d.pozvani_broj
                   and t.call_id = d.call_id and t.trajanje = d.trajanje;

         delete from tbtraffic
               where record_id in(select b.record_id
                                  from   tft_dupli_small a, tft_dupli_small b
                                  where  a.mrezna_grupa = b.mrezna_grupa
                                         and a.msisdn = b.msisdn
                                         and a.call_id = b.call_id
                                         and a.datumv = b.datumv
                                         and a.trajanje = b.trajanje
                                         and a.pozvani_broj = b.pozvani_broj
                                         and a.record_id < b.record_id);

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || 'Okon�ana obrada duplih iz TBTRAFFIC-a samog'
          where id = p_id;

         commit;
         tft_dblog.end_transaction(v_id, sysdate);
      exception
         when others then
            v_ok := 'F';
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Gre?ka kod lociranja duplih slogova bonusnog prometa '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

      begin
         v_id := bg_get_local_seqno('MDBEVENTLOG_SEQ');

         select nvl(min(datumv), sysdate), nvl(max(datumv), sysdate),
                count(*), nvl(sum(impulsi), 0)
         into   v_datmin, v_datmax,
                v_recmin, v_recmax
         from   tft_dupli_small
         where  record_id > v_recmaxold;

         tft_dblog.start_transaction(v_id, v_progid, v_dstart, v_datmin,
            v_datmax, v_recmin, v_recmax);

         update mdbeventlog
            set processlog_id = p_id
          where id = v_id;
      exception
         when others then
            v_ok := 'F';

            if v_id = 0 then
               v_id := v_idalt;
            end if;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
	  Pronadjeni dupli se brisu iz TBTRAFFIC, i ostaju u TFT_DUPLI_SMALL
	  do kraja mjeseca, kada se prebacuju u TFT_DUPLI i sluze za pidjamu
	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
      begin
         -- probam se provuci ovako lako, TSBTRAFFIC ima dva index-a
         -- ima jos varijanti: UNIQUE CONSTRAINT na TSBTRAFFIC, pa LoadSusp4Bonus sam rijesi problem
         delete from tsbtraffic
               where record_id in(select b.record_id
                                  from   tsbtraffic a, tsbtraffic b
                                  where  a.mrezna_grupa = b.mrezna_grupa
                                         and a.msisdn = b.msisdn
                                         and a.call_id = b.call_id
                                         and a.datumv = b.datumv
                                         and a.trajanje = b.trajanje
                                         and a.pozvani_broj = b.pozvani_broj
                                         and a.record_id < b.record_id);

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': SmallDouble-End'
          where id = p_id;

         tft_dblog.end_transaction(v_id, sysdate);
         commit;
      exception
         when others then
            v_ok := 'F';
            rollback;

            update mitbprocesslog
               set status_report = status_report || chr(13) || chr(10)
                                   || sysdate
                                   || ': Gre�ka kod brisanja duplih slogova '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;
   end double_small;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   procedure ccategory_handling(p_degree in number, p_id in number) is
      cursor csunday is
         select   record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                  sifra_servisa, sifra_sservisa, pozvani_broj, datumv,
                  trajanje, otrajanje, impulsi, iznos, pcijena,
                  bbroj_prefiks, zona_id, katpoziva_id, plan_id, tarifa_id,
                  pretplatnik_id, korisnik_id, bgrupa_id, paket_id,
                  datumv_obrade, flex_uus
         from     ttraffic
         where    get_correction_needed(paket_id, katpoziva_id, tarifa_id,
                     'CCAT') = 1
                  and iscorrectpart(nvl(sifra_sservisa, 'XXX'), datumv,
                        trajanje, sysdate) = 1
         order by korisnik_id, datumv;

      v_sqlerr varchar2(4000);
      v_rec traffrec;
      v_point_start date;
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
      v_recmaxold ttraffic.record_id%type;
      v_id number := 0;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_progid number := get_programid('CCategoryHandle');
   begin
      begin
         if p_degree != 0 then
            execute immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            execute immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         else
            execute immediate 'ALTER SESSION DISABLE PARALLEL QUERY';

            execute immediate 'ALTER SESSION DISABLE PARALLEL DML';
         end if;

         rollback;

         execute immediate 'set transaction use rollback segment RBIG';

         execute immediate 'alter session set nls_territory = ''CROATIA''';

         v_id := bg_get_local_seqno('MDBEVENTLOG_SEQ');

         select min(datumv), max(datumv), count(*), sum(impulsi)
         into   v_datmin, v_datmax, v_recmin, v_recmax
         from   ttraffic
         where  get_correction_needed(paket_id, katpoziva_id, tarifa_id,
                   'CCAT') = 1;

         tft_dblog.start_transaction(v_id, v_progid, sysdate, v_datmin,
            v_datmax, v_recmin, v_recmax);

         update mdbeventlog
            set processlog_id = p_id
          where id = v_id;

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': Call Category Hanlding -Start'
          where id = p_id;

         commit;
      exception
         when others then
            v_ok := 'F';

            if v_id = 0 then
               v_id := v_idalt;
            end if;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;

--slu?ajevi veze koja traje du?e od sedam dana nisu tretirani!!!
      begin                                        -- glavni begin za handling
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
Ova petlja obavlja sav posao.
Uzmemo CDR-ove, stavimo u petlju i pozivamo tft_record_cut koja cuta
recorde po timebandovima
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
         for c1_rec in csunday loop
            v_rec := c1_rec;
            tft_record_cut(v_rec, v_rec.datumv, v_rec.trajanje);
         end loop;

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': Call Category Handling -End'
          where id = p_id;

         tft_dblog.end_transaction(v_id, sysdate);
      exception
         when others then
            v_ok := 'F';
            rollback;

            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Gre?ka kod obrade popusta nedeljom za HaloPlus paket '
             where id = p_id;

            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
            null;
      end;                                         -- kraj begin od handling-a
   end ccategory_handling;
   

   /**
    * Ponovno obra�unavanje bonusa kod pojave zaka�njelih slogova
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
 	*/
   procedure prebonus_handling(p_degree in number,
      	  					   p_id    in number,
      						   p_ciklus in varchar2) 
   is
   	 cursor crebonus(c_ociklus varchar2) is
  	 select distinct 
  		 	korisnik_id, 
  		 	ociklus 
     from tbonus b
  	 where ociklus = c_ociklus
       and exists ( select 1 from tbtraffic a where a.korisnik_id = b.korisnik_id
	   	   		       and a.datumv < b.change_time
	   				   and a.datumv between to_date(c_ociklus, 'MONRRRR','NLS_DATE_LANGUAGE=American')
                       	   			and trunc(last_day(
					 	  	  		 to_date(c_ociklus,'MONRRRR','NLS_DATE_LANGUAGE=American')))
                           	    	  + 1 - 1 / 86400);

      --zadnja linija dodana da prebonusing ne pokrene proces u slucaju zakasnjelog prometa
      cursor cmonth is
         select to_char(sysdate, 'MONRRRR',
                            'NLS_DATE_LANGUAGE=American') ociklus from dual
		 union
		 select to_char(add_months(sysdate, -1), 'MONRRRR',
                            'NLS_DATE_LANGUAGE=American') ociklus from dual
							where sysdate between trunc(sysdate, 'MONTH')
                     and trunc(sysdate, 'MONTH') + get_udroffset;

      v_id number;
      v_progid number := get_programid('PreBonusHandle'); /* Iz MPROGRAM tablice */
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
   begin
      v_id := bg_get_local_seqno('MDBEVENTLOG_SEQ');

      select min(datumv), max(datumv), count(*), sum(impulsi)
      into   v_datmin, v_datmax, v_recmin, v_recmax
      from   tbtraffic tb
      where  datumv < (select max(change_time)
                       from   tbonus tbon
                       where  tbon.korisnik_id = tb.korisnik_id
                              and tbon.ociklus = p_ciklus);

      update mitbprocesslog
         set status_report = status_report || chr(13) || chr(10) || sysdate
                             || ': PreBonus Handling -Start'
      where id = p_id;

      tft_dblog.start_transaction(v_id, v_progid, sysdate, v_datmin, v_datmax,
         v_recmin, v_recmax);

      update mdbeventlog
         set processlog_id = p_id
      where id = v_id;

      commit;

	  for c1_month in cmonth loop
	  	  --vra�a sve slogove iz TBRECORD u TBTRAFFIC
      	  for c1_recbonus in crebonus(c1_month.ociklus) loop
                        --prebonus opcija radi samo na nivou trenutnog mjeseca
                        --svi ostali slogovi nece biti prebonusirani nego samo dodavani
						--proces vracanja slogova, bonusiracemo ih kasnije
         		  tft_day_handling.return2start(c1_recbonus.korisnik_id, c1_recbonus.ociklus);
      		end loop;
	  end loop;
      tft_dblog.end_transaction(v_id, sysdate);
      commit;
   exception
      when others then
         rollback;
         v_ok := 'F';

         update mitbprocesslog
            set status_report = status_report || chr(13) || chr(10) || sysdate
                                || ': Greska kod prebonus handle-a za MiniHalo '
          where id = p_id;

         tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
         raise;
   end prebonus_handling;

   /**
    * Novonastale slogove iz TBTRAFFIC i TBONUS prebacuje u TBRECORD
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
	* @param p_ciklus - obra�unski ciklus iz TBONUS (MONRRRR)
    */
   procedure bonus_handling(p_degree in number,
      		 				p_id    in number,
      						p_ciklus in varchar2) 
   is
      cursor ctraffic(c_ociklus varchar2) is
         select   record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                  sifra_servisa, sifra_sservisa, pozvani_broj, datumv,
                  trajanje, otrajanje, impulsi, iznos, pcijena,
                  bbroj_prefiks, zona_id, katpoziva_id, plan_id, tarifa_id,
                  pretplatnik_id, korisnik_id, bgrupa_id, paket_id,
                  datumv_obrade, flex_uus
         from tbtraffic t
         where datumv between to_date(c_ociklus, 'MONRRRR','NLS_DATE_LANGUAGE=American')
                          and trunc(last_day
                                       (to_date(c_ociklus, 'MONRRRR','NLS_DATE_LANGUAGE=American')))
                                 + 1 - 1 / 86400
		   and t.PAKET_ID in (50, 52) --DB dodano da izvla�i samo MINI HALO pakete
         order by korisnik_id, datumv;

      cursor cbonus(c_korisnik_id number, c_ociklus varchar2, c_type varchar2) is
         select amount_min, amount_dub
         from   tbonus
         where  korisnik_id = c_korisnik_id and ociklus = c_ociklus
                and bonus_type = c_type;

      cursor cmonth is
         select ociklus
         from (select distinct to_char(datumv, 'MONRRRR','NLS_DATE_LANGUAGE=American') ociklus
               from tbtraffic)
         order by to_date(ociklus, 'MONRRRR', 'NLS_DATE_LANGUAGE=American');

      v_rec traffrec;
      --v_bday_seq number := get_local_seqno('bday_seq');
      v_amount number;
      v_amount_dub number;
      v_amount_ned number;
      v_amount_dub_ned number;
      v_ociklus varchar2(7) := p_ciklus;
      v_am_till number;
      v_am_tillh number;
      v_am_over number;
      v_rt_over number;
      v_iznos_over number;
      v_id number;
      v_min_record_id number;
      v_max_record_id number;
      v_count number;
      v_sqlerr varchar2(4000);
      v_point_start date;
      v_ostatak number;
      v_progid number := get_programid('BonusCalc');
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
   begin
      execute immediate 'alter session set nls_territory = ''CROATIA''';

      execute immediate 'alter session set nls_date_format = ''dd.mm.rrrr hh24:mi:ss''';

      execute immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual' into v_id;

      select min(datumv), max(datumv), count(*), sum(impulsi)
      into   v_datmin, v_datmax, v_recmin, v_recmax
      from   tbtraffic;

      update mitbprocesslog
         set status_report = status_report || chr(13) || chr(10) || sysdate
                             || ': Obracun bonusa -Start'
       where id = p_id;

      tft_dblog.start_transaction(v_id, v_progid, sysdate, v_datmin, v_datmax,
         v_recmin, v_recmax);

      update mdbeventlog
         set processlog_id = p_id
       where id = v_id;

      commit;

      execute immediate 'set transaction use rollback segment rbig';

      for c1_month in cmonth loop
	  	  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * /
		  Iniciranje baze bonusa.
		  Baza bonusa predstavlja sve pretplatnike koji imaju paket_id neki
		  i njih trpamo ovdje.
		  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

         --treba vidjeti da li cemo inicijalizirati bazu bonusa za sve korisnike za odre�eni ociklus
		 --ili samo one koji imaju tada paket
		 --recimo slucaj kada korisnik u desetom mjesecu dobije paket 50 ili 52 da li inicijalizirati bazu bonusa
		 --za njega za 9 mjesec??? donji kod ce to uraditi
         begin
            insert into tbonus
               (select korisnik_id, c1_month.ociklus, 'P', 0, 0,
                       trunc(to_date(c1_month.ociklus, 'MONRRRR',
                             'NLS_DATE_LANGUAGE = American'),
                          'MONTH'),
                       null
                from   ppretplatnik
                where  paket_id in(50, 52)
				and datum_ukljucenja <= last_day(trunc(to_date(c1_month.ociklus, 'MONRRRR',
                             'NLS_DATE_LANGUAGE = American'),
                          'MONTH'))+1-1/86400)
               minus
               (select korisnik_id, ociklus, bonus_type, 0, 0,
                       trunc(to_date(ociklus, 'MONRRRR',
                             'NLS_DATE_LANGUAGE = American'),
                          'MONTH'),
                       null
                from   tbonus);                 --samo za newly wed(???) kastomere (customers)

            insert into tbonus
               (select korisnik_id, c1_month.ociklus, 'N', 0, 0,
                       trunc(to_date(c1_month.ociklus, 'MONRRRR',
                             'NLS_DATE_LANGUAGE = American'),
                          'MONTH'),
                       null
                from   ppretplatnik
                where  paket_id in(50, 52)
				and datum_ukljucenja <= last_day(trunc(to_date(c1_month.ociklus, 'MONRRRR',
                             'NLS_DATE_LANGUAGE = American'),
                          'MONTH'))+1-1/86400)
               minus
               (select korisnik_id, ociklus, bonus_type, 0, 0,
                       trunc(to_date(ociklus, 'MONRRRR',
                             'NLS_DATE_LANGUAGE = American'),
                          'MONTH'),
                       null
                from   tbonus);

            commit;
         exception
            when others then
               rollback;
               v_ok := 'F';

               update mitbprocesslog
                  set status_report =
                         status_report || chr(13) || chr(10) || sysdate
                         || ': Greska kod iniciranja baze popusta za MiniHalo paket '
                where id = p_id;

               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;

		 --cuvanje kopija TTRAFFICa ako se trazi naravno, dodaju se ociklus fix za sada 
		 --i v_bday_seq identifikator billing day-a
         execute immediate 'set transaction use rollback segment rbig';

		 --ovdje fino stavim loop koji vrti za sve mjesece i sve v_ocikluse 
		 --dole zamjenim sa kursorskom varijablom
		 --ovdje pocinje stvarni obracun bonusa, i ovo mora biti dobro dokumentovano
         for c1_rec in ctraffic(c1_month.ociklus) loop
            v_rec := c1_rec;

			if v_rec.trajanje = 0 then 
			--pozivi sa trajanjem nula samo se prepisuju uz kategoriju poziva 10604
					 insert into tbrecord
                       values (v_rec.record_id, v_rec.naziv_datoteke,
                               v_rec.call_id, v_rec.mrezna_grupa,
                               v_rec.msisdn, v_rec.sifra_servisa,
                               v_rec.sifra_sservisa, v_rec.pozvani_broj,
                               v_rec.datumv, v_rec.trajanje, v_rec.otrajanje,
                               v_rec.impulsi, v_rec.iznos, v_rec.pcijena,
                               v_rec.bbroj_prefiks, v_rec.zona_id,
                               v_rec.katpoziva_id, v_rec.plan_id,
                               v_rec.tarifa_id, v_rec.pretplatnik_id,
                               v_rec.korisnik_id, v_rec.bgrupa_id,
                               v_rec.paket_id, v_rec.datumv_obrade,
                               v_rec.flex_uus, c1_month.ociklus, null, 3);

			else
			--spa�avam orginalni slog

            --da te pitam koliki ti je prvih 60 minuta bonus
            open cbonus(c1_rec.korisnik_id, c1_month.ociklus, 'P');

            fetch cbonus
            into  v_amount, v_amount_dub;

            close cbonus;

            /*1 Poziv nije u nedjelju ****************************************/
            if issunday(c1_rec.datumv, c1_rec.trajanje) = 'N' then
               if v_amount < 3600 then

                  --procedura koja bonusira prvih 60 minuta. Valjda radi kako valja
                  first60bonus(c1_rec.record_id, v_rec, v_amount,
                     c1_month.ociklus, 1);

                  --1 znaci da se predaje cijeli orginalan slog
                  update tbonus --@todo �ini mi se da je ovaj update redundantan
                     set change_time = c1_rec.datumv
                   where korisnik_id = c1_rec.korisnik_id
                         and ociklus = c1_month.ociklus and bonus_type = 'P';
               else
                  insert into tbrecord
                       values (v_rec.record_id, v_rec.naziv_datoteke,
                               v_rec.call_id, v_rec.mrezna_grupa,
                               v_rec.msisdn, v_rec.sifra_servisa,
                               v_rec.sifra_sservisa, v_rec.pozvani_broj,
                               v_rec.datumv, v_rec.trajanje, v_rec.otrajanje,
                               v_rec.impulsi, v_rec.iznos, v_rec.pcijena,
                               v_rec.bbroj_prefiks, v_rec.zona_id,
                               v_rec.katpoziva_id, v_rec.plan_id,
                               v_rec.tarifa_id, v_rec.pretplatnik_id,
                               v_rec.korisnik_id, v_rec.bgrupa_id,
                               v_rec.paket_id, v_rec.datumv_obrade,
                               v_rec.flex_uus, c1_month.ociklus, null, 3);
                  null;
               end if;
			   
			/*2 Poziv je u nedelju ****************************************/
            else
          	   --koliko je nedeljnog bonusa do sada potroseno
               open cbonus(c1_rec.korisnik_id, c1_month.ociklus, 'N');

               fetch cbonus
               into  v_amount_ned, v_amount_dub_ned;

               close cbonus;

               /*1 Nije presao REGULARNI bonus ****************************************/
               if v_amount < 3600 then  --nije presao regularni bonus
                  --Odgovor je NE

                  /*1.1 Nije presao REGULARNI bonus i Nije presao NEDJELJNI bonus ***********/
                  if v_amount_ned < 3600 then
				  	 /*1.1.1 Y Before i NIJE presao REGULARNI bonus i NIJE presao NEDJELJNI bonus i *****/
                     if issunday(c1_rec.datumv, c1_rec.trajanje) = 'YB' then
                        --ako je poziv u nedjelju poceo prije 18 zavrsio izmedju 18 i 19
                        --zna�i poziv se inicijalno dijeli u dva dijela, prvi je prije 18:00 i poziva se First60Bonus
                        --bekap orginalnog sloga, koji ce biti iscjepan
                        insert into tbrecord
                             values (v_rec.record_id, v_rec.naziv_datoteke,
                                     v_rec.call_id, v_rec.mrezna_grupa,
                                     v_rec.msisdn, v_rec.sifra_servisa,
                                     v_rec.sifra_sservisa,
                                     v_rec.pozvani_broj, v_rec.datumv,
                                     v_rec.trajanje, v_rec.otrajanje,
                                     v_rec.impulsi, v_rec.iznos,
                                     v_rec.pcijena, v_rec.bbroj_prefiks,
                                     v_rec.zona_id, v_rec.katpoziva_id,
                                     v_rec.plan_id, v_rec.tarifa_id,
                                     v_rec.pretplatnik_id, v_rec.korisnik_id,
                                     v_rec.bgrupa_id, v_rec.paket_id,
                                     v_rec.datumv_obrade, v_rec.flex_uus,
                                     c1_month.ociklus, null, 1);

                        v_point_start := trunc(c1_rec.datumv) + 64800 / 86400;
                        --ovo je 18:00
                        v_rt_over := (v_point_start - c1_rec.datumv) * 86400;
                        --remaining time do 18:00 ovo nam treba radi kasnijeg izracuna
                        v_am_over := (ceil(v_rt_over / 60)) * 60;
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                        --promjeni otrajanje sloga jer se radi o novom CDR-u
                        v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                        first60bonus(c1_rec.record_id, v_rec, v_amount,
                           c1_month.ociklus, 0); 
						--0 zn�i da je predan komad samo
                        --u ovom trenutku, slog je obrisan, v_amount je popravljen da reflektuje stanje novog bonusa
                        --u ttrafficu imamo jedan ili dva sloga, ovisno o bonusu. dakle dio prije 18:00 je obradjen

                        --na osnovnu proteklog stanja pravimo slog koji oznacava dio nedelje
                        --to se radi tako sto se od kompletnog trajanja oduzmu ranija trajanja
                        v_rt_over := get_sekunde(c1_rec.trajanje, 'US')
                                     - v_rt_over;
                        --remaining time od 18:00 do kraja poziva
                        v_am_over := get_sekunde(c1_rec.otrajanje, 'US')
                                     - v_am_over;

                        --obracunske jedinice vremena od 18:00 do kraja poziva
                        --pamtim da bih dobro ub'o minutu
                        if v_amount_ned + v_am_over <= 3600 then
                           --ostatak nedjelje nije presao granicu nedeljnog bonusa
                           --samo popravljam stari slog tako da mu cijena bude nula
                           insertstavke(c1_rec.record_id, v_rec,
                              bg_get_local_seqno('TTRAFFIC_seq'),
                              v_point_start, get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, 0, 0, 16201,
                              0, 0
                                  --pocjepani original na prije nedelje i tokom nedelje
                           );

                           update tbonus
                              set amount_min = amount_min + v_am_over,
                                  change_time = c1_rec.datumv
                            where korisnik_id = v_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';
                        else
                           --jeste, pravim dva sloga, jedan je dio poziva koji ulazi u nedeljni bonus
                           --a drugi podlijeze pravilima racunanja regularnog bonusa
                           --ovo je trajanje besplatnog sloga, dok ne istekne bonus
                           v_rt_over :=(3600 - v_amount_ned);
                           v_am_over :=(3600 - v_amount_ned);            --ok
                           v_rec.otrajanje :=
                                get_sekunde(v_rec.otrajanje, 'US')
                                + v_am_over;
                           --ovo samo pamtim da bi dobro oduzeo dole
                           insertstavke(c1_rec.record_id, v_rec,
                              bg_get_local_seqno('TTRAFFIC_seq'),
                              v_point_start, get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, 0, 0, 16201,
                              0, 0                         --pocjepano dijete
                                  );

                           --postavljanje nedeljnog bonusa na 3600
                           update tbonus
                              set amount_min = 3600,
                                  change_time = c1_rec.datumv
                            where korisnik_id = v_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           v_point_start := v_point_start + v_am_over / 86400;
                           --vrijeme kada je bonus istekao
                           v_rt_over := ((c1_rec.datumv + get_sekunde(c1_rec.trajanje, 'US') / 86400)
                                         - v_point_start) * 86400;
                             --od trenutka isteka bonus do kraja zavrsetka poziva
                           /*1*/
                           v_am_over := get_sekunde(c1_rec.otrajanje, 'US')
                                        - v_rec.otrajanje; --to je ovoliko obracunskih jedinica
                           --postavljamo varijable naseg imaginarnog CDR-a
                           v_rec.record_id := bg_get_local_seqno('TTRAFFIC_seq');--posto nije prvi poziv, ovo mu je sekvenca
                           v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                           v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                           v_rec.impulsi := 0; --ovo je zato sto obradjujemo zadnji dio poziva a ne prvi dio poziva
                           v_rec.flex_uus := 0;
                           v_rec.datumv := v_point_start;
                           first60bonus(c1_rec.record_id, v_rec, v_amount, c1_month.ociklus, 0);

                           --samo update na datumv od originalnog sloga
                           update tbonus
                              set change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'P';
                        end if;
					 
					 /*1.1.2  Y During i NIJE presao REGULARNI bonus i NIJE presao NEDJELJNI bonus i *****/
                     elsif issunday(c1_rec.datumv, c1_rec.trajanje) = 'YD' then
                        v_am_over := get_sekunde(v_rec.otrajanje, 'US');

                        if v_amount_ned + get_sekunde(v_rec.otrajanje, 'US') <= 3600 then
                           --kompletan poziv nedelje je besplatan
                           --ostatak nedjelje nije presao granicu nedeljnog bonusa
                           --samo popravljam stari slog tako da mu cijena bude nula
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv, v_rec.trajanje,
                              v_rec.otrajanje, v_rec.impulsi, 0, 0, 16201,
                              v_rec.flex_uus, 2);

                           --postavljanje nedeljnog bonusa na koliko je potro�enno
                           update tbonus
                              set amount_min = amount_min + v_am_over,
                                  change_time = c1_rec.datumv
                            where korisnik_id = v_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';
                        else
                            --samo dio nedjelje je besplatan, ostatak podlijeze pravilima obicnog bonusa
                              --jeste, pravim dva sloga, jedan je dio poziva koji ulazi u nedeljni bonus
                              --a drugi podlijeze pravilima racunanja regularnog bonusa
                              --ovo je trajanje besplatnog sloga, dok ne istekne bonus
                           --bekap orginalnog sloga posto ce sada da bude iscijepan
                           insert into tbrecord
                                values (v_rec.record_id,
                                        v_rec.naziv_datoteke, v_rec.call_id,
                                        v_rec.mrezna_grupa, v_rec.msisdn,
                                        v_rec.sifra_servisa,
                                        v_rec.sifra_sservisa,
                                        v_rec.pozvani_broj, v_rec.datumv,
                                        v_rec.trajanje, v_rec.otrajanje,
                                        v_rec.impulsi, v_rec.iznos,
                                        v_rec.pcijena, v_rec.bbroj_prefiks,
                                        v_rec.zona_id, v_rec.katpoziva_id,
                                        v_rec.plan_id, v_rec.tarifa_id,
                                        v_rec.pretplatnik_id,
                                        v_rec.korisnik_id, v_rec.bgrupa_id,
                                        v_rec.paket_id, v_rec.datumv_obrade,
                                        v_rec.flex_uus, c1_month.ociklus,
                                        null, 1);

                           v_rt_over :=(3600 - v_amount_ned);
                           v_am_over :=(3600 - v_amount_ned);
                           --ovo je prvi slog koji kupi glavne osobine orginalnog sloga
                           --razlika sa slucajem YB je u tome sto je tamo nedjeljni dio ustvari drugi dio
                           --te ne nasljedjuje osobine orginalnog sloga
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi, 0,
                              0, 16201, 0, 0);

                           --postavljanje nedeljnog bonusa na 3600
                           update tbonus
                              set amount_min = 3600,
                                  change_time = c1_rec.datumv
                            where korisnik_id = v_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           v_point_start := v_rec.datumv + v_am_over / 86400;
                           --vrijeme kada je bonus istekao
                           v_rt_over := ((v_rec.datumv + get_sekunde(v_rec.trajanje, 'US') / 86400)
                                         - v_point_start) * 86400;
                           --od trenutka isteka bonus do kraja zavrsetka poziva
                           v_am_over := get_sekunde(c1_rec.otrajanje, 'US')- v_am_over; --to je ovoliko obracunskih jedinica
                           --postavljamo varijable naseg imaginarnog CDR-a
                           v_rec.record_id := bg_get_local_seqno('TTRAFFIC_seq');
                           --posto nije prvi poziv, ovo mu je sekvenca
                           v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                           v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                           v_rec.impulsi := 0;
                           --ovo je zato sto obradjujemo zadnji dio poziva a ne prvi dio poziva
                           v_rec.flex_uus := 0;
                           v_rec.datumv := v_point_start;
                           first60bonus(c1_rec.record_id, v_rec, v_amount,
                              c1_month.ociklus, 0);

                           update tbonus
                              set change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'P';
                        end if;

					 /*1.1.3 Y After i NIJE presao REGULARNI bonus i NIJE presao NEDJELJNI bonus i *****/
                     elsif issunday(c1_rec.datumv, c1_rec.trajanje) = 'YA' then
                        --ako je poziv u nedjelju poceo izmedju 18 i 19 zavrsio poslije 19

                        --bekap orginalnog sloga posto ce sada da bude iscijepan
                        insert into tbrecord
                             values (v_rec.record_id, v_rec.naziv_datoteke,
                                     v_rec.call_id, v_rec.mrezna_grupa,
                                     v_rec.msisdn, v_rec.sifra_servisa,
                                     v_rec.sifra_sservisa,
                                     v_rec.pozvani_broj, v_rec.datumv,
                                     v_rec.trajanje, v_rec.otrajanje,
                                     v_rec.impulsi, v_rec.iznos,
                                     v_rec.pcijena, v_rec.bbroj_prefiks,
                                     v_rec.zona_id, v_rec.katpoziva_id,
                                     v_rec.plan_id, v_rec.tarifa_id,
                                     v_rec.pretplatnik_id, v_rec.korisnik_id,
                                     v_rec.bgrupa_id, v_rec.paket_id,
                                     v_rec.datumv_obrade, v_rec.flex_uus,
                                     c1_month.ociklus, null, 1);

                        --treba ga prvo obrisati
                        /*                    delete from ttraffic
                                                  where record_id = v_rec.record_id;*/

                        --uzmi komad nedjelje do isteka bonusa ili do 19:00 pa sta prije dodje
                        --jer od toga zavisi kako ce se tretirati ostatak
                        v_ostatak := (
                                      (trunc(c1_rec.datumv) + 68400 / 86400)
                                      - c1_rec.datumv
                                     )
                                     * 86400;
                        --ovo je 68400 a to je 19:00 i pitamo se koliko je to nedelje ostalo
                        v_rt_over := v_ostatak;
                        v_am_over := ceil(v_rt_over / 60) * 60;

                        if v_amount_ned + v_am_over <= 3600 then
                                                         -- onda kompletan dio po
                            --ostatak nedjelje nije presao granicu nedeljnog bonusa tj. do 19:00 se bonus nije napunio
                           --ponovo insertujem taj isti slog samo za dio nedjelje
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi, 0,
                              0, 16201, v_rec.flex_uus, 0);

                           update tbonus
                              set amount_min = amount_min + v_am_over,
                                  change_time = c1_rec.datumv
                            where korisnik_id = v_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           --ostatak poziva obradimo tako da primjenimo First60 Bonus
                           --napravimo CDR i prepustimo ga proceduri First60Bonus
                           v_point_start := trunc(v_rec.datumv)
                                            + 68400 / 86400;
                           --on pocinje u 19:00
                           v_rt_over := (
                                         (
                                          v_rec.datumv
                                          + get_sekunde(v_rec.trajanje, 'US')
                                            / 86400
                                         )
                                         - v_point_start
                                        )
                                        * 86400;              --traje do kraja
                           v_am_over := get_sekunde(v_rec.otrajanje, 'US')
                                        - v_am_over;
                           v_rec.record_id :=
                                            bg_get_local_seqno('TTRAFFIC_SEQ');
                           v_rec.impulsi := 0;
                           v_rec.flex_uus := 0;
                           v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                           v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                           v_rec.datumv := v_point_start;
                           first60bonus(c1_rec.record_id, v_rec, v_amount,
                              c1_month.ociklus, 0);

                           update tbonus
                              set change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'P';
                        else
                           --ako nije onda sjecem na kraju HH Bonusa i taj komad obra?unavam besplatno
                           --a ostali dio poziva mi podlijeze apliciranju First60 Bonusa
                           v_am_over :=(3600 - v_amount_ned);
                           v_rt_over :=(3600 - v_amount_ned);
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv,

                              --pocinje kada i original
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi, 0,
                              0, 16201, v_rec.flex_uus, 0);

                           update tbonus
                              set amount_min = 3600,
                                  change_time = c1_rec.datumv
                            where korisnik_id = v_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           --ostatak iza HH podlije?e pravilima First60Bonus-a

                           --obradimo prvo ostatak poziva koji upada u HH
                           --napravimo CDR i prepustimo ga proceduri First60Bonus
                           v_point_start := v_rec.datumv + v_rt_over / 86400;
                           v_rt_over := (
                                         (
                                          v_rec.datumv
                                          + get_sekunde(v_rec.trajanje, 'US')
                                            / 86400
                                         )
                                         - v_point_start
                                        )
                                        * 86400;
                           v_am_over := get_sekunde(v_rec.otrajanje, 'US')
                                        - v_am_over;
                           v_rec.record_id :=
                                            bg_get_local_seqno('TTRAFFIC_SEQ');
                           v_rec.impulsi := 0;
                           v_rec.flex_uus := 0;
                           v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                           v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                           v_rec.datumv := v_point_start;
                           first60bonus(c1_rec.record_id, v_rec, v_amount,
                              c1_month.ociklus, 0);

                           update tbonus
                              set change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'P';
                        end if;
					 /*1.1.4 Y Over i NIJE presao REGULARNI bonus i NIJE presao NEDJELJNI bonus i   *****/
                     elsif issunday(c1_rec.datumv, c1_rec.trajanje) = 'YO' then
                                            --ovaj poziv pocinje prije 18:00 a zavrsava poslije 19:00
                                            --uzmemo komad poziva koji pocinje prije 18:00 i ostavimo ga sa strane
                        /*                    delete from ttraffic
                                                  where record_id = v_rec.record_id;*/

                        --bekap orginalnog sloga posto ce sada da bude iscijepan
                        insert into tbrecord
                             values (v_rec.record_id, v_rec.naziv_datoteke,
                                     v_rec.call_id, v_rec.mrezna_grupa,
                                     v_rec.msisdn, v_rec.sifra_servisa,
                                     v_rec.sifra_sservisa,
                                     v_rec.pozvani_broj, v_rec.datumv,
                                     v_rec.trajanje, v_rec.otrajanje,
                                     v_rec.impulsi, v_rec.iznos,
                                     v_rec.pcijena, v_rec.bbroj_prefiks,
                                     v_rec.zona_id, v_rec.katpoziva_id,
                                     v_rec.plan_id, v_rec.tarifa_id,
                                     v_rec.pretplatnik_id, v_rec.korisnik_id,
                                     v_rec.bgrupa_id, v_rec.paket_id,
                                     v_rec.datumv_obrade, v_rec.flex_uus,
                                     c1_month.ociklus, null, 1);

                        v_point_start := trunc(c1_rec.datumv) + 64800 / 86400;
                        --ovo je 18:00
                        v_rt_over := (v_point_start - c1_rec.datumv) * 86400;
                        --remaining time do 18:00
                        v_am_over := (ceil(v_rt_over / 60)) * 60;
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                        v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                        --taj rekord koji ima dio prije 18:00 je podlozan first60minutes bonusu
                        first60bonus(c1_rec.record_id, v_rec, v_amount,
                           c1_month.ociklus, 0);
                        v_ostatak := 3600;
                        --preso je cijeli happy hour jer poziv traje duze od 60 minuta
                              --ovo je 68400 a to je 19:00 i pitamo se koliko je to nedelje ostalo
                              --v_sun_start ostaje isti u 18:00
                        v_rt_over := 3600 - v_amount_ned;
                        --remaining time do 18:00
                        v_am_over := 3600 - v_amount_ned;
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_rec.record_id := bg_get_local_seqno('TTRAFFIC_SEQ');
                        insertstavke(c1_rec.record_id, v_rec,
                           v_rec.record_id, v_point_start,
                           get_sekunde(v_rt_over, 'UH'),
                           get_sekunde(v_am_over, 'UH'), 0, 0, 0, 16201, 0,
                           0);

                        update tbonus
                           set amount_min = 3600,
                               change_time = c1_rec.datumv
                         where korisnik_id = v_rec.korisnik_id
                               and ociklus = c1_month.ociklus
                               and bonus_type = 'N';

                        --eh a sada je ostatak poziva podlozan First60Bonus-u
                        v_point_start := v_point_start + v_rt_over / 86400;
                        v_rt_over := (
                                      (
                                       v_rec.datumv
                                       + get_sekunde(c1_rec.trajanje, 'US')
                                         / 86400
                                      )
                                      - v_point_start
                                     )
                                     * 86400;        --remaining time do 18:00
                        v_am_over := get_sekunde(c1_rec.otrajanje, 'US')
                                     -(
                                       get_sekunde(v_rec.otrajanje, 'US')
                                       + v_am_over
                                      );
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_rec.record_id := bg_get_local_seqno('TTRAFFIC_SEQ');
                        v_rec.impulsi := 0;
                        v_rec.flex_uus := 0;
                        v_rec.trajanje := get_sekunde(v_rt_over, 'UH');
                        v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                        v_rec.datumv := v_point_start;
                        first60bonus(c1_rec.record_id, v_rec, v_amount,
                           c1_month.ociklus, 0);

                        update tbonus
                           set change_time = c1_rec.datumv
                         where korisnik_id = c1_rec.korisnik_id
                               and ociklus = c1_month.ociklus
                               and bonus_type = 'P';
                     end if;
				  /*1.2  NIJE presao REGULARNI bonus i JESTE presao NEDJELJNI bonus     *****/
                  else
                  -- aplicira se isto kao i za sve ostalo na osnovu First60Minutes
/*                  delete from ttraffic
                        where record_id = v_rec.record_id;*/
                     first60bonus(c1_rec.record_id, v_rec, v_amount,
                        c1_month.ociklus, 1);
                  end if;                     --nedjelja od v_amount_ned <3600
			   /*2 JESTE presao regularni bonus     *****/
               else
			      /*2.1 JESTE presao regularni bonus i NIJE presao nedjeljni bonus    *****/
                  if v_amount_ned < 3600 then
				     /*2.1.1  Y Before i JESTE presao regularni bonus i NIJE presao NEDJELJNI bonus i   *****/
                     if issunday(c1_rec.datumv, c1_rec.trajanje) = 'YB' then
                     --ako je poziv u nedjelju poceo prije 18 zavrsio izmedju 18 i 19
                     --obrisi stari slog
/*                     delete from ttraffic
                           where record_id = c1_rec.record_id;*/

                        --bekap orginalnog sloga posto ce sada da bude iscijepan
                        insert into tbrecord
                             values (v_rec.record_id, v_rec.naziv_datoteke,
                                     v_rec.call_id, v_rec.mrezna_grupa,
                                     v_rec.msisdn, v_rec.sifra_servisa,
                                     v_rec.sifra_sservisa,
                                     v_rec.pozvani_broj, v_rec.datumv,
                                     v_rec.trajanje, v_rec.otrajanje,
                                     v_rec.impulsi, v_rec.iznos,
                                     v_rec.pcijena, v_rec.bbroj_prefiks,
                                     v_rec.zona_id, v_rec.katpoziva_id,
                                     v_rec.plan_id, v_rec.tarifa_id,
                                     v_rec.pretplatnik_id, v_rec.korisnik_id,
                                     v_rec.bgrupa_id, v_rec.paket_id,
                                     v_rec.datumv_obrade, v_rec.flex_uus,
                                     c1_month.ociklus, null, 1);

                        v_point_start := trunc(c1_rec.datumv) + 64800 / 86400;
                        --ovo je 18:00
                        v_rt_over := (v_point_start - c1_rec.datumv) * 86400;
                        --remaining time do 18:00
                        v_am_over := (ceil(v_rt_over / 60)) * 60;
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_iznos_over := (v_am_over / 60) * 0.56;
                        --cijena je umnozak obracunskih jedinica i 0.56
                        v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                        --napravi novi slog koji ce se regularno naplatiti
                        --jer je regularni bonus istrosen. sa starim record_idem
                        insertstavke(c1_rec.record_id, v_rec,
                           v_rec.record_id, v_rec.datumv,
                           get_sekunde(v_rt_over, 'UH'),
                           get_sekunde(v_am_over, 'UH'), v_rec.impulsi,
                           v_iznos_over, v_iznos_over, 10604, v_rec.flex_uus,
                           0);
                        --sada odradi nedjelju
                        v_rt_over := get_sekunde(c1_rec.trajanje, 'US')
                                     - v_rt_over;
                        --remaining time od 18:00 do kraja poziva
                        v_am_over := get_sekunde(c1_rec.otrajanje, 'US')
                                     - v_am_over;

                           --obracunske jedinice vremena od 18:00 do kraja poziva
                        --ovo pamtim da bi dole mogao oduzeti
                        if v_amount_ned + v_am_over <= 3600 then
                           --ostatak nedjelje nije presao granicu nedeljnog bonusa
                           --samo popravljam stari slog tako da mu cijena bude nula
                           insertstavke(c1_rec.record_id, v_rec,
                              bg_get_local_seqno('TTRAFFIC_seq'),
                              v_point_start, get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, 0, 0, 16201,
                              0, 0);

                           --kompletna nedjelja je pokrivena HH bonusom
                           update tbonus
                              set amount_min = amount_min + v_am_over,
                                  change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';
                        else
                           --jeste, pravim dva sloga, jedan je dio poziva koji ulazi u nedeljni bonus
                                               --a drugi se regularno napla?uje

                           --drugi poziv koji ima dio koji je pokriven nedjeljnim bonusom
                           v_point_start :=
                                          trunc(c1_rec.datumv)
                                          + 64800 / 86400;
                           --pocinje u 18:00
                           v_rt_over := 3600 - v_amount_ned;
                           --traje do istrosenja bonusa
                           v_am_over := 3600 - v_amount_ned;
                           --toliko je i obracunsko vrijeme
                           insertstavke(c1_rec.record_id, v_rec,
                              bg_get_local_seqno('TTRAFFIC_seq'),
                              v_point_start, get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, 0, 0, 16201,
                              0, 0);

                           --ovaj dio nedjelje pokriven je HH bonusom
                           update tbonus
                              set amount_min = 3600,
                                  change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           --ovo je slog koji se naplacuje
                           v_point_start := v_point_start + v_am_over / 86400;
                           --pocinje kad se bonus istrosi
                           v_rt_over := (
                                         (
                                          v_rec.datumv
                                          + get_sekunde(v_rec.trajanje, 'US')
                                            / 86400
                                         )
                                         - v_point_start
                                        )
                                        * 86400;
                           v_am_over := get_sekunde(c1_rec.otrajanje, 'US')
                                        -(
                                          get_sekunde(v_rec.otrajanje, 'US')
                                          + v_am_over
                                         );
                           v_iznos_over := (v_am_over / 60) * 0.56;
                           insertstavke(c1_rec.record_id, v_rec,
                              bg_get_local_seqno('TTRAFFIC_seq'),
                              v_point_start, get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, v_iznos_over,
                              v_iznos_over, 10604, 0, 0);
                        --slog na kraju koji se regularno napla?uje
                        end if;
                        --od da li je zbir poziva i bonusa nedelje presao 3600
					 /*2.1.2 Y During i JESTE presao regularni bonus i NIJE presao NEDJELJNI bonus i   *****/
                     elsif issunday(c1_rec.datumv, c1_rec.trajanje) = 'YD' then
                        --ako poziv pocinje i zavrsava izmedju 18 i 19 sati
                        if v_amount_ned
                           +(get_sekunde(c1_rec.otrajanje, 'US')) <= 3600 then
                            --da li taj poziv prelazi nedeljnu kvotu bonusa
                            --ako ne prelazi ma samo ga pregazi na nulu
                           /* delete from ttraffic
                                  where record_id = v_rec.record_id;*/

                           /*update TTRAFFIC set iznos = 0, pcijena = 0, katpoziva_id = 16201
                           where record_id = c1_rec.record_id;*/
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv, v_rec.trajanje,
                              v_rec.otrajanje, v_rec.impulsi, 0, 0, 16201,
                              v_rec.flex_uus, 2);

                           update tbonus
                              set amount_min =
                                     amount_min
                                     + get_sekunde(c1_rec.otrajanje, 'US'),
                                  change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';
                        else
--jeste, pravim dva sloga, jedan je za nedelju do bonusa, drugi regularno obracunavam
                        --jer se nalazimo u dijelu koda koji obradjuje pozive koji su potrosili regularni bonus

                           --delete from TTRAFFIC where record_id = c1_rec.record_id;
                           --bekap orginalnog sloga posto ce sada da bude iscijepan
                           insert into tbrecord
                                values (v_rec.record_id,
                                        v_rec.naziv_datoteke, v_rec.call_id,
                                        v_rec.mrezna_grupa, v_rec.msisdn,
                                        v_rec.sifra_servisa,
                                        v_rec.sifra_sservisa,
                                        v_rec.pozvani_broj, v_rec.datumv,
                                        v_rec.trajanje, v_rec.otrajanje,
                                        v_rec.impulsi, v_rec.iznos,
                                        v_rec.pcijena, v_rec.bbroj_prefiks,
                                        v_rec.zona_id, v_rec.katpoziva_id,
                                        v_rec.plan_id, v_rec.tarifa_id,
                                        v_rec.pretplatnik_id,
                                        v_rec.korisnik_id, v_rec.bgrupa_id,
                                        v_rec.paket_id, v_rec.datumv_obrade,
                                        v_rec.flex_uus, c1_month.ociklus,
                                        null, 1);

                           --drugi poziv koji ima dio koji je pokriven nedjeljnim bonusom
                           v_point_start := v_rec.datumv;   --pocinje u 18:00
                           v_rt_over := 3600 - v_amount_ned;
                           --traje do istrosenja bonusa
                           v_am_over := 3600 - v_amount_ned;
                           --toliko je i obracunsko vrijeme
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_point_start,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi, 0,
                              0, 16201, v_rec.flex_uus, 0);

                           --ovaj dio nedjelje pokriven je HH bonusom
                           update tbonus
                              set amount_min = 3600,
                                  change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           --ovo je slog koji se naplacuje
                           v_point_start :=
                                   v_point_start
                                   + (3600 - v_amount_ned) / 86400;
                           --pocinje kad se bonus istrosi
                           v_rt_over := (
                                         (
                                          v_rec.datumv
                                          + get_sekunde(v_rec.trajanje, 'US')
                                            / 86400
                                         )
                                         - v_point_start
                                        )
                                        * 86400;
                           v_am_over := ceil(v_rt_over / 60) * 60;
                           v_iznos_over := (v_am_over / 60) * 0.56;
                           insertstavke(c1_rec.record_id, v_rec,
                              bg_get_local_seqno('TTRAFFIC_seq'),
                              v_point_start, get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, v_iznos_over,
                              v_iznos_over, 10604, 0, 0);
                        --slog na kraju koji se regularno napla?uje
                        end if;
					 /*2.1.3 Y After i JESTE presao regularni bonus i NIJE presao NEDJELJNI bonus i   *****/
                     elsif issunday(c1_rec.datumv, c1_rec.trajanje) = 'YA' then
                        --ako je poziv u nedjelju poceo izmedju 18 i 19 zavrsio poslije 19
                        --uzmi komad nedjelje do isteka bonusa ili do 19:00 pa sta prije dodje

                        --uzmi komad nedjelje do isteka bonusa ili do 19:00 pa sta prije dodje
                          --jer od toga zavisi kako ce se tretirati ostatak
                        /*  delete from ttraffic
                                where record_id = v_rec.record_id;*/
                        --bekap orginalnog sloga posto ce sada da bude iscijepan
                        insert into tbrecord
                             values (v_rec.record_id, v_rec.naziv_datoteke,
                                     v_rec.call_id, v_rec.mrezna_grupa,
                                     v_rec.msisdn, v_rec.sifra_servisa,
                                     v_rec.sifra_sservisa,
                                     v_rec.pozvani_broj, v_rec.datumv,
                                     v_rec.trajanje, v_rec.otrajanje,
                                     v_rec.impulsi, v_rec.iznos,
                                     v_rec.pcijena, v_rec.bbroj_prefiks,
                                     v_rec.zona_id, v_rec.katpoziva_id,
                                     v_rec.plan_id, v_rec.tarifa_id,
                                     v_rec.pretplatnik_id, v_rec.korisnik_id,
                                     v_rec.bgrupa_id, v_rec.paket_id,
                                     v_rec.datumv_obrade, v_rec.flex_uus,
                                     c1_month.ociklus, null, 1);

                        v_ostatak := (
                                      (trunc(c1_rec.datumv) + 68400 / 86400)
                                      - c1_rec.datumv
                                     )
                                     * 86400;
                        --ovo je 68400 a to je 19:00 i pitamo se koliko je to nedelje ostalo
                        v_rt_over := v_ostatak;
                        v_am_over := ceil(v_rt_over / 60) * 60;

                        if v_amount_ned + v_am_over <= 3600 then
                                                         -- onda kompletan dio po
                            --ostatak nedjelje nije presao granicu nedeljnog bonusa
                           --ubacujem novi slog koji traje do kraja nedjelje i postavljam cijenu na nula
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi, 0,
                              0, 16201, v_rec.flex_uus, 0);

                           update tbonus
                              set amount_min = amount_min + v_am_over,
                                  change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           v_point_start := trunc(v_rec.datumv)
                                            + 68400 / 86400;
                           --ovo je 19:00
                           v_rt_over := (
                                         (
                                          v_rec.datumv
                                          + get_sekunde(v_rec.trajanje, 'US')
                                            / 86400
                                         )
                                         - v_point_start
                                        )
                                        * 86400;
                           --ovo je trajanje poziva iza 19:00
                           v_am_over := get_sekunde(v_rec.otrajanje, 'US')
                                        - v_am_over;
                           v_rec.record_id :=
                                            bg_get_local_seqno('TTRAFFIC_SEQ');
                           v_rec.impulsi := 0;
                           v_rec.flex_uus := 0;
                           v_iznos_over := (v_am_over / 60) * 0.56;
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_point_start,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi,
                              v_iznos_over, v_iznos_over, 10604, 0, 0);
                        else
                           v_am_over :=(3600 - v_amount_ned);
                           v_rt_over :=(3600 - v_amount_ned);
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_rec.datumv,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), v_rec.impulsi, 0,
                              0, 16201, v_rec.flex_uus, 0);

                           update tbonus
                              set amount_min = 3600,
                                  change_time = c1_rec.datumv
                            where korisnik_id = c1_rec.korisnik_id
                                  and ociklus = c1_month.ociklus
                                  and bonus_type = 'N';

                           v_point_start := v_rec.datumv + v_rt_over / 86400;
                           v_rt_over := (
                                         (
                                          v_rec.datumv
                                          + get_sekunde(v_rec.trajanje, 'US')
                                            / 86400
                                         )
                                         - v_point_start
                                        )
                                        * 86400;
                           v_am_over := get_sekunde(v_rec.otrajanje, 'US')
                                        - v_am_over;
                           v_rec.record_id :=
                                            bg_get_local_seqno('TTRAFFIC_SEQ');
                           v_rec.impulsi := 0;
                           v_rec.flex_uus := 0;
                           v_iznos_over := (v_am_over / 60) * 0.56;
                           insertstavke(c1_rec.record_id, v_rec,
                              v_rec.record_id, v_point_start,
                              get_sekunde(v_rt_over, 'UH'),
                              get_sekunde(v_am_over, 'UH'), 0, v_iznos_over,
                              v_iznos_over, 10604, v_rec.flex_uus, 0);
                        end if;
					 
					 /*2.1.4 Y Over i JESTE presao regularni bonus i NIJE presao NEDJELJNI bonus i *****/
                     elsif issunday(c1_rec.datumv, c1_rec.trajanje) = 'YO' then
                          --ovaj poziv pocinje prije 18:00 a zavrsava poslije 19:00
                          --uzmemo komad poziva koji pocinje prije 18:00 i ostavimo ga sa strane
                        /*  delete from ttraffic
                                where record_id = v_rec.record_id;*/

                        --bekap orginalnog sloga posto ce sada da bude iscijepan
                        insert into tbrecord
                             values (v_rec.record_id, v_rec.naziv_datoteke,
                                     v_rec.call_id, v_rec.mrezna_grupa,
                                     v_rec.msisdn, v_rec.sifra_servisa,
                                     v_rec.sifra_sservisa,
                                     v_rec.pozvani_broj, v_rec.datumv,
                                     v_rec.trajanje, v_rec.otrajanje,
                                     v_rec.impulsi, v_rec.iznos,
                                     v_rec.pcijena, v_rec.bbroj_prefiks,
                                     v_rec.zona_id, v_rec.katpoziva_id,
                                     v_rec.plan_id, v_rec.tarifa_id,
                                     v_rec.pretplatnik_id, v_rec.korisnik_id,
                                     v_rec.bgrupa_id, v_rec.paket_id,
                                     v_rec.datumv_obrade, v_rec.flex_uus,
                                     c1_month.ociklus, null, 1);

                        v_point_start := trunc(c1_rec.datumv) + 64800 / 86400;
                        --ovo je 18:0
                        v_rt_over := (v_point_start - c1_rec.datumv) * 86400;
                        --remaining time do 18:00
                        v_am_over := (ceil(v_rt_over / 60)) * 60;
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_iznos_over := (v_am_over / 60) * 0.56;
                        --cijena je umnozak obracunskih jedinica i 0.56
                        v_rec.otrajanje := get_sekunde(v_am_over, 'UH');
                        --obrisi stari slog
                         --delete from TTRAFFIC where record_id = c1_rec.record_id;

                        --napravi novi slog koji ce se regularno naplatiti
                        --jer je regularni bonus istrosen. sa starim record_idem
                        insertstavke(c1_rec.record_id, v_rec,
                           v_rec.record_id, v_rec.datumv,
                           get_sekunde(v_rt_over, 'UH'),
                           get_sekunde(v_am_over, 'UH'), v_rec.impulsi,
                           v_iznos_over, v_iznos_over, 10604, v_rec.flex_uus,
                           0);
                        v_ostatak := 3600;
                        --preso je cijeli happy hour jer poziv traje duze od 60 minuta
                             --v_sun_start ostaje isti u 18:00
                        v_point_start := trunc(c1_rec.datumv) + 64800 / 86400;
                        --ovo je 18:00
                        v_rt_over := 3600 - v_amount_ned;
                        --remaining time do 18:00
                        v_am_over := 3600 - v_amount_ned;
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_rec.record_id := bg_get_local_seqno('TTRAFFIC_SEQ');
                        v_iznos_over := (v_am_over / 60) * 0.56;
                        --cijena je umnozak obracunskih jedinica i 0.56
                        insertstavke(c1_rec.record_id, v_rec,
                           v_rec.record_id, v_point_start,
                           get_sekunde(v_rt_over, 'UH'),
                           get_sekunde(v_am_over, 'UH'), 0, 0, 0, 16201, 0,
                           0);

                        update tbonus
                           set amount_min = 3600,
                               change_time = c1_rec.datumv
                         where korisnik_id = c1_rec.korisnik_id
                               and ociklus = c1_month.ociklus
                               and bonus_type = 'N';

                        v_point_start := v_point_start + v_rt_over / 86400;
                        v_rt_over := (
                                      (
                                       v_rec.datumv
                                       + get_sekunde(c1_rec.trajanje, 'US')
                                         / 86400
                                      )
                                      - v_point_start
                                     )
                                     * 86400;        --remaining time do 18:00
                        v_am_over := get_sekunde(c1_rec.otrajanje, 'US')
                                     -(
                                       get_sekunde(v_rec.otrajanje, 'US')
                                       + v_am_over
                                      );
                        --obracunske jedinice vremena do 18:00 zaokruzuje se
                        v_iznos_over := (v_am_over / 60) * 0.56;
                        --cijena je umnozak obracunskih jedinica i 0.56
                        v_rec.record_id := bg_get_local_seqno('TTRAFFIC_SEQ');
                        insertstavke(c1_rec.record_id, v_rec, v_rec.record_id,
                           v_point_start, get_sekunde(v_rt_over, 'UH'),
                           get_sekunde(v_am_over, 'UH'), 0, v_iznos_over,
                           v_iznos_over, 10604, 0, 0);
                     end if;
				  /*2.2 Pre�ao sam regularni bonus i presao sam nedjelju  *************************/
                  else
                     --presao sam sve moguce bonuse, samo mi stavi kategoriju poziva i pichi
                                /*
                                   insertstavke(v_rec, v_rec.record_id, v_rec.datumv, v_rec.trajanje, v_rec.otrajanje,
                                v_rec.impulsi, v_rec.iznos, v_rec.pcijena, v_rec.katpoziva_id, v_rec.flex_uus);*/
                     null;

                     --bekap orginalnog sloga posto ce sada da bude iscijepan
                     insert into tbrecord
                          values (v_rec.record_id, v_rec.naziv_datoteke,
                                  v_rec.call_id, v_rec.mrezna_grupa,
                                  v_rec.msisdn, v_rec.sifra_servisa,
                                  v_rec.sifra_sservisa, v_rec.pozvani_broj,
                                  v_rec.datumv, v_rec.trajanje,
                                  v_rec.otrajanje, v_rec.impulsi,
                                  v_rec.iznos, v_rec.pcijena,
                                  v_rec.bbroj_prefiks, v_rec.zona_id,
                                  v_rec.katpoziva_id, v_rec.plan_id,
                                  v_rec.tarifa_id, v_rec.pretplatnik_id,
                                  v_rec.korisnik_id, v_rec.bgrupa_id,
                                  v_rec.paket_id, v_rec.datumv_obrade,
                                  v_rec.flex_uus, c1_month.ociklus, null, 3);
                  end if;                     --nedjelja od v_amount_ned <3600
               end if;                                    --od v_amount < 3600
            end if;
--update tbrecord set parent_id = c1_rec.record_id where parent_id is null;
		 end if;

         end loop;


      end loop;                                                       --mjesec

--suspendovani bonus
      begin
         update tsbonus t
            set amount_min =
                   (select nvl(sum(get_sekunde(otrajanje, 'US')), 0)
                    from   tsbtraffic s
                    where  s.mrezna_grupa = t.mrezna_grupa
                           and s.msisdn = t.msisdn
                           and(s.mrezna_grupa, s.msisdn) in(
                                                   select mrezna_grupa,
                                                          msisdn
                                                   from   tspretplatnik ts1
                                                   where  status = '1')
                           and s.datumv >
                                 (select datum_aktivacije
                                  from   tspretplatnik ts2
                                  where  ts2.mrezna_grupa = s.mrezna_grupa
                                         and ts2.msisdn = s.msisdn));
      exception
         when others then
            update mitbprocesslog
               set status_report =
                      status_report || chr(13) || chr(10) || sysdate
                      || ': Greska kod agregacije suspendovanog bonusa, proces nastavlja dalje'
             where id = p_id;

            raise;
      end;

      commit;

      update mitbprocesslog
         set status_report = status_report || chr(13) || chr(10) || sysdate
                             || ': Bonus Handling -End'
       where id = p_id;

      tft_dblog.end_transaction(v_id, sysdate);
   exception
      when others then
         rollback;
         v_ok := 'F';
         tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
         raise;
   --null;
   end bonus_handling;

   /**
    * Odvajanje prometa za nebonusirani promet iz TTRAFFIC u TMTRAFFIC
	* i bri�e TTRAFFIC i TBTRAFFIC
	* @param p_degree - broj paralelizama
	* @param p_id - identifikator procesa za upis u log
    */
   procedure final_small(p_degree in number, p_id in number) is
      v_command varchar2(50);
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
      v_id number := 0;
      v_idalt number := -to_number(to_char(sysdate, 'rrrrddmmhh24miss'));
      v_progid number := get_programid('FinalSmall');
	  v_datum_obrade date := sysdate;
   begin
      begin
         if p_degree != 0 then
            execute immediate 'ALTER SESSION FORCE PARALLEL QUERY PARALLEL '
                              || p_degree;

            execute immediate 'ALTER SESSION FORCE PARALLEL DML PARALLEL '
                              || p_degree;
         end if;

         execute immediate 'set transaction use rollback segment RBIG';

         begin

            execute immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual'
            into              v_id;

            tft_dblog.start_transaction(v_id, v_progid, sysdate, sysdate,
               sysdate, 0, 0);

            update mdbeventlog
               set processlog_id = p_id
             where id = v_id;

            commit;
         exception
            when others then
               v_ok := 'F';

               if v_id = 0 then
                  v_id := v_idalt;
               end if;

               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;

         commit;

         execute immediate 'set transaction use rollback segment RBIG';

         begin
            insert      /*+ parallel(tmtraffic, 4) */into tmtraffic
               select /*+ parallel(ttraffic, 4) */
                      record_id, naziv_datoteke, call_id, mrezna_grupa,
                      msisdn, sifra_servisa, sifra_sservisa, pozvani_broj,
                      datumv, trajanje, otrajanje, impulsi, iznos, pcijena,
                      bbroj_prefiks, zona_id, katpoziva_id, plan_id,
                      tarifa_id, pretplatnik_id, korisnik_id, bgrupa_id,
                      paket_id, datumv_obrade, flex_uus
               from   ttraffic
               where  get_correction_needed(paket_id, katpoziva_id,
                         tarifa_id, 'ALL') = 0
                      and iscorrectpart(nvl(sifra_sservisa, 'XXX'), datumv,
                            trajanje, v_datum_obrade) = 1;

            --prebacujem prave CNT pozive

            --and nvl(sifra_sservisa, 'XXX') != 'CNT';
            commit;
         exception
            when others then
               v_ok := 'F';
               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
         end;

         rollback;                       --radi cannot read modify bla bla ora

         execute immediate 'ALTER SESSION DISABLE PARALLEL QUERY ';

         execute immediate 'ALTER SESSION DISABLE PARALLEL DML';

         /* update tptraffic
            set status = 0
          where tftraffic_id in(select record_id
                                from   ttraffic
                                where  iscorrectpart(nvl(sifra_sservisa,
                                             'XXX'), datumv, trajanje, v_datum_obrade) = 0); */
											 
		-- Prepravio DB 20.01.2005 (tp.TFTRAFFIC_ID do ovog trenutka is null)
		begin						 
			update tptraffic tp set
                tp.status = 0,
			    tp.TFTRAFFIC_ID = (select t.RECORD_ID
                      			   from ttraffic t
                      			   where iscorrectpart(nvl(t.sifra_sservisa,'XXX'),t.datumv,t.trajanje,v_datum_obrade) = 0
					    		     and t.CALL_ID = tp.CALL_ID
									 and t.MREZNA_GRUPA = tp.MREZNA_GRUPA
									 and t.MSISDN = tp.MSISDN
									 and t.SIFRA_SERVISA = tp.SIFRA_SERVISA)
            where exists (select 1
                          from ttraffic t
                      	  where iscorrectpart(nvl(t.sifra_sservisa,'XXX'),
						  					  t.datumv,
											  t.trajanje,
											  v_datum_obrade) = 0
					        and t.CALL_ID = tp.CALL_ID
							and t.MREZNA_GRUPA = tp.MREZNA_GRUPA
							and t.MSISDN = tp.MSISDN
							and t.SIFRA_SERVISA = tp.SIFRA_SERVISA);
		exception
            when TOO_MANY_ROWS then
               v_ok := 'F';
               tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
               raise;
        end;						

         --otkljucavam samo lazne CNT pozive
         tft_dblog.end_transaction(v_id, sysdate);

         update mdbeventlog
            set processlog_id = p_id
          where id = v_id;

         commit;
         v_command := 'truncate table ttraffic drop storage';

         execute immediate v_command;

         v_command := 'truncate table tbtraffic drop storage';

         execute immediate v_command;
      exception
         when others then
            v_ok := 'F';
            tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
            raise;
      end;
   end final_small;

   /**
    * Insertira slog u TBRECORD
    */
   procedure insertstavke(
      p_recor_orig number,
      p_rec       traffrec,
      p_record_id number,
      p_datumv    date,
      p_trajanje  number,
      p_otrajanje number,
      p_impulsi   number,
      p_iznos     number,
      p_pcijena   number,
      p_katpoziva number,
      p_flex_uus  number,
      p_parent_ind number
   ) is
      v_naziv_datoteke varchar2(30);
   begin
      v_naziv_datoteke := p_rec.naziv_datoteke;

      insert into tbrecord
           values (p_record_id, v_naziv_datoteke, p_rec.call_id,
                   p_rec.mrezna_grupa, p_rec.msisdn, p_rec.sifra_servisa,
                   p_rec.sifra_sservisa, p_rec.pozvani_broj, p_datumv,
                   p_trajanje, p_otrajanje, p_impulsi, p_iznos, p_pcijena,
                   p_rec.bbroj_prefiks, p_rec.zona_id, p_katpoziva,
                   p_rec.plan_id, p_rec.tarifa_id, p_rec.pretplatnik_id,
                   p_rec.korisnik_id, p_rec.bgrupa_id, p_rec.paket_id,
                   p_rec.datumv_obrade, p_flex_uus,
                   to_char(p_rec.datumv, 'MONRRRR',
                      'NLS_DATE_LANGUAGE=AMERICAN'),
                   p_recor_orig, p_parent_ind);
   end;

   /**
    * Funkcija provjerava da li je poziv potrebno bonusirati
	* NAPOMENA: Preraditi za EURO25
	* @param p_paket_id - identifikator paketa iz TTRAFFIC
	* @param p_katpoziva_id - identifikator kategorije poziva iz TTRAFFIC
	* @param p_tarifa_id - identifikator tarife iz TTRAFFIC
	* @param p_tip - identifikator tipa poziva iz TTRAFFIC
	* @return 0 ako poziv ne treba bonusirati, ina�e 1.
    */
   function get_correction_needed(p_paket_id in number,
      							  p_katpoziva_id in number,
      							  p_tarifa_id   in number,
      							  p_tip in varchar2)
      return number is
      v_rez number := 0;
   begin
      if p_tip = 'ALL' then
         if p_paket_id in(50, 52) and p_katpoziva_id = 10604 then
            v_rez := 1;
         /*elsif p_paket_id in(51, 53, 54, 55, 56) and p_tarifa_id = 8 then
            v_rez := 1;*/
         end if;
      elsif p_tip = 'BONUS' then
         if p_paket_id in(50, 52) and p_katpoziva_id = 10604 then
            v_rez := 1;
         end if;
      /*elsif p_tip = 'CCAT' then
         if p_paket_id in(51, 53, 54, 55, 56) and p_tarifa_id = 8 then
            v_rez := 1;
         end if;*/
      end if;

      return(v_rez);
   end get_correction_needed;

   /**
    * Provjerava da li je poziv bio u nedjelju izme�u 18-19h
	* @param p_datumv - po�etak poziva
	* @param p_trajanje - trajanje poziva
    */
   function issunday(p_datumv date, p_trajanje number)
      return varchar2 is
      v_issun varchar2(5);
   begin
      if to_char(p_datumv, 'D') = '7' then   --ja sam nedjelja da se upoznamo
         if to_number(to_char(p_datumv, 'HH24')) < 18
            and to_number(to_char((p_datumv
                                   + (get_sekunde(p_trajanje, 'US') - 1)
                                     / 86400), 'HH24')) = 18 then
            v_issun := 'YB';
         elsif to_number(to_char(p_datumv, 'HH24')) < 18
               and to_number(to_char((p_datumv
                                      + (get_sekunde(p_trajanje, 'US') - 1)
                                        / 86400), 'HH24')) > 18 then
            v_issun := 'YO';
         elsif to_number(to_char(p_datumv, 'HH24')) = 18
               and to_number(to_char((p_datumv
                                      + (get_sekunde(p_trajanje, 'US') - 1)
                                        / 86400), 'HH24')) = 18 then
            v_issun := 'YD';
         elsif to_number(to_char(p_datumv, 'HH24')) = 18
               and to_number(to_char((p_datumv
                                      + (get_sekunde(p_trajanje, 'US') - 1)
                                        / 86400), 'HH24')) > 18 then
            v_issun := 'YA';
         else
            v_issun := 'N';
         end if;
		 --napisi komentar zasto si sekundu oduzeo
		 --sekunda se oduzima zato sto poziv koji zavrsi tacno u 18:00 nije presao granicu niti je poziv koji
		 --zavrsava u 19:00 nije presao granicu, samim tim ta se sekunda oduzme da vidimo da li je poziv zavrsio tog trena
		 --ili nije
      else
         v_issun := 'N';
      end if;

--dbms_output.put_line (to_char(p_datumv, 'DAY'));
      return v_issun;
   end issunday;

   /**
    * Obra�un bonusa za prvih 60 min trajanja poziva
	* @param p_record_original - record_id iz TBTRAFFIC
	* @param p_trec - cjeli record iz TBTRAFFIC
	* @param v_amount - koliko je do sada potro�io iz TBONUS
	* @param p_ociklus - obra�unski ciklus iz shell skripte
	* @param p_cijeli_ind - 0 znaci da se predao odcjepljeni cdr a 1 znaci da se predao orginalni cdr
    */
   procedure first60bonus(p_record_original      number,
      		 			  p_trec                 traffrec,
      					  v_amount         in out number,
      					  p_ociklus              varchar2,
      					  p_cijeli_ind           number
   ) is
      v_am_over number;
      v_rt_over number;
      v_iznos_over number;
      v_datumv date;
      v_parent_ind number := 2;
   begin
--obrisi record
--delete from TTRAFFIC where record_id = p_trec.record_id;
      if p_cijeli_ind = 0 then
         v_parent_ind := 0;
      end if;

      if v_amount + get_sekunde(p_trec.otrajanje, 'US') <= 3600 then
         --citav poziv upada unutar bonusa, samo mjenjam cijenu i dodajem bonus

         --update TTRAFFIC set iznos = 0, katpoziva_id = 16101 where record_id = p_trec.record_id;
         insertstavke(p_record_original, p_trec, p_trec.record_id,
            p_trec.datumv, p_trec.trajanje, p_trec.otrajanje, p_trec.impulsi,
            0, 0, 16101, p_trec.flex_uus, v_parent_ind);
         v_amount := v_amount + get_sekunde(p_trec.otrajanje, 'US');

         update tbonus
            set amount_min = v_amount,
                change_time = p_trec.datumv
          where korisnik_id = p_trec.korisnik_id and ociklus = p_ociklus
                and bonus_type = 'P';
      else                                                         --tek preso
         --v_am_till:=(3600-p_amount);
         v_am_over :=(3600 - v_amount);
         v_rt_over :=(3600 - v_amount);

                      --v_am_tillH:=get_sekunde((3600-p_amount), 'UH');
         --obrisi record
                      --delete from TTRAFFIC where record_id = p_trec.record_id;
         --napravi dva

         --posto pravim dva ukoliko je rijec o cijelom slogu koji cijepam a ne dijelu
         --moram ga bekapirati sa kodom 1
         if p_cijeli_ind = 1 then
            insert into tbrecord
                 values (p_trec.record_id, p_trec.naziv_datoteke,
                         p_trec.call_id, p_trec.mrezna_grupa, p_trec.msisdn,
                         p_trec.sifra_servisa, p_trec.sifra_sservisa,
                         p_trec.pozvani_broj, p_trec.datumv, p_trec.trajanje,
                         p_trec.otrajanje, p_trec.impulsi, p_trec.iznos,
                         p_trec.pcijena, p_trec.bbroj_prefiks,
                         p_trec.zona_id, p_trec.katpoziva_id, p_trec.plan_id,
                         p_trec.tarifa_id, p_trec.pretplatnik_id,
                         p_trec.korisnik_id, p_trec.bgrupa_id,
                         p_trec.paket_id, p_trec.datumv_obrade,
                         p_trec.flex_uus, p_ociklus, null, 1);
         end if;

         --pravim cdr-ove po narudzbi, moje ruke, materijal sa centrale!!!:)
         if v_amount < 3600 then
--ovo je da ne bi napravio prazan slog, valjda cu se sjetiti sta sam htio da kazem
            insertstavke(p_record_original, p_trec, p_trec.record_id,
               p_trec.datumv, get_sekunde(v_rt_over, 'UH'),
               get_sekunde(v_am_over, 'UH'), p_trec.impulsi, 0, 0, 16101,
               p_trec.flex_uus, 0  --svakako ga cijepam dakle on je pocijepan
                                 );
         end if;

         --koristimo stare varijable da napravimo nove
         v_datumv := p_trec.datumv + v_am_over / 86400;
         v_am_over := get_sekunde(p_trec.otrajanje, 'US') - v_am_over;
         v_rt_over := get_sekunde(p_trec.trajanje, 'US') - v_rt_over;
         v_iznos_over := (v_am_over / 60) * 0.56;
         insertstavke(p_record_original, p_trec,
            bg_get_local_seqno('TTRAFFIC_seq'), v_datumv,
            get_sekunde(v_rt_over, 'UH'), get_sekunde(v_am_over, 'UH'), 0,
            v_iznos_over, v_iznos_over, 10604, 0, 0);

         --ako u ovom trenutku prelazis 3600 onda zakljucavamo stanje
         update tbonus
            set amount_min = 3600,
                change_time = p_trec.datumv
          where korisnik_id = p_trec.korisnik_id and ociklus = p_ociklus
                and bonus_type = 'P';

         v_amount := 3600;
      end if;
   end;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   procedure updatesusp(p_ciklus varchar2) is
      p_datum_begin date
         := trunc(to_date(p_ciklus, 'MONRRRR',
                 'NLS_DATE_LANGUAGE = American'), 'MONTH');
      p_datum_end date
         := last_day(trunc(to_date(p_ciklus, 'MONRRRR',
                     'NLS_DATE_LANGUAGE = American'),
                  'MONTH'))
            + 1 - 1 / 86400;
   begin
      /*insert into tsbonus
         select   mrezna_grupa, msisdn, p_ciklus,
                  sum(get_sekunde(otrajanje, 'US'))
         from     tstraffic
         where    datumv between p_datum_begin and p_datum_end
                  and katpoziva_id in(10601, 10602)
         group by mrezna_grupa, msisdn;*/
      commit;
/*
select mrezna_grupa, msisdn, p_ciklus, sum(get_sekunde(otrajanje, 'US'))
from tptraffic
where zona_id = 0
and status not in (0,1,2)
group by mrezna_grupa,msisdn;
*/
   end updatesusp;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   procedure tft_record_cut(
      p_rec       traffrec,
      p_datumv  in date,
      p_trajanje in number
   ) is
      type tbandsegs is record(
         starttime date,
         duration number,
         tariff varchar2(3),
         bduration number
      );

      type tbandentry is record(
         n_seq number,
         day_type varchar2(3),
         starttime varchar2(8),
         endtime varchar2(8),
         tariff varchar2(3),
         duration number,
         price number,
         fprice number
      );

      type tbandtable is table of tbandentry
         index by binary_integer;

      mytbandtable tbandtable;
      v_weekcount integer;
      v_starttime date;
      v_starttimeorig date := p_datumv;
      v_fstarttimeorig date := p_datumv;
      v_endtime date;
      v_duration integer;
      v_daynum varchar2(3);
      v_daynum_old varchar2(3) := 'XXX';
      v_part integer;
      --za sada su ovo integer-i, treba paziti kada se bude radilo sa sekundama
      v_opart integer;                     -- implicit round ce se desavati!!!
      v_fpart integer;
      v_fopart integer;
      k integer := 0;
      v_impulsi integer;
      v_record_id integer;
      v_katpoziva_id integer;
      v_flex_uus integer;
      v_prvi boolean := true;
      v_tpart number := 0;
      v_topart number := 0;
      v_tfpart number := 0;
      v_tfopart number := 0;
      v_tiznos number := 0;
      v_tpcijena number := 0;
      v_tfiznos number := 0;
      v_tfpcijena number := 0;
      v_otr number := get_sekunde(p_rec.otrajanje, 'US');
   begin
      execute immediate 'alter session set nls_territory = ''CROATIA''';

      v_daynum := to_char(p_datumv, 'D');
      --execute immediate 'alter session set nls_date_format = ''dd.mm.rrrr hh24:mi:ss''';

      -- inicijalizacija strukture za provlacenje kroz time bands
      mytbandtable(1).n_seq := 1;
      mytbandtable(1).day_type := 'SUN';
      mytbandtable(1).starttime := '00:00:00';
      mytbandtable(1).endtime := '23:59:59';
      mytbandtable(1).tariff := 'SUN';
      mytbandtable(1).duration := 24;
      mytbandtable(1).price := 0;
      mytbandtable(1).fprice := 0;
      mytbandtable(2).n_seq := 2;
      mytbandtable(2).day_type := 'ALL';
      mytbandtable(2).starttime := '00:00:00';
      mytbandtable(2).endtime := '06:59:59';
      mytbandtable(2).tariff := 'OFF';
      mytbandtable(2).duration := 7;
      mytbandtable(2).price := 0.115;
      mytbandtable(2).fprice := 0.23;
      mytbandtable(3).n_seq := 3;
      mytbandtable(3).day_type := 'ALL';
      mytbandtable(3).starttime := '07:00:00';
      mytbandtable(3).endtime := '18:59:59';
      mytbandtable(3).tariff := 'ON';
      mytbandtable(3).duration := 10;
      mytbandtable(3).price := 0.23;
      mytbandtable(3).fprice := 0.23;
      mytbandtable(4).n_seq := 4;
      mytbandtable(4).day_type := 'ALL';
      mytbandtable(4).starttime := '19:00:00';
      mytbandtable(4).endtime := '23:59:59';
      mytbandtable(4).tariff := 'OFF';
      mytbandtable(4).duration := 7;
      mytbandtable(4).price := 0.115;
      mytbandtable(4).fprice := 0.23;
      -- koliko je dana razgovor trajao
      v_starttime := p_datumv;
      v_duration := p_trajanje;
      v_endtime := v_starttime + get_sekunde(p_trajanje, 'US') / 86400;

      --weekcount predstavlja broj dana
      if trunc(v_endtime, 'DD') = trunc(v_starttime, 'DD') then
         v_weekcount := 1;                    --po?inje i zavr?ava istog dana
      else
         v_weekcount := ceil(trunc(v_endtime) - trunc(v_starttime)) + 1;
                                                  --ovo treba jos preispitati
--     v_weekcount := ceil(get_sekunde(p_trajanje, 'US') / 86400)+1;--ovo treba jos preispitati
      end if;

      DBMS_OUTPUT.put_line('Petlja ce raditi:' || v_weekcount || 'puta.');

      if to_char(p_datumv, 'D') = 7 then
         v_daynum := 'SUN';
      else
         v_daynum := 'ALL';
      end if;

      --execute immediate 'truncate table call_parts';
      for j in 1 .. v_weekcount loop
         --upis se vrsi prilikom promjene tipa dana ili pri zadnjem pozivu
         v_daynum_old := v_daynum;

         if to_char(p_datumv +(j - 1), 'D') = 7 then
            v_daynum := 'SUN';
         else
            v_daynum := 'ALL';
         end if;

         --postavljanje kategorije poziva
         if v_daynum_old = 'ALL' then
            v_katpoziva_id := p_rec.katpoziva_id;
         else
            if p_rec.paket_id in(51, 53) then
               v_katpoziva_id := 16202;
            else
               v_katpoziva_id := 16203;
            end if;
         end if;

         --ukoliko dodje do promjene dana treba to evidentirati
         if v_prvi = false then
            v_flex_uus := 0;
            v_impulsi := 0;
            v_record_id := bg_get_local_seqno('TTRAFFIC_seq');
         else
            v_flex_uus := p_rec.flex_uus;
            v_impulsi := p_rec.impulsi;
            v_record_id := p_rec.record_id;
         end if;

         /*DBMS_OUTPUT.put_line(v_daynum || v_daynum_old
            || bg_get_local_seqno('ttlog_seq'));*/
         if v_daynum_old != v_daynum then
            --kada dodje do promjene dana vrsi se upis
            insert into tmtraffic
                 values (v_record_id, p_rec.naziv_datoteke, p_rec.call_id,
                         p_rec.mrezna_grupa, p_rec.msisdn,
                         p_rec.sifra_servisa, p_rec.sifra_sservisa,
                         p_rec.pozvani_broj, v_starttimeorig,
                         get_sekunde(v_tpart, 'UH'),
                         get_sekunde(v_topart, 'UH'), v_impulsi, v_tiznos,
                         v_tpcijena, p_rec.bbroj_prefiks, p_rec.zona_id,
                         v_katpoziva_id, p_rec.plan_id, p_rec.tarifa_id,
                         p_rec.pretplatnik_id, p_rec.korisnik_id,
                         p_rec.bgrupa_id, p_rec.paket_id,
                         p_rec.datumv_obrade, v_flex_uus);

            v_otr := v_otr - v_topart;       --pamtim da bi upisao kako treba
            v_tpart := 0;
            v_topart := 0;
            v_tiznos := 0;
            v_tpcijena := 0;
            v_starttimeorig := v_starttime;
            v_tfpart := 0;
            v_tfopart := 0;
            v_tfiznos := 0;
            v_tfpcijena := 0;
            v_fstarttimeorig := v_starttime;
            v_prvi := false;
         end if;

--u dijelu ispod se vrsi racunanje
         for i in 1 .. 4 loop
            if v_daynum = mytbandtable(i).day_type then
               if v_starttime >= to_date(trunc(v_starttime, 'DD') || ' '
                                   || mytbandtable(i).starttime,
                                   'dd.mm.rrrr hh24:mi:ss')
                  and v_starttime <= to_date(trunc(v_starttime, 'DD') || ' '
                                       || mytbandtable(i).endtime,
                                       'dd.mm.rrrr hh24:mi:ss') then
                  if v_endtime >= to_date(trunc(v_starttime, 'DD') || ' '
                                    || mytbandtable(i).starttime,
                                    'dd.mm.rrrr hh24:mi:ss')
                     and v_endtime <= to_date(trunc(v_starttime, 'DD') || ' '
                                        || mytbandtable(i).endtime,
                                        'dd.mm.rrrr hh24:mi:ss') then
                     --zadnji komadic poziva njegova kategorija
                     if v_daynum = 'ALL' then
                        v_katpoziva_id := p_rec.katpoziva_id;
                     else
                        if p_rec.paket_id in(51, 53) then
                           v_katpoziva_id := 16202;
                        else
                           v_katpoziva_id := 16203;
                        end if;
                     end if;

                     --ukoliko dodje do promjene dana treba to evidentirati
                     if v_prvi = false then
                        v_flex_uus := 0;
                        v_impulsi := 0;
                        v_record_id := bg_get_local_seqno('TTRAFFIC_seq');
                     else
                        v_flex_uus := p_rec.flex_uus;
                        v_impulsi := p_rec.impulsi;
                        v_record_id := p_rec.record_id;
                     end if;

                     --ovdje se treba izvrsiti upis
                     k := k + 1;
                     --zadnji komad zavrsnog dijela razgovora
                     v_fpart := 86400 *((v_endtime - v_starttime));
                     -- zavrsni komadic ili cijeli razgovor
                     v_fopart := ceil(v_fpart / 60) * 60;
                     v_tfpart := v_fpart + v_tpart;
                     v_tfopart := v_otr;
                     --ostatak otrajanja se upisuje u zadnji poziv
                     v_tfiznos := v_tiznos
                                  + trunc(v_tfopart / 60
                                      * mytbandtable(i).price, 2);
                     v_tfpcijena := v_tpcijena
                                    +(
                                      v_tfopart / 60 * mytbandtable(i).fprice
                                     );

                     --ostatak iznosa se pravi tako sto saberemo sve
                        -- zavrsni komadic ili cijeli razgovor
                     insert into tmtraffic
                          values (v_record_id, p_rec.naziv_datoteke,
                                  p_rec.call_id, p_rec.mrezna_grupa,
                                  p_rec.msisdn, p_rec.sifra_servisa,
                                  p_rec.sifra_sservisa, p_rec.pozvani_broj,
                                  v_endtime - v_tfpart / 86400,
                                  get_sekunde(v_tfpart, 'UH'),
                                  get_sekunde(v_tfopart, 'UH'), v_impulsi,
                                  v_tfiznos, v_tfpcijena, p_rec.bbroj_prefiks,
                                  p_rec.zona_id, v_katpoziva_id,
                                  p_rec.plan_id, p_rec.tarifa_id,
                                  p_rec.pretplatnik_id, p_rec.korisnik_id,
                                  p_rec.bgrupa_id, p_rec.paket_id,
                                  p_rec.datumv_obrade, v_flex_uus);
                  /*insert into call_parts
                       values (k,
                               'Komadic: ' || 'Poceo u:'
                               || to_char(v_starttime, 'Day')
                               || to_char(v_starttime,
                                    'dd.mm.rrrr hh24:mi:ss')
                               || ' Trajao(min):' || v_fpart
                               || ' A OTrajao(min):' || v_fopart);*/
                  else
                     -- sjeckaj i trci dalje
                     v_part := 86400
                               *(
                                 (
                                  to_date(trunc(v_starttime, 'DD') || ' '
                                     || mytbandtable(i).endtime,
                                     'dd.mm.rrrr hh24:mi:ss')
                                  - v_starttime
                                 )
                                );
                     v_opart := ceil(v_part / 60) * 60;
                     k := k + 1;
                     v_tpart := v_tpart + v_part;
                     v_topart := v_topart + v_opart;
                     v_tiznos := v_tiznos
                                 + trunc(v_opart / 60 * mytbandtable(i).price,
                                     2);
                     v_tpcijena := v_tpcijena
                                   +(v_opart / 60 * mytbandtable(i).fprice);
                     --ovo je sabiranje za zavrsni dio razgovora od nedelju u ponoc do nekada
                     v_fpart := 86400 *((v_endtime - v_starttime));
                     v_fopart := ceil(v_fpart / 60) * 60;
                     v_tfiznos := v_tfiznos
                                  + trunc(v_fopart / 60
                                      * mytbandtable(i).price, 2);
                     v_tfpcijena := v_tfpcijena
                                    + trunc((
                                         v_fopart / 60
                                         * mytbandtable(i).fprice
                                        ),
                                        2);
                     v_tfpart := v_fpart + v_tpart;
                     v_tfopart := ceil(v_tfpart / 60) * 60;
                     v_starttime := 1 / 86400
                                    + to_date(trunc(v_starttime, 'DD') || ' '
                                        || mytbandtable(i).endtime,
                                        'dd.mm.rrrr hh24:mi:ss');
                  end if;
               end if;
            end if;
         end loop;
      end loop;
   end;

   /**
    * Provjerava da li je slog korektan za odvajanje u TBTRAFFIC
	* @param p_sifra_sservisa - sifra_sservisa iz TTRAFFIC
	* @param p_datumv - datumv iz TTRAFFIC
	* @param p_trajanje - trajanje iz TTRAFFIC
	* @param p_predati_datum - datum_obrade iz TTRAFFIC
	* @return 1 ako je slog korektan, 0 ako nije
    */
   function iscorrectpart(p_sifra_sservisa varchar2,
      					  p_datumv date,
      					  p_trajanje number, 
						  p_predati_datum date)
      return number is
      v_correctpart number(1) := 1;
   begin
      if p_predati_datum between trunc(p_predati_datum, 'MONTH')
                     and trunc(p_predati_datum, 'MONTH') + get_udroffset then
         if p_sifra_sservisa = 'CNT' --@todo IZBACITI
            and trunc(p_datumv, 'MONTH') = trunc(p_predati_datum, 'MONTH') then
            v_correctpart := 0;
         end if;
      else
         if p_sifra_sservisa = 'CNT' then
            v_correctpart := 0;
         end if;
      end if;

      return v_correctpart;
	  
   end iscorrectpart;

   /**
    * Djeli slogove iz TBTRAFFIC na trajanju obra�unskig ciklusa
    */
   procedure udrmidnightcut(p_id number) 
   is
      cursor ccandidate is
         select record_id, naziv_datoteke, call_id, mrezna_grupa, msisdn,
                sifra_servisa, sifra_sservisa, pozvani_broj, datumv,
                trajanje, otrajanje, impulsi, iznos, pcijena, bbroj_prefiks,
                zona_id, katpoziva_id, plan_id, tarifa_id, pretplatnik_id,
                korisnik_id, bgrupa_id, paket_id, datumv_obrade, flex_uus
         from   tbtraffic t
         where  trunc(datumv, 'MONTH') !=
                   trunc(datumv + (get_sekunde(trajanje, 'US') - 1) / 86400,
                      'MONTH')
		and trajanje != 0;

      v_midnight date;
      v_trajanje number;
      v_otrajanje number;
      v_iznos number;
      v_id number;
      v_progid number := get_programid('UdrMidnightCut');
      v_datmin ttraffic.datumv_obrade%type;
      v_datmax ttraffic.datumv_obrade%type;
      v_recmin ttraffic.record_id%type;
      v_recmax ttraffic.record_id%type;
   begin
      execute immediate 'alter session set nls_territory = ''CROATIA''';

      execute immediate 'alter session set nls_date_format = ''dd.mm.rrrr hh24:mi:ss''';

      execute immediate 'select MDBEVENTLOG_SEQ.NEXTVAL from dual'
      into              v_id;

      select min(datumv), max(datumv), count(*), sum(impulsi)
      into   v_datmin, v_datmax, v_recmin, v_recmax
      from   tbtraffic
      where  trunc(datumv, 'MONTH') != trunc(datumv
                                         + (get_sekunde(trajanje, 'US') - 1)
                                           / 86400,
                                         'MONTH');

      update mitbprocesslog
         set status_report =
                status_report || chr(13) || chr(10) || sysdate
                || ': Rezanje CDR-ova u pono� na prelazu mjeseca - Start '
       where id = p_id;

      tft_dblog.start_transaction(v_id, v_progid, sysdate, v_datmin, v_datmax,
         v_recmin, v_recmax);

      update mdbeventlog
         set processlog_id = p_id
       where id = v_id;

      commit;

      for c1_rec in ccandidate loop
         v_midnight := trunc(last_day(c1_rec.datumv), 'DD') + 1;
         v_trajanje := (v_midnight - c1_rec.datumv) * 86400;
         v_otrajanje := ceil(v_trajanje / 60) * 60;
         v_iznos := (v_otrajanje / 60) * 0.56;

         delete from tbtraffic
               where record_id = c1_rec.record_id;

	     --prvi slog
         insert into tbtraffic
              values (c1_rec.record_id, c1_rec.naziv_datoteke,
                      c1_rec.call_id, c1_rec.mrezna_grupa, c1_rec.msisdn,
                      c1_rec.sifra_servisa, c1_rec.sifra_sservisa,
                      c1_rec.pozvani_broj, c1_rec.datumv,
                      get_sekunde(v_trajanje, 'UH'),
                      get_sekunde(v_otrajanje, 'UH'), c1_rec.impulsi,
                      v_iznos, v_iznos, c1_rec.bbroj_prefiks, c1_rec.zona_id,
                      c1_rec.katpoziva_id, c1_rec.plan_id, c1_rec.tarifa_id,
                      c1_rec.pretplatnik_id, c1_rec.korisnik_id,
                      c1_rec.bgrupa_id, c1_rec.paket_id,
                      c1_rec.datumv_obrade, c1_rec.flex_uus);

		 --drugi slog
         insert into tbtraffic
              values (bg_get_local_seqno('TTRAFFIC_seq'),
                      c1_rec.naziv_datoteke, c1_rec.call_id,
                      c1_rec.mrezna_grupa, c1_rec.msisdn,
                      c1_rec.sifra_servisa, c1_rec.sifra_sservisa,
                      c1_rec.pozvani_broj, v_midnight,
                      get_sekunde(get_sekunde(c1_rec.trajanje, 'US')
                         - v_trajanje, 'UH'),
                      get_sekunde(get_sekunde(c1_rec.otrajanje, 'US')
                         - v_otrajanje, 'UH'),
                      0, c1_rec.iznos - v_iznos, c1_rec.pcijena - v_iznos,
                      c1_rec.bbroj_prefiks, c1_rec.zona_id,
                      c1_rec.katpoziva_id, c1_rec.plan_id, c1_rec.tarifa_id,
                      c1_rec.pretplatnik_id, c1_rec.korisnik_id,
                      c1_rec.bgrupa_id, c1_rec.paket_id,
                      c1_rec.datumv_obrade, 0);
      end loop;

      commit;

      update mitbprocesslog
         set status_report =
                status_report || chr(13) || chr(10) || sysdate
                || ': Rezanje CDR-ova u pono� na prelazu mjeseca - End '
       where id = p_id;

      tft_dblog.end_transaction(v_id, sysdate);
   	  exception
      when others then
         rollback;
         v_ok := 'F';
         tft_dblog.write_error(v_id, v_progid, sysdate, sqlerrm);
         raise;
   end udrmidnightcut;
  
end tft_hour_handling;
