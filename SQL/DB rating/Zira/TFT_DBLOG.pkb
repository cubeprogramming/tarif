/* Formatted on 2005/01/10 10:38 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PACKAGE BODY tft_dblog
IS
   PROCEDURE start_transaction (
      p_id               IN   NUMBER,
      p_programid        IN   VARCHAR2
--implicitni typecasting kod kojeg trba predati number a ne varchar2 ovdje je najvjerovatnije greska u pisanju
                                      ,
      p_start_datetime   IN   DATE,
      p_min_datetimep    IN   DATE,
      p_max_datetimep    IN   DATE,
      p_min_recordid     IN   NUMBER,
      p_max_recordid     IN   NUMBER
   )
   IS
   BEGIN
      INSERT INTO mdbeventlog
                  (ID, program_id, start_datetime, min_datetimep,
                   max_datetimep, status, min_record_id, max_record_id
                  )
           VALUES (p_id, p_programid, p_start_datetime, p_min_datetimep,
                   p_max_datetimep, 'R', p_min_recordid, p_max_recordid
                  );

      COMMIT;

      EXECUTE IMMEDIATE 'set transaction use rollback segment RBIG';
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END start_transaction;

   PROCEDURE end_transaction (p_id IN NUMBER, p_stop_datetime IN DATE)
   IS
   BEGIN
      UPDATE mdbeventlog
         SET end_datetime = p_stop_datetime,
             status = 'O'
       WHERE ID = p_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END end_transaction;

   PROCEDURE write_error (
      p_id               IN   NUMBER,
      p_programid        IN   NUMBER,
      p_error_datetime   IN   DATE,
      p_description      IN   VARCHAR2
   )
   IS
      v_dummy   VARCHAR2 (1);
   BEGIN
      ROLLBACK;               -- Transaction rollback and error event logging

      SELECT 'x'
        INTO v_dummy
        FROM mdbeventlog
       WHERE ID = p_id;

      UPDATE mdbeventlog                                         -- Error data
         SET error_datetime = p_error_datetime,
             status = 'F',
             error_description = p_description
       WHERE ID = p_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         INSERT INTO mdbeventlog
                     (ID, program_id, start_datetime, error_datetime,
                      status, error_description
                     )
              VALUES (p_id, p_programid, p_error_datetime, p_error_datetime,
                      'F', p_description
                     );

         COMMIT;
   END write_error;

   PROCEDURE update_transaction (p_id IN NUMBER, p_process_id IN NUMBER)
   IS
   BEGIN
      UPDATE mdbeventlog
         SET processlog_id = p_process_id
       WHERE ID = p_id;

      COMMIT;
   END update_transaction;
END tft_dblog;
/