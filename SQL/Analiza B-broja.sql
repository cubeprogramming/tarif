/* Analiza B-broja */

-- Zona_id

select sifra,
	   zona_id 
from tmrezna_grupa --tzemlja_mspecial, tvas, tinvas
--where sifra=p_bbroj_prefiks

select sifra,
	   zona_id 
from tzemlja_mspecial

select sifra,
	   zona_id,
	   bbroj_ind 
from tvas

select sifra,
	   zona_id,
	   bbroj_ind
from tinvas
where sifra like '%***%'


--katpoziva_id
	

 -- Subselect 1
select paket_id,
	   pvrsta_usluge_id
from tzona_ppaket
where status='A'
  and zona_id = :p_zona_id

 -- Subselect 2
select p.ID paket_id,
	   zp.PVRSTA_USLUGE_ID
from tzona_pristup zp,ppaket p
where zp.vrsta_pristupa_id=p.vrsta_pristupa_id
  and zp.ZONA_ID = :P_ZONA_ID
  
-- Spoj subselecta 1 i 2
select tz.paket_id,
	   tz.pvrsta_usluge_id
from tzona_ppaket tz
where status='A'
  and zona_id = :p_zona_id
union
select p.ID paket_id,
	   zp.PVRSTA_USLUGE_ID
from tzona_pristup zp,ppaket p
where zp.vrsta_pristupa_id=p.vrsta_pristupa_id
  and zp.ZONA_ID = :P_ZONA_ID   
  
select tz.zona_id,
	   tz.paket_id,
	   tz.pvrsta_usluge_id
from tzona_ppaket tz
where tz.status='A'
union
select zp.ZONA_ID,
	   p.ID paket_id,
	   zp.PVRSTA_USLUGE_ID
from tzona_pristup zp,ppaket p
where zp.vrsta_pristupa_id=p.vrsta_pristupa_id    
  
 -- Subselect 3
select kp.pvrsta_usluge_id
from tkomb_ppaket kp
where kp.kombinacija_id=:p_kombinacija_id--id iz tablice tkombinacija
and kp.paket_id=:p_paket_id--paket dobiven u analizi a-broja  
  
select kp.PAKET_ID,
	   kp.KOMBINACIJA_ID,
	   kp.PVRSTA_USLUGE_ID katpoziva_id
from tkomb_ppaket kp
where kp.PAKET_ID = 50
order by kp.PAKET_ID,
	   kp.KOMBINACIJA_ID
  
  --Master select:
select distinct
	   tzp.ZONA_ID
from tzona_ppaket tzp,
	 tzona_pristup zp
where tzp.ZONA_ID = zp.ZONA_ID
  and tzp.ZONA_ID = 69
  
select tzp.ZONA_ID
from tzona_ppaket tzp
union
select zp.ZONA_ID
from tzona_pristup zp
 
	 
--plan_id 

select plan_id
from   ppaket_tplan
where  :p_datumv between datum_start and nvl(datum_stop, sysdate + 1)
  and status = '1' and paket_id = :p_paket_id
  and plan_id<90;
  
select paket_id,
	   plan_id,
	   datum_start,
	   nvl(datum_stop, sysdate + 1) datum_stop
from ppaket_tplan
where status = '1'
  and plan_id<90
order by paket_id, plan_id  

-- klasa_dana_id

select k.zona_id,
	   k.plan_id,
	   u.id,  
	   u.klasa_dana_id, 
	   kd.NAZIV, 
	   u.vopseg_id, 
	   o.VRIJEME_OD, 
	   o.VRIJEME_DO, 
	   o.opis, 
	   u.opis
from tuzorak u,
	 tvremenski_opseg o,
	 tklasa_dana kd,
	 tkombinacija k
where u.KLASA_DANA_ID=kd.id
and u.VOPSEG_ID=o.id
and k.UZORAK_ID=u.id
and k.plan_id=:p_plan_id
and k.zona_id=:p_zona_id
order by k.zona_id, k.plan_id

   -- Select za program
   select kd.id klasa_dana_id, 
	   	  kd.NAZIV
   from tklasa_dana kd
   
   select distinct
   		  kd.id klasa_dana_id, 
	   	  kd.NAZIV
   from tklasa_dana kd,
   		ttip_datuma td
   where td.KLASA_DANA_ID(+) = kd.id

   -- Select za program loadTkombinacija(int zona_id)
   select k.plan_id,
		  k.UZORAK_ID,
		  k.TARIFA_ID
   from tkombinacija k
   where k.zona_id = :p_zona_id 
   order by k.plan_id
   

select klasa_dana_id 
from ttip_datuma
where datum_od >= :p_datumv
and datum_do <= :p_datumv

select klasa_dana_id 
from ttip_dana
where naziv=to_char(:p_datumv,'DAY')

--opseg_id

select id opseg_id
from tvremenski_opseg
where to_date('01.01.2000'||' '||to_char(:p_datumv,'hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')>=
	  to_date('01.01.2000'||' '||to_char(vrijeme_od,'hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')
and   to_date('01.01.2000'||' '||to_char(:p_datumv,'hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')<=
	  to_date('01.01.2000'||' '||to_char(vrijeme_do,'hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')
	  
  -- Select za program:
  select vo.id opseg_id,
	     vo.vrijeme_od,
	     vo.vrijeme_do
  from tvremenski_opseg vo

--uzorak_id

-- kada saznamo opseg_id i klasa_dana_id
SELECT u.id uzorak_id,  u.klasa_dana_id, kd.NAZIV, u.vopseg_id, o.VRIJEME_OD, o.VRIJEME_DO, o.opis, u.opis
FROM tuzorak u,tvremenski_opseg o,tklasa_dana kd,tkombinacija k
WHERE u.KLASA_DANA_ID=kd.id
AND u.VOPSEG_ID=o.id
AND k.UZORAK_ID=u.id
AND k.plan_id=p_plan_id
AND k.zona_id=p_zona_id
AND u.vopseg_id in (p_opseg_id)--opseg_id ima 1 ili vi�e
AND u.klasa_dana_id in (p_klasa_dana_id)-p_klasa_id ima jedan ili vi�e
-- iz selekta treba dobiti uzorak_id (u.id)!!!!

   -- Select za program:
   select u.klasa_dana_id,
	   	  u.vopseg_id,
   		  u.id
   from tuzorak u


koeficijent1 (obra�unska jedinica), usluga_id, tarifa_id,koeficijent2 (pick off koef)

-- kada prona?emo odgovatajuai uzorak_id
select f.KOEFICIJENT koeficijent1,
	   k.usluga_id,
	   k.tarifa_id,
	   t.KOEFICIJENT koeficijent2, --into p_fatkor,p_usluga_id,p_tarifa_id,
	   k.ID kombinacija_id
from tkombinacija k,
	 tfaktor f, 
	 ttarifa t
where k.plan_id=1
  and k.faktor_id=f.id
  and k.zona_id= :p_zona_id
  and t.id=k.tarifa_id
  and k.uzorak_id= :p_uzorak_id
--p_tarifa_id ide u ttraffic, a p_faktor i p_usluga_ide slu�e za izraeunavanje cijene i otrajanje

--na�in obra�una i cijena

select cijena,tip_obracuna--into p_cijena,p_tip_obracuna
from fusluga
where id=:p_usluga_id

obra�unsko trajanje

--otrajanje

if mod(p_trajanje,p_koeficijent1)=0
then
	 p_otrajanje:=get_sekunde(p_trajanje,'UH');
else
	p_otrajanje:=get_sekunde((trunc(p_trajanje/p_koeficijent1) +1 )*p_koeficijent1,'UH');
end if;

SELECT round(5.59*get_sekunde(115,'US')/60,2),
	   5.58*get_sekunde(115,'US')/60
from dual

	  

