CREATE OR REPLACE TRIGGER "ARATE".TSTRAFFIC_SEQ 
before insert on TSTRAFFIC
referencing NEW as NEW 
for each row 
begin 
	 select SEQ_TRAFFIC.nextval into :new.RECORD_ID from dual; 
end;
/
