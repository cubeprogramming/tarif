-- GLAVNI LOG

select *
from MITBPROCESSLOG
order by START_DATETIME desc

WHERE UPPER(STATUS_REPORT) like '%RATTING%'

select *
from MPROGSTATUS

select ID
from MPROGRAM
where NAME = 'Rating'

INSERT INTO MITBPROCESSLOG (
	   ID, 	   
	   PROGRAM_ID,
	   START_DATETIME,
	   STOP_DATETIME,
	   STATUS,
	   STATUS_REPORT,
	   OZNAKA_OBRADE )
VALUES (
	   MITBPROCESSLOG_SEQ.nextval,
	   (select ID
	    from MPROGRAM
		where NAME = 'Billing'),
		sysdate,
		sysdate,
		'R',
		--'28.08.2001 20:41:15 : Pocetak tarifiranja ' || CHR(10) || '28.08.2001 20:31:01 : Pocetak dnevnog ciklusa obrade',
		to_char(sysdate,'DD.MM.YYYY HH24:MI:SS') || ' : Pocetak dnevnog ciklusa obrade ' || CHR(10) || to_char(sysdate,'DD.MM.YYYY HH24:MI:SS') || ' : Pocetak rattinga',
		1 )
		
UPDATE MITBPROCESSLOG p SET
	   p.STOP_DATETIME = sysdate,
	   p.STATUS = 'O',
	   p.STATUS_REPORT = (select plg.STATUS_REPORT || CHR(10) || to_char(sysdate,'DD.MM.YYYY HH24:MI:SS') || ' : Kraj rattinga'
	  			          from MITBPROCESSLOG plg
					      where plg.ID = p.ID ) 
where ID = (select max(ID)
	  		from MITBPROCESSLOG pl
		    where pl.PROGRAM_ID = (select ID
	    			               from MPROGRAM
						   		   where NAME = 'Billing'))
								   
-- FILE LOG

select id,
	   FILE_NAME_IN,
	   FILE_NAME_OUT,
	   MIN_FILE_DATETIME,
	   MAX_FILE_DATETIME,
	   START_DATETIME,
	   END_DATETIME,
	   RECORD_COUNT_IN,
	   RECORD_COUNT_OUT,
	   PULSE_COUNT_IN,
	   PULSE_COUNT_OUT,   
	   LOG_FILE,
	   PROGRAM_ID, 
	   PROGSTATUS_CODE,
	   STATUS,
	   PROCESSLOG_ID 
from MCEVENTLOG	
--where min_file_datetime is null
order by id desc	

INSERT INTO MCEVENTLOG (
	 FILE_NAME_IN,
	 FILE_NAME_OUT,
	 START_DATETIME,
	 END_DATETIME,
	 RECORD_COUNT_IN,
	 RECORD_COUNT_OUT,
	 LOG_FILE,
	 PROGRAM_ID, 
	 PROGSTATUS_CODE,
	 STATUS,
	 PROCESSLOG_ID )
VALUES (
	 'BLA2',
	 'BLA',
	 sysdate,
	 sysdate,
	 0,
	 0,
	 'BLA',
	 (select ID
      from MPROGRAM
      where NAME = 'Rating'),
	 'R',
	 0,
	 (select max(ID)
	  		from MITBPROCESSLOG pl
		    where pl.PROGRAM_ID = (select ID
	    			               from MPROGRAM
						   		   where NAME = 'Billing')) ) 					   
								   
UPDATE MCEVENTLOG SET
	 MIN_FILE_DATETIME = sysdate,
	 MAX_FILE_DATETIME = sysdate,
	 END_DATETIME = sysdate,
	 RECORD_COUNT_IN = 1,
	 RECORD_COUNT_OUT = 1,
	 PULSE_COUNT_IN = 0,
	 PULSE_COUNT_OUT = 0,
	 PROGSTATUS_CODE = 'O' 
where ID = (select max(ID)
	  		from MCEVENTLOG el
		    where el.FILE_NAME_IN = 'tt35001_20041028_152222.txt'
			  and el.PROGRAM_ID = (select ID
	    			               from MPROGRAM
						   		   where NAME = 'Rating'))								   