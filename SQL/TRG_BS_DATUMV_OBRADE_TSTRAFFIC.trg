CREATE OR REPLACE TRIGGER ARATE.TRG_BS_DATUMV_OBRADE_TSTRAFFIC
BEFORE INSERT or UPDATE
ON ARATE.TSTRAFFIC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05.10.04     Dalibor Blazvic        1. Created this trigger.
   1.1		  03.12.04	   Dalibor Bla�evi�		  2. Dodan i update

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     
      Sysdate:         05.10.04
      Date and Time:   05.10.04, 10:00:49, and 05.10.04 10:00:49
      Username:         Arate
      Table Name:       TSTRAFFIC
      Trigger Options:  AFTER INSERT FOR EACH ROW
******************************************************************************/
BEGIN
   :new.DATUMV_OBRADE := tft_utils.Get_Udr_Time(:new.DATUMV);
   
   EXCEPTION
     WHEN OTHERS THEN
       raise_application_error(SQLCODE-20000, SQLERRM||' Greska u trigeru: TRG_BS_DATUMV_OBRADE_TSTRAFFIC'||USER);
END ;
/
