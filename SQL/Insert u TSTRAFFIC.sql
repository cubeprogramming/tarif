INSERT /*+APPEND*/ INTO TSTRAFFIC ts
	   (ts.NAZIV_DATOTEKE,
		ts.CALL_ID,
		ts.MREZNA_GRUPA,
		ts.MSISDN,
		ts.SIFRA_SERVISA,
		ts.SIFRA_SSERVISA,
		ts.POZVANI_BROJ,
		ts.DATUMV,
		ts.TRAJANJE,
		ts.OTRAJANJE,
		ts.IMPULSI,
		ts.IZNOS,
		ts.BBROJ_PREFIKS,
		ts.ZONA_ID,
		ts.KATPOZIVA_ID,
		ts.PLAN_ID,
		ts.TARIFA_ID,
		ts.A_CATEGORY,
		ts.B_CATEGORY,
		ts.TYPE_OF_B_NUM,
		ts.CHARGED_PARTY,
		ts.ORIGIN_FOR_CHARGING,
		ts.TARIFF_CLASS,
		ts.CHARGING_CASE,
		ts.CHARGING_CASE_ORIGIN,
		ts.DESCRIPTION,
		ts.FLEX_UUS,
		ts.ERROR_CODE,
		ts.TYPE_OF_ASUBSCRIBER)
VALUES (
	   'TT52004_20031101_000755.txt',
	   '1853221929',
	   '051',
	   '274214',
	   '10',
	   'FWD',
	   '076767676',
	   to_date('2004-09-30','YYYY-MM-DD'),
	   2410,
	   2500,
	   0,
	   2.56,
	   076,
	   51,
	   10713,
	   1,
	   1,
	   '0',
	   null,
	   null,
	   '0',
	   null,
	   null,
	   '28',
	   null,
	   'Nepoznata zona B-broja',
	   1,
	   '921',
	   '2'
);


 cause_for_output = 1
 record_number = 2
 naziv_datoteke = TT52004_20031101_000755.txt
 call_id = 1853221929
 mrezna_grupa = 52
 msisdn = 830552
 sifra_servisa = 10
 sifra_sservisa = FWD
 pozivni_broj = 076767676
 datumv = 2003-10-31
 trajanje = 0
 otrajanje = null
 impulsi = 1
 status = 0
 iznos = null
 pcijena = null
 bbroj_prefiks = null
 zona_id = null
 katpoziva_id = null
 plan_id = null
 tarifa_id = null
 pretplatnik_id = null
 korisnik_id = null
 bgrupa_id = null
 paket_id = null
 a_category = 0
 b_category = null
 type_of_b_num = null
 charged_party = 0
 origin_for_charging = null
 tariff_class = null
 charging_case = 28
 charging_case_origin = null
 tftraffic_id = null
 flex_uus = 1
 file_prefix = TT52004
 type_of_a_num = 2

 select code,
	   log_text
from ctariffcode

/* paket_id
 *     bgrupa_id
 bbroj_prefiks
 *     zona_id
 *     plan_id
 *     tarifa_id
 *     katpoziva_id
 *     error_code
 *     description
 pcijena
 *     iznos
 *     otrajanje 
 */