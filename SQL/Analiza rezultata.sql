select * 
from TTRAFFIC_19112004

select * 
from TPTRAFFIC_19112004

select *
from TSTRAFFIC_19112004

select * 
from TTRAFFIC_19112004
where record_id in (145349,148373,146287,136883,146782,148897,133204,
	  			    138474,139024,132377,150384,142303,134169,139368,
					139434,133155,136465,135057,136273,138701,135512)
					
select *
from TTRAFFIC 
where katpoziva_id is null	

alter session force parallel query parallel 4;	

select * 
from TTRAFFIC t
where exists (select 1 
	  		  from TTRAFFIC_19112004 tt
			  where tt.record_id in (145349,148373,146287,136883,146782,148897,133204,
	  			    		         138474,139024,132377,150384,142303,134169,139368,
								  	 139434,133155,136465,135057,136273,138701,135512)
			  and tt.NAZIV_DATOTEKE = t.NAZIV_DATOTEKE
			  and tt.CALL_ID = t.CALL_ID) 
	
select t.MSISDN,
	   t.POZVANI_BROJ,
	   t.ZONA_ID,
	   t.PAKET_ID,
	   t.KATPOZIVA_ID,
	   t.DATUMV,
	   t.TRAJANJE,
	   t.OTRAJANJE,
	   t.IMPULSI,
	   t.PCIJENA,
	   t.IZNOS  
from TTRAFFIC_19112004 t
where record_id in (145349,148373,146287,136883,146782,148897,133204,
	  			    138474,139024,132377,150384,142303,134169,139368,
					139434,133155,136465,135057,136273,138701,135512)
					
select t.CALL_ID,
	   t.MSISDN,
	   t.POZVANI_BROJ,
	   t.ZONA_ID,
	   t.PLAN_ID,
	   t.TARIFA_ID,
	   t.PAKET_ID,
	   t.KATPOZIVA_ID,
	   t.DATUMV,
	   t.TRAJANJE,
	   t.OTRAJANJE,
	   t.IMPULSI,
	   t.PCIJENA,
	   t.IZNOS
from TTRAFFIC t
where exists (select 1 
	  		  from TTRAFFIC_19112004 tt
			  where tt.record_id in (145349,148373,146287,136883,146782,148897,133204,
	  			    		         138474,139024,132377,150384,142303,134169,139368,
								  	 139434,133155,136465,135057,136273,138701,135512)
			  and tt.NAZIV_DATOTEKE = t.NAZIV_DATOTEKE
			  and tt.CALL_ID = t.CALL_ID)					
			  
/* Analiza rezultata od 12.01.2004 */

--Nije dobro zaokruživanje			  										
select t.CALL_ID,
	   t.MSISDN,
	   t.POZVANI_BROJ,
	   t.ZONA_ID,
	   t.PAKET_ID,
	   t.KATPOZIVA_ID,
	   t.DATUMV,
	   t.TRAJANJE,
	   t.OTRAJANJE,
	   t.IMPULSI,
	   t.PCIJENA,
	   t.IZNOS  
from TTRAFFIC_12012005 t
where call_id in (8910414,9340930,9346628)

--Katpoziva_id se ne određuje dobro
select distinct 
	   t.katpoziva_id,
	   i.katpoziva_id,
	   t.paket_id,
	   i.paket_id,
	   t.plan_id,
	   i.plan_id,
	   t.zona_id,
	   i.zona_id
from TTRAFFIC_12012005 t,
	 IS_TFT_TEST_22102004@itbos i
where i.call_id=t.call_id
and i.mrezna_grupa=t.mrezna_grupa
and i.msisdn=t.msisdn
and i.datumv=t.datumv
and i.trajanje=t.trajanje
and t.katpoziva_id!=i.katpoziva_id
and i.paket_id=t.paket_id

-- Spoj subselecta 1 i 2
select tz.paket_id,
	   tz.pvrsta_usluge_id
from tzona_ppaket tz
where status='A'
  and tz.PAKET_ID = :PAKET_ID
  and tz.zona_id = :p_zona_id

select p.ID paket_id,
	   zp.PVRSTA_USLUGE_ID
from tzona_pristup zp,ppaket p
where zp.vrsta_pristupa_id=p.vrsta_pristupa_id
  and p.ID =:PAKET_ID 
  and zp.ZONA_ID = :P_ZONA_ID 
  
--Razlike kod obrade time bandova  
select t.call_id,
	   t.korisnik_id,
	   t.otrajanje,
	   i.otrajanje,
	   t.pcijena,
	   i.pcijena,
	   t.iznos,
	   i.iznos,
	   t.katpoziva_id,
	   i.katpoziva_id,
	   i.paket_id,
	   t.paket_id
from TTRAFFIC_14012005_BAND t,
	 is_tft_time_band@itbos i
where i.call_id=t.call_id
  and i.mrezna_grupa=t.mrezna_grupa
  and i.msisdn=t.msisdn
  and i.datumv=t.datumv
  and i.trajanje=t.trajanje
  and i.zona_id!=0
  and i.iznos!=t.iznos
  and i.paket_id=t.paket_id
  and i.pcijena!=0
  and t.iznos<t.PCIJENA  

select t.call_id,
	   t.korisnik_id,
	   t.otrajanje,
	   i.otrajanje,
	   t.pcijena,
	   i.pcijena,
	   t.iznos,
	   i.iznos,
	   t.katpoziva_id,
	   i.katpoziva_id,
	   i.paket_id,
	   t.paket_id
from TTRAFFIC_14012005_BAND t,
	 is_tft_time_band@itbos i
where i.call_id=t.call_id
  and i.mrezna_grupa=t.mrezna_grupa
  and i.msisdn=t.msisdn
  and i.datumv=t.datumv
  and i.trajanje=t.trajanje
  and i.iznos!=t.iznos
  and i.pcijena=t.pcijena
  and i.paket_id=t.paket_id
  and i.pcijena!=0
  and t.iznos>t.PCIJENA

--Razlike kod obrade time bandova - TEST
select t.call_id,
	   t.korisnik_id,
	   t.otrajanje,
	   i.otrajanje,
	   t.pcijena,
	   i.pcijena,
	   t.iznos,
	   i.iznos,
	   t.katpoziva_id,
	   i.katpoziva_id,
	   i.paket_id,
	   t.paket_id
from TTRAFFIC t,
	 is_tft_time_band@itbos i
where i.call_id=t.call_id
  and i.mrezna_grupa=t.mrezna_grupa
  and i.msisdn=t.msisdn
  and i.datumv=t.datumv
  and i.trajanje=t.trajanje
  and i.zona_id!=0
  and i.iznos!=t.iznos
  and i.paket_id=t.paket_id
  and i.pcijena!=0
  and t.iznos<t.PCIJENA
    
  and t.call_id = 8630861

select t.call_id,
	   t.korisnik_id,
	   t.otrajanje,
	   i.otrajanje,
	   t.pcijena,
	   i.pcijena,
	   t.iznos,
	   i.iznos,
	   t.katpoziva_id,
	   i.katpoziva_id,
	   i.paket_id,
	   t.paket_id
from TTRAFFIC t,
	 is_tft_time_band@itbos i
where i.call_id=t.call_id
  and i.mrezna_grupa=t.mrezna_grupa
  and i.msisdn=t.msisdn
  and i.datumv=t.datumv
  and i.trajanje=t.trajanje
  and i.iznos!=t.iznos
  and i.pcijena=t.pcijena
  and i.paket_id=t.paket_id
  and i.pcijena!=0
  and t.iznos>t.PCIJENA  
  
select t.call_id,
	   t.korisnik_id,
	   t.otrajanje,
	   i.otrajanje,
	   t.pcijena,
	   i.pcijena,
	   t.iznos,
	   i.iznos,
	   t.katpoziva_id,
	   i.katpoziva_id,
	   i.paket_id,
	   t.paket_id
from TTRAFFIC t,
	 is_tft_time_band@itbos i
where i.call_id=t.call_id
  and i.mrezna_grupa=t.mrezna_grupa
  and i.msisdn=t.msisdn
  and i.datumv=t.datumv
  --and i.trajanje!=t.trajanje
  and i.otrajanje != t.otrajanje
  and i.zona_id!=0
  and i.iznos!=t.iznos
  and i.paket_id=t.paket_id
  and i.pcijena!=0
  and t.iznos=t.PCIJENA
  
select *
from TTRAFFIC
where call_id = 9028898

select *
from is_tft_time_band@itbos i
where call_id = 9028898