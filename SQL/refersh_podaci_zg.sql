alter table FSTAVKA_BUFFER modify  constraint FSTAVKA_FRACUN_BUFFER_FK disable ;
alter table FRACUN modify  constraint FRACUN_KKORISNIK_FK disable ;
alter table FRACUN_BUFFER modify  constraint FRACUN_BUFFER_KKORISNIK_FK disable ;
alter table PPRETPLATNIK modify  constraint PPRETPLATNIK_KKORISNIK_FK disable ;
alter table SCZAHTJEV modify  constraint SCZAHTJEV_KKORISNIK_FK disable ;
truncate table FSTAVKA_BUFFER;
truncate table  FRACUN_BUFFER;
truncate table   PPRETHIST;
truncate table    PPRETPLATNIK;
truncate table  PBROJHIST;
truncate table   PBROJ;
--truncate table  KKORHIST;
truncate table  KKORISNIK;
truncate table    SCZAHTJEV; 

insert into KKORISNIK select * from KKORISNIK@ITBZG.RI.HT.HR;
--  insert into KKORHIST select * from KKORHISTK@ITBZG.RI.HT.HR;
insert into PBROJ select * from PBROJ@ITBZG.RI.HT.HR;
insert into PBROJHIST select * from PBROJHIST@ITBZG.RI.HT.HR;
insert into PPRETPLATNIK select * from PPRETPLATNIK@ITBZG.RI.HT.HR;
insert into PPRETHIST select * from PPRETHIST@ITBZG.RI.HT.HR;
insert into FRACUN_BUFFER select * from FRACUN_BUFFER@ITBZG.RI.HT.HR;
insert into FSTAVKA_BUFFER select * from FSTAVKA_BUFFER@ITBZG.RI.HT.HR;
insert into SCZAHTJEV select * from SCZAHTJEV@ITBZG.RI.HT.HR;

alter table FSTAVKA_BUFFER modify  constraint FSTAVKA_FRACUN_BUFFER_FK enable ;
alter table FRACUN modify  constraint FRACUN_KKORISNIK_FK enable ;
alter table FRACUN_BUFFER modify  constraint FRACUN_BUFFER_KKORISNIK_FK enable ;
alter table PPRETPLATNIK modify  constraint PPRETPLATNIK_KKORISNIK_FK enable ;
alter table SCZAHTJEV modify  constraint SCZAHTJEV_KKORISNIK_FK enable ;

