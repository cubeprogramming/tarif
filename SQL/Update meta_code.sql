-- Update meta_codova iz TEMP_FIELDS

update cdr_field tcf set
	   tcf.META_CODE = (select tf.META_CODE
		                 from temp_fields tf
						 where tf.value_type is not null
						 and tf.rbr = tcf.field_id)
where tcf.record_id in (select cr.ID
                        from cdr_record cr
					    where ((record_type = 'AXE_R16') or (record_type = 'EWSD_R16')) )
  and exists (select 1
		      from temp_fields tf
			  where tf.META_CODE is not null
				and tf.rbr = tcf.field_id)
				
-- Deaktivacija polja iz PROGRAM_FIELDS koja nisu potrebna
select *
from cdr_field cf		
where cf.record_id in (select cr.ID
                        from cdr_record cr
					    where record_type = 'AXE_R16')
  and cf.FIELD_ID = 22
  

  
select * from PROGRAM_FIELDS pf
where pf.PROGRAM_ID = 8
  and pf.FIELD_ID in (select cf.ID
  	  			  	  from cdr_field cf		
					  where cf.record_id in (select cr.ID
                        				 	 from cdr_record cr
					    					 where record_type = 'EWSD_R16')
					  and cf.FIELD_ID = 4)							  
  
delete from PROGRAM_FIELDS pf
where pf.PROGRAM_ID = 8
  and pf.FIELD_ID in (select cf.ID
  	  			  	  from cdr_field cf		
					  where cf.record_id in (select cr.ID
                        				 	 from cdr_record cr
					    					 where record_type = 'AXE_R16')
					  and cf.FIELD_ID = 22)							
					  
update cdr_field cf	set
  	   cf.META_CODE = 'POZVANI_BROJ'
where cf.META_CODE = 'POZIVNI_BROJ_AXA'		

SELECT tf.RBR,
	   tf.FIELD_NAME,
	   tf.VALUE_TYPE,
	   tf.META_CODE 
from temp_fields tf
where tf.META_CODE is not null
order by tf.RBR		  