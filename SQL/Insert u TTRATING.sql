INSERT /*+APPEND*/ INTO TTRATING tr
	   (tr.NAZIV_DATOTEKE,
		tr.CALL_ID,
		tr.MREZNA_GRUPA,
		tr.MSISDN,
		tr.SIFRA_SERVISA,
		tr.SIFRA_SSERVISA,
		tr.POZVANI_BROJ,
		tr.DATUMV,
		tr.TRAJANJE,
		tr.OTRAJANJE,
		tr.IMPULSI,
		tr.IZNOS,
		tr.PCIJENA, -- dodano
		tr.BBROJ_PREFIKS,
		tr.ZONA_ID,
		tr.KATPOZIVA_ID,
		tr.PLAN_ID,
		tr.TARIFA_ID,
		tr.PRETPLATNIK_ID, -- dodano
		tr.KORISNIK_ID, -- dodano
		tr.BGRUPA_ID, -- dodano
		tr.PAKET_ID, -- dodano
		tr.A_CATEGORY,
		tr.B_CATEGORY,
		tr.TYPE_OF_B_NUM,
		tr.CHARGED_PARTY,
		tr.ORIGIN_FOR_CHARGING,
		tr.TARIFF_CLASS,
		tr.CHARGING_CASE,
		tr.CHARGING_CASE_ORIGIN,
		tr.FLEX_UUS,
		tr.ERROR_CODE,
		tr.TYPE_OF_ASUBSCRIBER)
VALUES (
	   'TT52004_20031101_000755.txt',
	   '1853221929',
	   '051',
	   '274214',
	   '10',
	   'FWD',
	   '076767676',
	   to_date('2004-09-30','YYYY-MM-DD'),
	   2410,
	   2500,
	   0,
	   2.56,
	   2.56,
	   076,
	   51,
	   10713,
	   1,
	   1,
	   103306755991,
	   103306755991,
	   1,
	   1,
	   '0',
	   null,
	   null,
	   '0',
	   null,
	   null,
	   '28',
	   null,
	   1,
	   '921',
	   '2'
);


 cause_for_output = 1
 record_number = 2
 naziv_datoteke = TT52004_20031101_000755.txt
 call_id = 1853221929
 mrezna_grupa = 52
 msisdn = 830552
 sifra_servisa = 10
 sifra_sservisa = FWD
 pozivni_broj = 076767676
 datumv = 2003-10-31
 trajanje = 0
 otrajanje = null
 impulsi = 1
 status = 0
 iznos = null
 pcijena = null
 bbroj_prefiks = null
 zona_id = null
 katpoziva_id = null
 plan_id = null
 tarifa_id = null
 pretplatnik_id = null
 korisnik_id = null
 bgrupa_id = null
 paket_id = null
 a_category = 0
 b_category = null
 type_of_b_num = null
 charged_party = 0
 origin_for_charging = null
 tariff_class = null
 charging_case = 28
 charging_case_origin = null
 tftraffic_id = null
 flex_uus = 1
 file_prefix = TT52004
 type_of_a_num = 2

 select code,
	   log_text
from ctariffcode

/* paket_id
 *     bgrupa_id
 bbroj_prefiks
 *     zona_id
 *     plan_id
 *     tarifa_id
 *     katpoziva_id
 *     error_code
 *     description
 pcijena
 *     iznos
 *     otrajanje 
 */