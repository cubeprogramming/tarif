select K.ID,
	   K.NAZIV,
	   CURSOR(SELECT GODINA_MJESEC,
	   				 BROJ_RACUNA
			  FROM FRACUN F
			  WHERE F.KORISNIK_ID = K.ID) 
from KKORISNIK K
where K.ID = 995306847091

SELECT * from KKORISNIK

CMETA_FIELD
CMETA_FORMAT
CMETA_RECORD
CMETHOD
CSOURCE_ELEMENT
CSOURCE_FORMAT

/* Formatted on 2004/09/08 15:14 (Formatter Plus v4.8.0) */
SELECT   f.record_type,
		 e.id, 
		 e.source_element_id, 
		 e.NAME, 
		 e.tag_id, 
		 e.input_length,
         f.start_position, 
		 f.LENGTH, 
		 f.tariff_key_word, 
		 f.traffic_column_name,
         f.rtraffic_column_name
    FROM cmeta_field f, 
		 csource_element e
   WHERE f.meta_format_code IN (SELECT code
                          		  FROM cmeta_format
                         		 WHERE billing_flag = 1
                               	   AND source_format_code = 'AXEF')
     AND f.record_type = '00'
     AND f.source_element_id = e.ID
ORDER BY f.start_position

--
select *
from CMETA_RECORD re
where re.META_FORMAT_CODE = 'HR12'
  and re.RECORD_TYPE = '0A'
  
select *
from CMETA_FIELD f
where f.RECORD_TYPE = '0A'

-- Tipovi podataka u recordima
select distinct
	   mr.RECORD_TYPE r_id,
	   se.TAG_ID id,
	   se.NAME field_name,
	   me.name value_type,
	   se.SOURCE_FORMAT_CODE,
	   mf.CODE
from cmethod me,
     csource_element se,
	 cmeta_format mf,
	 cmeta_record mr
where me.ID = se.METHOD_ID
  and se.SOURCE_FORMAT_CODE = mf.SOURCE_FORMAT_CODE
  and mf.CODE = mr.META_FORMAT_CODE
  and mr.SOURCE_ELEMENT_ID = se.ID
order by mr.RECORD_TYPE, se.TAG_ID

select 
	   mr.RECORD_TYPE r_id,
	   se.TAG_ID id,
	   se.NAME field_name,
	   me.name value_type,
	   se.SOURCE_FORMAT_CODE
from cmethod me,
     csource_element se,
	 cmeta_record mr
where me.ID = se.METHOD_ID
  and mr.SOURCE_ELEMENT_ID = se.ID
order by mr.RECORD_TYPE, se.TAG_ID

--Provjera za koji tip CDR-a postoji najvise unosa
select re.VALUE,
	   count(*) count
from CDR_FIELD fi,
     CDR_RECORD re
where fi.RECORD_ID = re.ID
  and re.RECORD_TYPE = 'AXE_NEW'
group by re.VALUE 
order by count(*) desc							
							
-- integrirano sa mojoim tablicama
SELECT   distinct
		 f.record_type oznaka_cdra,
		 re.RECORD_TYPE, 
		 cf.ID,
		 cf.DESCRIPTION, 
		 e.NAME,
		 e.tag_id, 
		 e.input_length,
         f.start_position, 
		 f.LENGTH, 
		 f.tariff_key_word, 
		 f.traffic_column_name,
         f.rtraffic_column_name
    FROM cmeta_field f, 
		 csource_element e,
		 CDR_FIELD cf,
		 CDR_RECORD re,
		 SWITCH sw  
   WHERE f.meta_format_code IN (SELECT code
                          		  FROM cmeta_format
                         		 WHERE billing_flag = 1
                               	   AND source_format_code = 'AXEF')
     AND f.record_type = '00'
     AND f.source_element_id = e.ID
	 and cf.FIELD_ID = e.TAG_ID -- povezivanje moje i njihove tabele
	 and cf.RECORD_ID = re.ID
	 and re.VALUE = f.RECORD_TYPE -- povezivanje moje i njihove tabele
	 and sw.RECORD_TYPE = re.RECORD_TYPE
	 and sw.PROGRAM_ID = 1
ORDER BY f.start_position

--Razlike izmedju target CDR-a i CDR-a sa najvise unosa
select *
from cdr_field fi,
	 CDR_RECORD re
where fi.RECORD_ID = re.ID
  and re.RECORD_TYPE = 'AXE_NEW'
  and re.VALUE = '04'
  and fi.FIELD_ID not in (select fi.FIELD_ID
  	  			  	  	  from CDR_FIELD fi,
     					       CDR_RECORD re
						  where fi.RECORD_ID = re.ID
  						    and re.RECORD_TYPE = 'AXE_R16'
							and re.VALUE = '05')
														
select fi.ID,
	   fi.FIELD_ID,
	   fi.RECORD_ID,
	   fi.VALUE_TYPE,
	   fi.OFFSET,
	   fi.LENGTH,
	   fi.DESCRIPTION
from cdr_field fi,
	 CDR_RECORD re,
	 cmeta_field f, 
	 csource_element e
where fi.RECORD_ID = re.ID
  and re.RECORD_TYPE = 'AXE_NEW'
  and re.VALUE = '04'
  
-- Trafic i ostale tablice
select *
from ttraffic@itbos

-- Potrebni metaslogovi u ovisnosti o tipu CDR-a
select distinct
	   mf.RECORD_TYPE,
	   se.TAG_ID,
	   se.NAME,
	   mf.TARIFF_KEY_WORD,
	   mf.TRAFFIC_COLUMN_NAME,
	   mf.RTRAFFIC_COLUMN_NAME,
	   cf.SOURCE_FORMAT_CODE
from cmeta_field mf,
     csource_element se,
	 cmeta_format cf
where mf.SOURCE_ELEMENT_ID = se.ID
  and mf.META_FORMAT_CODE = cf.CODE
  and cf.BILLING_FLAG = 1
  --and cf.SOURCE_FORMAT_CODE = 'AXEF'
  
-- join sa mojim tablicama
select distinct
	   mf.RECORD_TYPE,
	   se.TAG_ID,
	   se.NAME,
	   mf.TARIFF_KEY_WORD,
	   mf.TRAFFIC_COLUMN_NAME,
	   mf.RTRAFFIC_COLUMN_NAME,
	   cf.SOURCE_FORMAT_CODE
from cmeta_field mf,
     csource_element se,
	 cmeta_format cf,
	 cdr_record cr
where mf.SOURCE_ELEMENT_ID = se.ID
  and mf.META_FORMAT_CODE = cf.CODE
  and cf.BILLING_FLAG = 1
  and mf.RECORD_TYPE = cr.VALUE
  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  
-- Aktivacija polja u program fields u ovisnosti o dozvoljenim poljima u C_META_FIELD
select distinct
	   mf.RECORD_TYPE,
	   cr.ID,
	   cdrf.RECORD_ID,
	   cdrf.ID,
	   cdrf.FIELD_ID,
	   se.TAG_ID,
	   se.NAME,
	   mf.TARIFF_KEY_WORD,
	   mf.TRAFFIC_COLUMN_NAME,
	   mf.RTRAFFIC_COLUMN_NAME,
	   cf.SOURCE_FORMAT_CODE
from cmeta_field mf,
     csource_element se,
	 cmeta_format cf,
	 cdr_record cr,
	 cdr_field cdrf
where mf.SOURCE_ELEMENT_ID = se.ID
  and mf.META_FORMAT_CODE = cf.CODE
  and cf.BILLING_FLAG = 1
  and mf.RECORD_TYPE = cr.VALUE
  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  and cdrf.FIELD_ID = se.TAG_ID
  and cdrf.RECORD_ID = cr.ID 
  
  
-- Update META_CODE polje u CDR_FIELD

update CDR_FIELD tcf set
	   tcf.META_CODE = (select mf.RTRAFFIC_COLUMN_NAME 
	   				    from cmeta_field mf,
							 csource_element se,
	 						 cmeta_format cf,
	 						 cdr_record cr,
	 						 cdr_field cdrf
						where mf.SOURCE_ELEMENT_ID = se.ID
  						  and mf.META_FORMAT_CODE = cf.CODE
  						  and cf.BILLING_FLAG = 1
  						  and mf.RECORD_TYPE = cr.VALUE
  						  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  						  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  						  and cdrf.FIELD_ID = se.TAG_ID
  						  and cdrf.RECORD_ID = cr.ID
						  and cdrf.ID = tcf.ID )
where tcf.ID in (select cdrf.ID 
	   			 from cmeta_field mf,
					 csource_element se,
	 				 cmeta_format cf,
	 				 cdr_record cr,
	 				 cdr_field cdrf
				where mf.SOURCE_ELEMENT_ID = se.ID
  				  and mf.META_FORMAT_CODE = cf.CODE
  				  and cf.BILLING_FLAG = 1
  				  and mf.RECORD_TYPE = cr.VALUE
  				  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  				  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  				  and cdrf.FIELD_ID = se.TAG_ID
  				  and cdrf.RECORD_ID = cr.ID)
				  
-- Aktivacija polja u tablici PROGRAM_FIELDS

insert into program_fields
select 8,
	   cdrf.ID 
from cmeta_field mf,
	 csource_element se,
	 cmeta_format cf,
	 cdr_record cr,
	 cdr_field cdrf
where mf.SOURCE_ELEMENT_ID = se.ID
  and mf.META_FORMAT_CODE = cf.CODE
  and cf.BILLING_FLAG = 1
  and mf.RECORD_TYPE = cr.VALUE
  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  and cdrf.FIELD_ID = se.TAG_ID
  and cdrf.RECORD_ID = cr.ID
  
-- Lista za update tipova podataka

select distinct
	   cdrf.FIELD_ID,
       cdrf.DESCRIPTION,
	   mf.RTRAFFIC_COLUMN_NAME 
from cmeta_field mf,
	 csource_element se,
	 cmeta_format cf,
	 cdr_record cr,
	 cdr_field cdrf
where mf.SOURCE_ELEMENT_ID = se.ID
  and mf.META_FORMAT_CODE = cf.CODE
  and cf.BILLING_FLAG = 1
  and mf.RECORD_TYPE = cr.VALUE
  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  and cdrf.FIELD_ID = se.TAG_ID
  and cdrf.RECORD_ID = cr.ID
order by cdrf.FIELD_ID  

-- Update R16 plahte sa META_CODE

update TEMP_FIELDS tmpf set
	   tmpf.META_CODE = (select distinct mf.RTRAFFIC_COLUMN_NAME 
	   				    from cmeta_field mf,
							 csource_element se,
	 						 cmeta_format cf,
	 						 cdr_record cr,
	 						 cdr_field cdrf
						where mf.SOURCE_ELEMENT_ID = se.ID
  						  and mf.META_FORMAT_CODE = cf.CODE
  						  and cf.BILLING_FLAG = 1
  						  and mf.RECORD_TYPE = cr.VALUE
  						  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  						  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  						  and cdrf.FIELD_ID = se.TAG_ID
  						  and cdrf.RECORD_ID = cr.ID
						  and cdrf.FIELD_ID = tmpf.RBR )
where tmpf.RBR in (select distinct cdrf.FIELD_ID 
	   			 from cmeta_field mf,
					 csource_element se,
	 				 cmeta_format cf,
	 				 cdr_record cr,
	 				 cdr_field cdrf
				where mf.SOURCE_ELEMENT_ID = se.ID
  				  and mf.META_FORMAT_CODE = cf.CODE
  				  and cf.BILLING_FLAG = 1
  				  and mf.RECORD_TYPE = cr.VALUE
  				  and ((cf.SOURCE_FORMAT_CODE = 'AXEF') or (cf.SOURCE_FORMAT_CODE = 'EWSD'))
  				  and ((cr.RECORD_TYPE = 'AXE_R16') or (cr.RECORD_TYPE = 'EWSD_R16'))
  				  and cdrf.FIELD_ID = se.TAG_ID
  				  and cdrf.RECORD_ID = cr.ID)
				  
-- Potrebni metaslogovi
select distinct
	   se.TAG_ID,
	   se.NAME,
	   mf.TARIFF_KEY_WORD,
	   mf.TRAFFIC_COLUMN_NAME,
	   mf.RTRAFFIC_COLUMN_NAME,
	   cf.SOURCE_FORMAT_CODE
from cmeta_field mf,
     csource_element se,
	 cmeta_format cf
where mf.SOURCE_ELEMENT_ID = se.ID
  and mf.META_FORMAT_CODE = cf.CODE
  and cf.BILLING_FLAG = 1
order by se.TAG_ID
  --and cf.SOURCE_FORMAT_CODE = 'AXEF'  
				  
-- Aktivne vrste slogova za AXE i EWSD

select distinct
	   cmr.record_type
from cmeta_record cmr
where cmr.META_FORMAT_CODE in (select cmf.CODE
	  					   	   from cmeta_format cmf
							   where cmf.BILLING_FLAG = 1
							     and ((cmf.SOURCE_FORMAT_CODE = 'AXEF') or (cmf.SOURCE_FORMAT_CODE = 'EWSD')) )