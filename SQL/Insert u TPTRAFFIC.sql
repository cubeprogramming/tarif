INSERT /*+APPEND*/ INTO TPTRAFFIC tp
	   (tp.RECORD_ID,
	    tp.NAZIV_DATOTEKE,
		tp.CAUSE_FOR_OUTPUT,
		tp.RECORD_NUMBER,
		tp.CALL_ID,
		tp.MREZNA_GRUPA,
		tp.MSISDN,
		tp.SIFRA_SERVISA,
		tp.SIFRA_SSERVISA,
		tp.POZVANI_BROJ,
		tp.DATUMV,
		tp.TRAJANJE,
		tp.IMPULSI,
		tp.STATUS,
		tp.A_CATEGORY,
		tp.B_CATEGORY,
		tp.TYPE_OF_B_NUM,
		tp.CHARGED_PARTY,
		tp.ORIGIN_FOR_CHARGING,
		tp.TARIFF_CLASS,
		tp.CHARGING_CASE,
		tp.CHARGING_CASE_ORIGIN,
		tp.TFTRAFFIC_ID,
		tp.FLEX_UUS,
		tp.FILE_PREFIX,
		tp.TYPE_OF_ASUBSCRIBER)
VALUES (
	   null,
	   'TT52004_20031101_000755.txt',
	   '1',
	   2,
	   '1853221929',
	   '52',
	   '830552',
	   '10',
	   'FWD',
	   '076767676',
	   to_date('2003-10-31','YYYY-MM-DD'),
	   0,
	   1,
	   '0',
	   '0',
	   null,
	   null,
	   '0',
	   null,
	   null,
	   '28',
	   null,
	   null,
	   1,
	   'TT52004',
	   '2'
);


 cause_for_output = 1
 record_number = 2
 naziv_datoteke = TT52004_20031101_000755.txt
 call_id = 1853221929
 mrezna_grupa = 52
 msisdn = 830552
 sifra_servisa = 10
 sifra_sservisa = FWD
 pozivni_broj = 076767676
 datumv = 2003-10-31
 trajanje = 0
 otrajanje = null
 impulsi = 1
 status = 0
 iznos = null
 pcijena = null
 bbroj_prefiks = null
 zona_id = null
 katpoziva_id = null
 plan_id = null
 tarifa_id = null
 pretplatnik_id = null
 korisnik_id = null
 bgrupa_id = null
 paket_id = null
 a_category = 0
 b_category = null
 type_of_b_num = null
 charged_party = 0
 origin_for_charging = null
 tariff_class = null
 charging_case = 28
 charging_case_origin = null
 tftraffic_id = null
 flex_uus = 1
 file_prefix = TT52004
 type_of_a_num = 2
