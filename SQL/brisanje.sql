delete 
from TTRAFFIC;

delete 
from TTRATING;

delete 
from TPTRAFFIC;

delete 
from TSTRAFFIC;

delete
from MCEVENTLOG;

delete
from MCEVENTLOG m
where m.MIN_FILE_DATETIME is null
  and m.MAX_FILE_DATETIME is null;
  
delete
from MCEVENTLOG
where FILE_NAME_IN like 'tt98001_%';  

delete 
from TTRAFFIC
where NAZIV_DATOTEKE like 'tt98001_%';

delete 
from TTRATING
where NAZIV_DATOTEKE like 'tt98001_%';

delete 
from TPTRAFFIC
where NAZIV_DATOTEKE like 'tt98001_%';

delete 
from TSTRAFFIC
where NAZIV_DATOTEKE like 'tt98001_%';

truncate table TTRAFFIC;

truncate table TTRATING;

truncate table TPTRAFFIC;

truncate table TSTRAFFIC;

truncate table tmtraffic;

truncate table tbtraffic;

truncate table tbtraffic_err;

truncate table tbrecord;

truncate table tbrecord_err;

truncate table tbonus;

truncate table tft_dupli_small;

truncate table tft_dupli;

truncate table TFTRAFFIC;

delete from TFTRAFFIC;

truncate table MDBEVENTLOG;