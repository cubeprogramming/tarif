CREATE OR REPLACE TRIGGER "ARATE".TTRAFFIC_SEQ 
before insert on TTRAFFIC
referencing NEW as NEW 
for each row 
begin 
	 select SEQ_TRAFFIC.nextval into :new.RECORD_ID from dual; 
end;
/
