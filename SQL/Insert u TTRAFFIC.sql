INSERT /*+APPEND*/ INTO TTRAFFIC tf
	   (tf.NAZIV_DATOTEKE,
		tf.CALL_ID,
		tf.MREZNA_GRUPA,
		tf.MSISDN,
		tf.SIFRA_SERVISA,
		tf.SIFRA_SSERVISA,
		tf.POZVANI_BROJ,
		tf.DATUMV,
		tf.TRAJANJE,
		tf.OTRAJANJE,
		tf.IMPULSI,
		tf.IZNOS,
		tf.PCIJENA,
		tf.BBROJ_PREFIKS,
		tf.ZONA_ID,
		tf.KATPOZIVA_ID,
		tf.PLAN_ID,
		tf.TARIFA_ID,
		tf.PRETPLATNIK_ID,
		tf.KORISNIK_ID,
		tf.BGRUPA_ID,
		tf.PAKET_ID,
		tf.A_CATEGORY,
		tf.B_CATEGORY,
		tf.TYPE_OF_B_NUM,
		tf.CHARGED_PARTY,
		tf.ORIGIN_FOR_CHARGING,
		tf.TARIFF_CLASS,
		tf.CHARGING_CASE,
		tf.CHARGING_CASE_ORIGIN,
		tf.FLEX_UUS)
VALUES (
	   'TT52004_20031101_000755.txt',
	   '1853221929',
	   '051',
	   '274214',
	   '10',
	   'FWD',
	   '076767676',
	   to_date('2004-09-30','YYYY-MM-DD'),
	   2410,
	   2500,
	   0,
	   2.56,
	   2.56,
	   076,
	   51,
	   10713,
	   1,
	   1,
	   103306755991,
	   103306755991,
	   1,
	   1,
	   '0',
	   null,
	   null,
	   '0',
	   null,
	   null,
	   '28',
	   null,
	   1
);


 cause_for_output = 1
 record_number = 2
 naziv_datoteke = TT52004_20031101_000755.txt
 call_id = 1853221929
 mrezna_grupa = 52
 msisdn = 830552
 sifra_servisa = 10
 sifra_sservisa = FWD
 pozivni_broj = 076767676
 datumv = 2003-10-31
 trajanje = 0
 otrajanje = null
 impulsi = 1
 status = 0
 iznos = null
 pcijena = null
 bbroj_prefiks = null
 zona_id = null
 katpoziva_id = null
 plan_id = null
 tarifa_id = null
 pretplatnik_id = null
 korisnik_id = null
 bgrupa_id = null
 paket_id = null
 a_category = 0
 b_category = null
 type_of_b_num = null
 charged_party = 0
 origin_for_charging = null
 tariff_class = null
 charging_case = 28
 charging_case_origin = null
 tftraffic_id = null
 flex_uus = 1
 file_prefix = TT52004
 type_of_a_num = 2

 select code,
	   log_text
from ctariffcode

/* paket_id
 *     bgrupa_id
 bbroj_prefiks
 *     zona_id
 *     plan_id
 *     tarifa_id
 *     katpoziva_id
 *     error_code
 *     description
 pcijena
 *     iznos
 *     otrajanje 
 */