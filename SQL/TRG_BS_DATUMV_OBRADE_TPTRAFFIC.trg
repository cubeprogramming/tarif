CREATE OR REPLACE TRIGGER ARATE.TRG_BS_DATUMV_OBRADE_TPTRAFFIC
BEFORE INSERT or UPDATE
ON ARATE.TPTRAFFIC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01.10.04	  Dalibor Blazevic 1. Created this trigger.
   1.1		  13.12.04	  Dalibor Blazevic 2. Dodan UPDATE			   

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     
      Sysdate:         01.10.04
      Date and Time:   01.10.04, 10:00:49, and 01.10.04 10:00:49
      Username:         Dalibor
      Table Name:       TPTRAFFIC
      Trigger Options:  AFTER INSERT FOR EACH ROW
******************************************************************************/
BEGIN
   :new.DATUMV_OBRADE := tft_utils.Get_Udr_Time(:new.DATUMV);
   
   EXCEPTION
     WHEN OTHERS THEN
       raise_application_error(SQLCODE-20000, SQLERRM||' Greska u trigeru: TRG_BS_DATUMV_OBRADE_TPTRAFFIC'||USER);
END ;
/
