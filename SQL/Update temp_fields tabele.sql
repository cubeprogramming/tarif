select * from Temp_fields
order by rbr

select sum(A00)
from Temp_fields
where rbr between 1 and 3

select tf.rbr,
	   tf.FIELD_NAME,
	   tf.A00,
	   nvl((select sum(A00)
	    from Temp_fields
		where rbr < tf.rbr),0) sum
from Temp_fields tf
order by rbr

					  
create table temp_cdr_field as
select * from cdr_field

select *
from cdr_record
order by record_type, value

select value,
	   record_length
from cdr_record
where record_length = -1
  and record_type = 'AXE_R16'
  and value = '00'

update cdr_record
set record_length = (select sum(E17)
				  	 from Temp_fields)
where record_length = -1
  and record_type = 'EWSD_R16'
  and value = '11'		 			  
  
-- izracun offseta za pojedine CDR slogove
update Temp_fields tf
   set tf.EO17 = nvl((select sum(E17)
	    	   	      from Temp_fields
				      where rbr < tf.rbr),0)
  
-- brisanje AXE i EWSD polja iz temp_cdr_field tablice  
select *
from temp_cdr_field tf
where tf.record_id in (select cr.ID
                       from cdr_record cr
					   where record_type = 'AXE_R16')
					   
delete from temp_cdr_field tf
where tf.record_id in (select cr.ID
                       from cdr_record cr
					   where record_type = 'AXE_R16')
					   
-- insertiranje novih podataka u temp_cdr_field
insert into temp_cdr_field tf
select null id,
	   rbr field_id,
	   (select cr.ID
        from cdr_record cr
		where cr.record_type = 'EWSD_R16'
		and cr.value = '11') record_id,
	   'UNK' value_type,
	   EO17 offset,
	   E17 length,
	   FIELD_NAME description,
	   null meta_code  
from temp_fields
where E17 is not null 
order by record_id, rbr			

-- update postojecih polja sa tipovima
update temp_cdr_field tcf set
	   tcf.value_type = (select tf.value_type
		                 from temp_fields tf
						 where tf.value_type is not null
						 and tf.rbr = tcf.field_id)
where tcf.record_id in (select cr.ID
                        from cdr_record cr
					    where ((record_type = 'AXE_R16') or (record_type = 'EWSD_R16')) )
  and exists (select 1
		      from temp_fields tf
			  where tf.value_type is not null
				and tf.rbr = tcf.field_id)
   	  
-- brisanje polja iz cdr_field tablice  

delete from program_fields pf
where exists (select 1 
              from switch sw
			  where sw.PROGRAM_ID = pf.PROGRAM_ID
			    and sw.RECORD_TYPE = 'AXE_R16')


select *
from cdr_field tf
where tf.record_id in (select cr.ID
                       from cdr_record cr
					   where record_type = 'AXE_R16')
					   
delete from cdr_field tf
where tf.record_id in (select cr.ID
                       from cdr_record cr
					   where record_type = 'EWSD_R16')
					   
-- prebacivanje iz temp tablice u produkcijsku


  -- brisanje duplih  
/*  delete from temp_cdr_field tf
  where tf.ID in (select tcf.ID
  			      from temp_cdr_field tcf
				  where tf.rowid > tcf.rowid
				    and tf.FIELD_ID = tcf.FIELD_ID
                    and tf.RECORD_ID = tcf.RECORD_ID) */
  
insert into cdr_field cf
select null,
	   tf.FIELD_ID,
	   tf.RECORD_ID,
	   tf.VALUE_TYPE,
	   tf.OFFSET,
	   tf.LENGTH,
	   tf.DESCRIPTION,
	   null
from temp_cdr_field tf
where tf.record_id in (select cr.ID
                       from cdr_record cr
					   where record_type = 'EWSD_R16') 


-- update postojecih polja sa offsetima

update cdr_field tcf set
	   tcf.OFFSET = (select tf.EO11
		                 from temp_fields tf
						 where tf.value_type is not null
						 and tf.rbr = tcf.field_id)
where tcf.record_id in (select cr.ID
                        from cdr_record cr
					    where (cr.record_type = 'EWSD_R16')
						  and cr.VALUE = '0B')
  and tcf.FIELD_ID >= 241
  
update cdr_field tcf set
	   tcf.VALUE_TYPE = 'TIME'
where tcf.META_CODE = 'TRAJANJE'
 
  
				
					    