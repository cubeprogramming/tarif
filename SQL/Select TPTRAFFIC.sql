/* Formatted on 2004/12/10 15:24 (Formatter Plus v4.8.0) */
SELECT record_id, 
	   naziv_datoteke, 
	   cause_for_output, 
	   record_number, 
	   call_id,
       mrezna_grupa, 
	   msisdn, 
	   sifra_servisa, 
	   sifra_sservisa, 
	   pozvani_broj,
       datumv, 
	   trajanje, 
	   impulsi, 
	   status,
	   a_category,
       b_category, 
	   type_of_b_num, 
	   charged_party, 
	   origin_for_charging,
       tariff_class, 
	   charging_case, 
	   charging_case_origin, 
	   tftraffic_id,
       flex_uus, 
	   file_prefix, 
	   type_of_asubscriber
FROM tptraffic
where status = '0'
  and datumv <= :datum
order by file_prefix, 
	  	 mrezna_grupa,
		 msisdn,
		 call_id,
		 datumv;	 
		 
select * from tptraffic
order by file_prefix, 
	  	 mrezna_grupa,
		 msisdn,
		 call_id,
		 datumv;
  
update tptraffic set
  status = :status
  
where record_id = :record_id
		 
select * from tdst_offset;

-- Mo�e vratiti i NULL na prelazu godine
SELECT TIME, 
	   offset_flag
FROM tdst_offset
where trunc(time,'YYYY') = trunc(sysdate,'YYYY');

select *
from pmrezni_servis
where kategorija_ms = 1;		 
		 
select file_prefix, 
	   mrezna_grupa,
	   msisdn,
	   call_id,
	   datumv,
	   trajanje,
	   cause_for_output,
	   status
FROM tptraffic
where status in ('1','2');

				  
-- Zaka�njeli slogovi

select tp.file_prefix, 
	   tp.mrezna_grupa,
	   tp.msisdn,
	   tp.call_id
FROM tptraffic tp
where tp.status = '0'
  and exists (select 1
              from tptraffic tpo
			  where tpo.status in ('1','2')
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV >= tpo.DATUMV)
  and exists (select 1
              from tptraffic tpo
			  where status in ('1','2')
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV <= (tpo.DATUMV + (get_sekunde(trajanje,'US')/86400) ) );
				
-- Update zaka�njelih slogova kojima nije poznat zavr�etak
update tptraffic tp set
  tp.STATUS = 'z'
where tp.status = '0'
  and exists (select 1
              from tptraffic tpo
			  where tpo.status = '1'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV >= tpo.DATUMV)
  and exists (select 1
              from tptraffic tpo
			  where status = '1'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV <= (tpo.DATUMV + (get_sekunde(trajanje,'US')/86400) ) );	
				
-- Update zaka�njelih slogova kojima je poznat zavr�etak
update tptraffic tp set
  tp.STATUS = 'Z'
where tp.status = '0'
  and exists (select 1
              from tptraffic tpo
			  where tpo.status = '2'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV >= tpo.DATUMV)
  and exists (select 1
              from tptraffic tpo
			  where status = '2'
			    and tpo.FILE_PREFIX = tp.FILE_PREFIX
			    and tpo.MREZNA_GRUPA = tp.MREZNA_GRUPA
				and tpo.MSISDN = tp.MSISDN
			    and tpo.CALL_ID = tp.CALL_ID
			    and tp.DATUMV <= (tpo.DATUMV + (get_sekunde(trajanje,'US')/86400) ) );		
				
-- Selekt parcijalnih slogova po grupama

select tp.file_prefix, 
	   tp.mrezna_grupa,
	   tp.msisdn,
	   tp.call_id,
	   min(datumv) datum_od,
	   max(datumv) datum_do
FROM tptraffic tp
where tp.status = '0'
group by tp.file_prefix, 
	   	 tp.mrezna_grupa,
	   	 tp.msisdn,
	   	 tp.call_id;