-- Sifra i zona
select sifra,
	   zona_id,
	   sifra_mrezkomb
from tmrezna_grupa

-- Polje 241 u slogu
select TF.SS_KOD,
	   TF.ID,
	   TF.PRIORITET
from TFACILITY tf
where TF.SS_KOD is not null

-- prioriteti servisa
select pms.SS_KOD,
	   pms.PRIORITET
from PMREZNI_SERVIS pms
where pms.KATEGORIJA_MS = 1
order by pms.PRIORITET