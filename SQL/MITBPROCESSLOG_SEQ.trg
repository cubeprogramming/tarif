CREATE OR REPLACE TRIGGER "ARMALI".MITBPROCESSLOG_SEQ 
before insert on MITBPROCESSLOG
referencing NEW as NEW 
for each row 
begin 
	 select MITBPROCESSLOG_SEQ.nextval into :new.ID from dual; 
end;
/
